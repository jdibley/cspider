/**
 * CspiderGoSTErrorListener
 */
package io.github.jamesdibley.cspider.translator.go;

import org.stringtemplate.v4.*;
import org.stringtemplate.v4.misc.*;
import io.github.jamesdibley.cspider.translator.go.*;

public class CspiderGoSTErrorListener extends CspiderSTErrorListener {

	public CspiderGoSTErrorListener() {
		super();
	}

	@Override
	public void compileTimeError(STMessage msg) {
		// TODO: Handle these properly (log4j channel?) 
		// instead of just suppressing them
		;
	}

	@Override
	public void runTimeError(STMessage msg) {
		// TODO: Handle these properly (log4j channel?) 
		// instead of just suppressing them
		;
	}

	@Override
	public void IOError(STMessage msg) {
		// TODO: Handle these properly (log4j channel?) 
		// instead of just suppressing them
		;
	}

	@Override
	public void internalError(STMessage msg) {
		// TODO: Handle these properly (log4j channel?) 
		// instead of just suppressing them
		;
	}
}
