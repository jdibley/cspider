/**
 * SetDef
 */
package io.github.jamesdibley.cspider.translator.def;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.LinkedHashMap;
import java.util.Map;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
import org.stringtemplate.v4.*;
import io.github.jamesdibley.cspider.parser.gen.CSPMParser.PatternDeclContext;
import io.github.jamesdibley.cspider.translator.BaseDef;
import io.github.jamesdibley.cspider.translator.ValueType;
import io.github.jamesdibley.cspider.translator.symbol.*;

public class SetDef extends BaseDef {
	private List<String> members;
	private ValueType memberType;
	private String beginRange;
	private String endRange;
	private boolean baseSet;
	private boolean rangedIntSet;
	private Map<String, ChannelDef> referencingChans;
	private Map<String, ProcessDef> referencingProcs;
	private Map<String, FunctionDef> referencingFuncs;
	private Map<String, ExtBaseDef> extRefs;
	private ST declTemplate;
	//private List<> extSymbols;

	public SetDef(ParserRuleContext ctx, List<String> members,
			ValueType memberType, String name, 
			Scope currentScope) {
		super(ctx, name);
		setType(ValueType.SET);
		this.members = new ArrayList<String>(members);
		this.memberType = memberType;
		referencingChans = new LinkedHashMap<String, ChannelDef>();
		referencingProcs = new LinkedHashMap<String, ProcessDef>();
		referencingFuncs = new LinkedHashMap<String, FunctionDef>();
		extRefs = new LinkedHashMap<String, ExtBaseDef>();
		// FIXME: check members for ext refs
		for (String s : members) {
			checkMemberForExtRef(s, currentScope);
		}
		baseSet = false;	
		rangedIntSet = false;
		declTemplate = null;
		//markComplete();
	}

	public SetDef(ParserRuleContext ctx, ValueType memberType, String name) {
		super(ctx, name);
		setType(ValueType.SET);
		this.memberType = memberType;
		members = new ArrayList<String>();
		referencingChans = new LinkedHashMap<String, ChannelDef>();
		referencingProcs = new LinkedHashMap<String, ProcessDef>();
		referencingFuncs = new LinkedHashMap<String, FunctionDef>();
		extRefs = new LinkedHashMap<String, ExtBaseDef>();
		baseSet = true;
		rangedIntSet = false;
		declTemplate = null;
	}

	public SetDef(ParserRuleContext ctx, String beginRange, String endRange,
			String name, Scope currentScope) {
		super(ctx, name);
		setType(ValueType.RANGED_SET);
		this.memberType = ValueType.INT;
		members = new ArrayList<String>();
		members.add(beginRange);
		members.add(endRange);
		referencingChans = new LinkedHashMap<String, ChannelDef>();
		referencingProcs = new LinkedHashMap<String, ProcessDef>();
		referencingFuncs = new LinkedHashMap<String, FunctionDef>();
		// FIXME: check begin and end range for ext refs
		extRefs = new LinkedHashMap<String, ExtBaseDef>();
		checkMemberForExtRef(beginRange, currentScope);
		checkMemberForExtRef(endRange, currentScope);
		baseSet = false;
		rangedIntSet = true;
		declTemplate = null;
	}

	private void checkMemberForExtRef(String s, Scope currentScope) {
		Symbol sym = currentScope.resolve(s);
		if (sym != null && sym.def() != null 
				&& sym.def() instanceof ExtBaseDef) {
			extRefs.put(s, (ExtBaseDef) sym.def());
		}
	}

	public List<String> members() {
		return members;
	}

	public String member(int i) {
		return members.get(i);
	}

	public ValueType memberType() {
		return memberType;
	}

	public void setMemberType(ValueType vt) {
		this.memberType = vt;
	}


	public void addReferencingChan(ChannelDef cd) {
		referencingChans.put(cd.name(), cd);	
	}

	public void addReferencingProc(ProcessDef pd) {
		referencingProcs.put(pd.name(), pd);
	}

	public void addReferencingFunc(FunctionDef fd) {
		referencingFuncs.put(fd.name(), fd);
	}

	public void addExtRef(ExtBaseDef ebd) {
		extRefs.put(ebd.name(), ebd);
	}

	public Collection<ExtBaseDef> extRefs() {
		return extRefs.values();
	}

	public Collection<ChannelDef> referencingChans() {
		return referencingChans.values();
	}

	public Collection<ProcessDef> referencingProcs() {
		return referencingProcs.values();
	}

	public Collection<FunctionDef> referencingFuncs() {
		return referencingFuncs.values();
	}


	public boolean referencedFromProcs() {
		return !(referencingProcs.isEmpty());
	}

	public boolean referencedFromChans() {
		return !(referencingChans.isEmpty());
	}

	public boolean referencedFromFuncs() {
		return !(referencingFuncs.isEmpty());
	}	


	public void addDeclTemplate(ST st) {
		declTemplate = st;
	}

	public ST declTemplate() {
		return declTemplate;
	}

	public String toString() {
		List<String> chanNames = new ArrayList<String>();
		List<String> procNames = new ArrayList<String>();
		List<String> funcNames = new ArrayList<String>();
		List<String> extRefNames = new ArrayList<String>();
		for (ChannelDef c : referencingChans.values()) {
			chanNames.add(c.name());
		}
		for (ProcessDef p : referencingProcs.values()) {
			procNames.add(p.name());
		}
		for (FunctionDef f : referencingFuncs.values()) {
			funcNames.add(f.name());
		}
		for (ExtBaseDef e : extRefs.values()) {
			extRefNames.add(e.name());
		}
		if (type() == ValueType.RANGED_SET) {
			return defStateToString() + "-> (" 
				+ "[" + members.get(0) + ".." + members.get(1) + "]" + ")"
				+ "[" + memberType + "]" 
				+ chanNames 
				+ procNames
				+ funcNames
				+ extRefNames;
		} else if (baseSet) {
			return defStateToString() 
				+ memberType
				+ chanNames 
				+ procNames
				+ funcNames
				+ extRefNames;
		} else {
			return defStateToString() + "-> (" 
				+ members.toString() + ")"
				+ "[" + memberType + "]"
				+ chanNames 
				+ procNames
				+ funcNames
				+ extRefNames;
		}
	}
}
