/**
 * Pass00
 * We scan the input file for unsupported declarations
 */
package io.github.jamesdibley.cspider.translator.pass;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import io.github.jamesdibley.cspider.parser.gen.*;
import io.github.jamesdibley.cspider.parser.gen.CSPMParser.*;
import io.github.jamesdibley.cspider.translator.BasePass;
import io.github.jamesdibley.cspider.translator.TranState;

public class Pass00 extends BasePass {
	private static final Logger logger = LogManager.getLogger(Pass00.class);

	public Pass00(TranState tState, ParseTreeWalker walker, 
			CSPMParser parser) {
		super(tState, walker, parser);
		logger.trace("Scanning for unsupported declarations.");
	}

	public void process(SourcefileContext tree) {	
		//logger.trace("FIXME: I don't check anything yet.");
		//logger.trace("FIXME: Reject unimplementable CSP operators.");
		//logger.trace("FIXME: Reject identifiers matching reserved words.");
		//logger.trace("FIXME: Only accept sets that are int or event type.");
		//logger.trace("FIXME: Only accept sequences that are int or char type.");
		walker.walk(this, tree);
	}

	@Override
	public void exitSourcefile(SourcefileContext ctx) {
		logger.trace("Pass00 over.");
	}

	@Override
	public void exitExprIntCh(ExprIntChContext ctx) {
		logger.error("Unsupported expression detected at line " 
				+ ctx.start.getLine()
				+ ": internal choice.");
		tState.abortTranslation();
	}

}
