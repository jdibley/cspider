/**
 * Pass09
 */
package io.github.jamesdibley.cspider.translator.pass;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
import org.antlr.v4.runtime.tree.xpath.XPath;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import io.github.jamesdibley.cspider.parser.gen.*;
import io.github.jamesdibley.cspider.parser.gen.CSPMParser.*;
import io.github.jamesdibley.cspider.translator.BaseDef;
import io.github.jamesdibley.cspider.translator.BasePass;
import io.github.jamesdibley.cspider.translator.TranState;
import io.github.jamesdibley.cspider.translator.ValueType;
import io.github.jamesdibley.cspider.translator.def.*;
import io.github.jamesdibley.cspider.translator.def.var.*;
import io.github.jamesdibley.cspider.translator.symbol.*;

public class Pass09 extends ProcFuncCommon {
	private static final Logger logger = LogManager.getLogger(Pass09.class);
	private String xPath = "//letClause/patternDecl";
	
	public Pass09(TranState tState, ParseTreeWalker walker,
			CSPMParser parser) {
		super(tState, walker, parser);
		logger.trace("Gathering regular processes defined within LDEs.");
	}

	public void process(SourcefileContext tree) {
		processLDEProcesses(tree, xPath);
	}

	private void processLDEProcesses(ParseTree tree, String path) {
		for (ParseTree t : XPath.findAll(tree, path, parser)) {
			resetWP();
			String tmpName = null;
			Scope parentScope = null;
			PatternDeclContext pdc = (PatternDeclContext) t;
			NamePatternContext npc = pdc.patternLHS().namePattern();

			//logger.trace(t.getText());

			if (tState.isInMap(t)) {
//				logger.error(getTmpName(npc) + " is already in map.");
				continue; 
			}
			if (!isRegularPattern(npc)) {
				logger.error("Unsupported decl found. Check "
						+ "input for type annotations. "
						+ npc.getText());
				tState.abortTranslation();
				return;
			}	

		// Fetch and load the appropriate scope.
		// the parent of ParseTree t should be a LetClause, which should
		// have a LetClauseDef associated with it.
			LetClauseDef lcd = (LetClauseDef) def(t.getParent());
			parentScope = lcd.getScope();


			tmpName = getTmpName(npc);
		// Push a new scope here to hold any new identifiers 
		// encountered in the process expression (as well as an LDE,
		// if it exists, although we won't hoover up whatever defs 
		// exist there until the reconciliation pass.)
			tState.symTable.pushScope(parentScope);
			ExpressionContext e = pdc.expression();
			Map<String, String> tmpRenameMap = 
				processRenamingBlock(pdc);
		// This is an LDE process so is unlikely to have its own 
		// renaming clause. Odds are good that its parent process will, 
		// however:
		// 	1) attempt to do the LDE process' block
		// 	2) copy all renamings out of parent process' map
			copyParentRenaming(pdc, tmpRenameMap);
			classifying = true;
			walker.walk(this, e);
			classifying = false;

		// We define the process within the parent scope, not the
		// one we just pushed.
			ProcessDef pd = 
				new ProcessDef(pdc, null, parentScope, tmpName);
		// We also stow the scope for the -contents- of this process
		// and its expression:
			pd.setContainedScope(tState.symTable.currentScope());
	
			scanIOOperations(e, pd);

			logger.trace("Pass09: " + pd.name() 
					+ " tmp rename map: " + tmpRenameMap);
			
			if (walkedCompType != CompType.UNKNOWN) {
				pd.setCompType(walkedCompType);
				resetWalkedCompType();
			}

			for (String s : tmpRenameMap.keySet()) {
				pd.addChanRenaming(s, tmpRenameMap.get(s));
			}
			
			for (SetDef sd : walkedSetRefs.values()) {
				sd.addReferencingProc(pd);
				pd.addSetRef(sd);
			}

			for (ListDef ld : walkedListRefs.values()) {
				ld.addReferencingProc(pd);
				pd.addListRef(ld);
			}

			for (BaseDef bd : walkedConstRefs.values()) {
				pd.addConstRef(bd);
				if (bd instanceof ConstIntDef) {
					ConstIntDef cid = (ConstIntDef) bd;
					for (ExtBaseDef ebd : cid.extRefs()) {
						ebd.addReferencingProc(pd);
						pd.addExtRef(ebd);
					}
				}
			}

			for (ExtBaseDef ebd : tmpExtRefs.values()) {
				ebd.addReferencingProc(pd);
				pd.addExtRef(ebd);
			}
			tmpExtRefs.clear();
		// A process defined within an LDE can still have an LDE of its 
		// own: the key detail, which we -don't- enforce here, is that 
		// its own LDE can't contain any ProcessDefs.
			if (pdc.letClause() != null) {
				LetClauseContext lcc = pdc.letClause();
				LetClauseDef nlcd = new LetClauseDef(lcc, 
						tState.symTable.currentScope(), pd);
				nlcd.setTranState(tState);
				setDef(lcc, nlcd);
				//logger.trace("set LDE on process: " + tmpName);
			}

			logger.trace(pd.name());
			logger.trace("params: " );
			for (ParamDef prd : pd.params()) {
				logger.trace(prd.name());
			}
			logger.trace("extrefs: " + pd.extRefKeys());

			pd.setTranState(tState);
			setDef(pdc, pd);
			tState.symTable.currentScope().parentScope().define(pd, tmpName);

			//walkedChanRefs.clear();
			tState.symTable.popScope();
		}
	}

	private void copyParentRenaming(PatternDeclContext pdc,
			Map<String, String> renamingMap) {	
		if (!(pdc.getParent() instanceof LetClauseContext)) {
			logger.error("Unexpected parent found in copyParentRenaming.");
			return;
		}
		LetClauseContext lcc = (LetClauseContext) pdc.getParent();
		BaseDef bd = def(lcc);
		if (!(bd instanceof LetClauseDef)) { 
			logger.error("Unexpected def found in copyParentRenaming.");
			return;
		}
		LetClauseDef lcd = (LetClauseDef) bd;
		ProcessDef parent = (ProcessDef) lcd.parent();
		for (String s : parent.chanRenameKeys()) {
			renamingMap.put(s, parent.chanRenaming(s));
		}
	}
}
