/**
 * OutputModel
 */
package io.github.jamesdibley.cspider.translator.go;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.stringtemplate.v4.*;
import io.github.jamesdibley.cspider.FileProcessor;
import io.github.jamesdibley.cspider.translator.ValueType;
import io.github.jamesdibley.cspider.translator.go.*;
import io.github.jamesdibley.cspider.translator.def.*;

public class OutputModel {
	private static final Logger logger = LogManager.getLogger(OutputModel.class);
	
	// Admin and organisation 
	private CspiderGoSTErrorListener stErrorL = 
		new CspiderGoSTErrorListener();
	public ParseTreeProperty<ST> STMap;
	private String stPath = "src/ST/cspiderGo.stg";
	public static STGroup templates;
	
	// Code gen targets 
	public ProcessNetwork procNet;
	public Map<String, ProcessObject> procObjects;
	
	// Global definition maps
	public Map<String, ChannelDef> chanDefMap;
	public Map<String, ConstCharDef> constCharDefMap;
	public Map<String, ConstIntDef> constIntDefMap;
	public Map<String, ConstStringDef> constStringDefMap;
	public Map<String, ExtBaseDef> extBaseDefMap;
	public Map<String, FunctionDef> functionDefMap;
	public Map<String, ST> functionSTMap;
	public Map<String, ListDef> listDefMap;
	public Map<String, ProcessDef> processDefMap;
	public Map<String, SetDef> setDefMap;
	public Map<String, MsgStructDef> msgStructDefMap;
	public Map<String, ProcInvocationDef> procInvocationDefMap;
	public Map<String, ParamDef> exportedParamMap;

	public OutputModel() {
		STMap = new ParseTreeProperty<ST>();
		templates = new STGroupFile(stPath);
		// FIXME: configure ST error listener here
		templates.setListener(stErrorL);

		procNet = 
			new ProcessNetwork(templates.getInstanceOf("procNet"), this);
		// Set up maps
		procObjects = 
			new LinkedHashMap<String, ProcessObject>();
		chanDefMap = 
			new LinkedHashMap<String, ChannelDef>();
		constCharDefMap =
			new LinkedHashMap<String, ConstCharDef>();
		constIntDefMap =
			new LinkedHashMap<String, ConstIntDef>();
		constStringDefMap =
			new LinkedHashMap<String, ConstStringDef>();
		extBaseDefMap =
			new LinkedHashMap<String, ExtBaseDef>();
		functionDefMap =
			new LinkedHashMap<String, FunctionDef>();
		functionSTMap =
			new LinkedHashMap<String, ST>();
		listDefMap = 
			new LinkedHashMap<String, ListDef>();
		processDefMap = 
			new LinkedHashMap<String, ProcessDef>();
		setDefMap =
			new LinkedHashMap<String, SetDef>();
		msgStructDefMap = 
			new LinkedHashMap<String, MsgStructDef>();
		procInvocationDefMap =
			new LinkedHashMap<String, ProcInvocationDef>();
		exportedParamMap =
			new LinkedHashMap<String, ParamDef>();
	}

	public void registerNames(String inputName) {
		procNet.registerInputName(inputName);
	}

	public ProcessObject getProcessObject(String s) {
		return procObjects.get(s);
	}

	public ProcessObject newProcessObject(ProcessDef pd, 
			ProcInvocationDef pid) {
		ProcessObject po = 
			new ProcessObject(pd, procNet, pid);
		procObjects.put(pd.name(), po);
		return po;
	}

	public String getSrcFileExt() {
		return ".go";
	}

	public static ST getTemplate(String name) {
		return templates.getInstanceOf(name);
	}

	public ST getGoType(ValueType vt) {
		switch (vt) {
		case INT:
			return getTemplate("int");
		case BOOL:
			return getTemplate("bool");
		case CHAR:
			return getTemplate("char");
		case STRING:
			return getTemplate("string");
		case LIST:
		case RANGED_LIST:
			return getTemplate("cspISQType");
		case SET:
		case RANGED_SET:
			return getTemplate("cspISTType"); 
		}
		return null;
	}

	public ST getGoType(String typeString) {
		switch (typeString) {
		case "Int":
			return getTemplate("int");
		case "Bool":
			return getTemplate("bool");
		case "Char":
			return getTemplate("char");
		case "String":
			return getTemplate("string");
		case "List":
			return getTemplate("cspISQType");
		case "Set":
			return getTemplate("cspISTType");
		}
		return null;
	}

	public void addExportParam(ParamDef prd) {
		exportedParamMap.put(prd.name(), prd);	
	}
}
