/**
 * ValueType
 */
package io.github.jamesdibley.cspider.translator;

public enum ValueType {
	UNKNOWN,
	INT,
	BOOL,
	CHAR,
	STRING,
	SET,
	RANGED_SET,
	LIST,
	RANGED_LIST,
	TUPLE,
	EVENT,
	DATAEVENT;
}
