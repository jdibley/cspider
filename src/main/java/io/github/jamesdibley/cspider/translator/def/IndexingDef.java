/**
 * IndexingDef
 */
package io.github.jamesdibley.cspider.translator.def;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
import io.github.jamesdibley.cspider.parser.gen.CSPMParser.*;
import io.github.jamesdibley.cspider.translator.BaseDef;
import io.github.jamesdibley.cspider.translator.def.*;

public class IndexingDef extends BaseDef {
	private Map<String, SetDef> indexes;

	public IndexingDef(ParserRuleContext ctx, String name) {
		super(ctx, name);
		Map indexes = new LinkedHashMap<String, SetDef>();
	}

	public void addIndexingSet(String name, SetDef sd) {
		indexes.put(name, sd);
	}

	public Set<String> getIndexingNames() {
		return indexes.keySet();
	}

	public SetDef getIndexingSet(String name) {
		return indexes.get(name);
	}
}
