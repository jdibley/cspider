/**
 * Symbol
 */

package io.github.jamesdibley.cspider.translator.symbol;

import io.github.jamesdibley.cspider.translator.BaseDef;

import org.antlr.v4.runtime.*;


public class Symbol {
	protected Scope scope;
	protected String name;
	protected BaseDef def;

	public Symbol(BaseDef def, String name) {
		this.def = def;
		this.name = name;
	}

	public BaseDef def() {
		return def;
	}

	public String name() {
		return name;
	}

	public void setScope(Scope scope) {
		this.scope = scope;
	}

	public Scope scope() {
		return scope;
	}

	public Symbol copy() {
		return new Symbol(def, name);
	}

	public String toString() {
		return def.toString();
	}
}
