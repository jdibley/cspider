/**
 * RangedIntDef
 */ 
package io.github.jamesdibley.cspider.translator.def;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
import io.github.jamesdibley.cspider.translator.BaseDef;

public class RangedIntDef extends BaseDef {
	private String minLimit;
	private String maxLimit;

	public RangedIntDef(ParserRuleContext ctx, String name, 
			String min, String max) {
		super(ctx, name);
		minLimit = min;
		maxLimit = max;
	}

	public String getMin() {
		return minLimit;
	}

	public String getMax() {
		return maxLimit;
	}

	public String toString() {
		return name() + "," 
			+ minLimit + ","
			+ maxLimit;
	}
}
