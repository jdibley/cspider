/**
 * BaseDef
 */

package io.github.jamesdibley.cspider.translator;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;

public abstract class BaseDef {

	private TranState state;
	private ParserRuleContext ctx;
	private ValueType type;
	private String name;
	private boolean global;
	private boolean complete; // Every attribute is gathered
	private boolean excluded; // Excluded from template injections

	public BaseDef(ParserRuleContext ctx, String name) {
		this.ctx = ctx;
		this.name = name;
		this.complete = false;
		this.excluded = false;
		this.global = false;
	}

	public BaseDef def(ParseTree ctx) {
		return state.nodeContextMap.get(ctx);
	}

	public ParserRuleContext ctx() {
		return ctx;
	}

	public void setTranState(TranState state) {
		this.state = state;
	}
	
	public TranState tranState() {
		return state;
	}

	public boolean complete() {
		return complete;
	}

	public void markComplete() {
		this.complete = true;
	}

	public boolean excluded() {
		return excluded;
	}
	
	public void markExcluded() {
		this.excluded = true;
	}

	public void setGlobal() {
		global = true;
	}

	public boolean global() {
		return global;
	}

	public void setType(ValueType type) {
		this.type = type;
	}

	public ValueType type() {
		return type;
	}

	public String name() {
		return name;
	}

	public String defStateToString() {
		StringBuilder sb = new StringBuilder();
		sb.append("(");
		if (complete()) {
			sb.append("C");
		}
		if (excluded()) {
			sb.append("E");
		}
		sb.append(")");
		return sb.toString();
	}

	public String toString() {
		return defStateToString() ;
	}
}
