/**
 * AlternativeDef
 */
package io.github.jamesdibley.cspider.translator.def;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
import io.github.jamesdibley.cspider.parser.gen.*;
import io.github.jamesdibley.cspider.parser.gen.CSPMParser.*;
import io.github.jamesdibley.cspider.translator.BaseDef;


public class AlternativeDef extends BaseDef {
	public boolean guarded = false;
	public boolean requiresProxyChannel = false;
	public ExpressionContext guardCtx;
	public ExpressionContext first;
	public ExpressionContext rest;

	public AlternativeDef(ParserRuleContext ctx, String name) {
		super(ctx, name);
		if (ctx instanceof ExprGuardedContext) {
			ExprGuardedContext egc = (ExprGuardedContext) ctx;
			guarded = true;
			guardCtx = egc.expression(0);
			if (egc.expression(1) instanceof ExprPrimaryContext) {
				requiresProxyChannel = true;
				first = null;
				rest = egc.expression(1);
			} else if (egc.expression(1) instanceof ExprPrefixContext) {
				ExprPrefixContext epc = (ExprPrefixContext) egc.expression(1);
				first = epc.expression(0);
				rest = epc.expression(1);
			}
		} else if (ctx instanceof ExprPrefixContext) {
			ExprPrefixContext epc = (ExprPrefixContext) ctx;
			first = epc.expression(0);
			rest = epc.expression(1);
		} else if (ctx instanceof ExprPrimaryContext) {
			ExprPrimaryContext eprc = (ExprPrimaryContext) ctx;
			requiresProxyChannel = true;
			first = null;
			rest = eprc;
		}
	}

	public String toString() {
		String g = "";
		String rPC = "";
		if (guarded) { g = "g"; }
		if (requiresProxyChannel) { rPC = "rPC"; }
		return name() + "(" + g + rPC + ")";
	}
}
