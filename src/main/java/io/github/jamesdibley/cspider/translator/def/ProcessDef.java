/**
 * ProcessDef
 */
package io.github.jamesdibley.cspider.translator.def;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.LinkedHashMap;
import java.util.Set;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import io.github.jamesdibley.cspider.parser.gen.*;
import io.github.jamesdibley.cspider.parser.gen.CSPMParser.*;
import io.github.jamesdibley.cspider.translator.BaseDef;
import io.github.jamesdibley.cspider.translator.symbol.Scope;

public class ProcessDef extends BaseDef {
	private static final Logger logger = LogManager.getLogger(ProcessDef.class);

	private List<ParamDef> params;
	private ProcessDef parent;
	private Scope scope;
	private Scope containedScope;
	private boolean requiresProcessObject;
	// stores anything defined in the process' LDE
	private Map<String, BaseDef> localDefs; 
	// referenced single channels ("chanends" in XMOS speak)
	private Map<String, BaseDef> chanRefs;
	// referenced channel -arrays- 
	private Map<String, BaseDef> indexedChanRefs;
	// LDE equivalents
	private Map<String, BaseDef> LDEchanRefs;
	private Map<String, BaseDef> LDEindexedChanRefs;
	// Public names for channel struct members
	private Map<String, String> publicChanNames;
	// referenced sets
	private Map<String, SetDef> setRefs;
	// referenced lists
	private Map<String, ListDef> listRefs;
	// referenced consts
	private Map<String, BaseDef> constRefs;
	// referenced externals	
	private Map<String, ExtBaseDef> extRefs;
	// referenced functions
	private Map<String, FunctionDef> funcRefs;
	// Composition process-specific attributes below
	// local renamings for channel ends
	private Map<String, String> chanRenames;
	// deduplicated references to each proc invocation
	private Map<String, ProcInvocationDef> procInvocations;
	private Map<String, List<String>> indexes;
	private ExpressionListContext indexExprs;
	private ExpressionContext indexingID;
	// Records initial state for processes that require process objects
	private ProcInvocationDef initialState;

	private boolean waitGroupAssigned;
	private CompType compType;
	private boolean nestedComp;
	private int procInvocCtr = 0;

	public ProcessDef(PatternDeclContext ctx, ProcessDef parent, 
			Scope scope, String name) {
		super(ctx, name);
		this.parent = parent;
		this.scope = scope;
		containedScope = null;
		params = new ArrayList<ParamDef>();
		localDefs = new LinkedHashMap<String, BaseDef>();
		chanRefs = new LinkedHashMap<String, BaseDef>();
		indexedChanRefs = new LinkedHashMap<String, BaseDef>();
		LDEchanRefs = new LinkedHashMap<String, BaseDef>();
		LDEindexedChanRefs = new LinkedHashMap<String, BaseDef>();
		publicChanNames = new LinkedHashMap<String, String>();
		setRefs = new LinkedHashMap<String, SetDef>();
		listRefs = new LinkedHashMap<String, ListDef>();
		extRefs = new LinkedHashMap<String, ExtBaseDef>();
		constRefs = new LinkedHashMap<String, BaseDef>();
		funcRefs = new LinkedHashMap<String, FunctionDef>();
		chanRenames = new LinkedHashMap<String, String>();
		procInvocations = new LinkedHashMap<String, ProcInvocationDef>();
		indexes = new LinkedHashMap<String, List<String>>();
		indexExprs = null;
		compType = CompType.UNKNOWN;
		nestedComp = false;
	}

	public ProcessDef(ParametricPatternDeclContext ctx, 
			ProcessDef parent, Scope scope, String name) {
		super(ctx, name);
		this.parent = parent;
		this.scope = scope;
		params = new ArrayList<ParamDef>();
		localDefs = new LinkedHashMap<String, BaseDef>();
		chanRefs = new LinkedHashMap<String, BaseDef>();
		indexedChanRefs = new LinkedHashMap<String, BaseDef>();
		LDEchanRefs = new LinkedHashMap<String, BaseDef>();
		LDEindexedChanRefs = new LinkedHashMap<String, BaseDef>();
		publicChanNames = new LinkedHashMap<String, String>();
		setRefs = new LinkedHashMap<String, SetDef>();
		listRefs = new LinkedHashMap<String, ListDef>();
		extRefs = new LinkedHashMap<String, ExtBaseDef>();
		constRefs = new LinkedHashMap<String, BaseDef>();
		funcRefs = new LinkedHashMap<String, FunctionDef>();
		chanRenames = new LinkedHashMap<String, String>();
		procInvocations = new LinkedHashMap<String, ProcInvocationDef>();
		indexes = new LinkedHashMap<String, List<String>>();
		indexExprs = null;
		compType = CompType.UNKNOWN;
		nestedComp = false;
	}

	public void addParam(ParamDef pd) {
		params.add(pd);
	}

	public void setRequiresProcessObject() {
		requiresProcessObject = true;
	}

	public boolean requiresProcessObject() {
		return requiresProcessObject;
	}

	public void setContainedScope(Scope scope) {
		containedScope = scope;
	}

	public Scope containedScope() {
		return containedScope;
	}

	public ProcessDef parent() {
		return parent;
	}

	public void setParent(ProcessDef pd) {
		parent = pd;
	}

	public List<ParamDef> params() {
		return params;
	}

	public Scope getScope() {
		return scope;
	}

	public void setCompType(CompType ct) {
		compType = ct;
	}

	public CompType compType() {
		return compType;
	}

	public void setNestedComp() {
		if (compType != CompType.UNKNOWN && compType != CompType.NOT_COMP) {
			nestedComp = true;
		}
	}

	public boolean nestedComp() {
		return nestedComp;
	}

	public void addLocalDef(String name, BaseDef bd) {
		localDefs.put(name, bd);
		// sum LDE proc's chan refs into the special area
		if (bd instanceof ProcessDef) {
			ProcessDef pd = (ProcessDef) bd;
			for (String s : pd.chanRefKeys()) {
				addLDEChanRef(pd.chanRef(s));
				//this.LDEchanRefs.put(s, pd.chanRef(s));
			}
			for (String s : pd.indexedChanRefKeys()) {
				addLDEIndexedChanRef(pd.indexedChanRef(s));
				//this.LDEindexedChanRefs.put(s, pd.indexedChanRef(s));
			}
		}
	}

	public BaseDef localDef(String name) {
		return localDefs.get(name);
	}

	public Collection<BaseDef> localDefs() {
		return localDefs.values();
	}

	public void addChanRef(BaseDef bd) {
		chanRefs.put(bd.name(), bd);
		//addPublicChanName(bd.name());
	}

	public BaseDef chanRef(String name) {
		return chanRefs.get(name);
	}

	public Set<String> chanRefKeys() {
		return chanRefs.keySet();
	}
	
	public void addLDEChanRef(BaseDef bd) {
		LDEchanRefs.put(bd.name(), bd);
	//	addPublicChanName(bd.name());
	}

	public BaseDef LDEchanRef(String name) {
		return LDEchanRefs.get(name);
	}

	public Set<String> LDEchanRefKeys() {
		return LDEchanRefs.keySet();
	}

	public void addIndexedChanRef(BaseDef bd) {
		indexedChanRefs.put(bd.name(), bd);
		//addPublicChanName(bd.name());
	}

	public BaseDef indexedChanRef(String name) {
		return indexedChanRefs.get(name);
	}

	public Set<String> indexedChanRefKeys() {
		return indexedChanRefs.keySet();
	}
	
	public void addLDEIndexedChanRef(BaseDef bd) {
		LDEindexedChanRefs.put(bd.name(), bd);
		//addPublicChanName(bd.name());
	}

	public BaseDef LDEindexedChanRef(String name) {
		return LDEindexedChanRefs.get(name);
	}

	public Set<String> LDEindexedChanRefKeys() {
		return LDEindexedChanRefs.keySet();
	}

	public void addSetRef(SetDef set) {
		setRefs.put(set.name(), set);
		for (ExtBaseDef ebd : set.extRefs()) {
			extRefs.put(ebd.name(), ebd);
		}
	}

	public SetDef setRef(String setKey) {
		return setRefs.get(setKey);
	}

	public Set<String> setRefKeys() {
		return setRefs.keySet();
	}

	public void addListRef(ListDef list) {
		listRefs.put(list.name(), list);
	}

	public ListDef listRef(String listKey) {
		return listRefs.get(listKey);
	}

	public Set<String> listRefKeys() {
		return listRefs.keySet();
	}

	public void addExtRef(ExtBaseDef ebd) {
		//logger.trace("extRefs before add: " + extRefs);
		extRefs.put(ebd.name(), ebd);
		//logger.trace("extRefs after add: " + extRefs);
	}

	public ExtBaseDef extRef(String refKey) {
		return extRefs.get(refKey);
	}

	public Set<String> extRefKeys() {
		return extRefs.keySet();
	}

	public Set<String> constRefKeys() {
		return constRefs.keySet();
	}

	public BaseDef constRef(String refKey) {
		return constRefs.get(refKey);
	}

	public void addConstRef(BaseDef bd) {
		constRefs.put(bd.name(), bd);
	}

	public Set<String> funcRefKeys() {
		return funcRefs.keySet();
	}

	public FunctionDef funcRef(String refKey) {
		return funcRefs.get(refKey);
	}

	public void addFuncRef(FunctionDef fd) {
		funcRefs.put(fd.name(), fd);
	}

	public void addChanRenaming(String intName, String extName) {
		chanRenames.put(intName, extName);
	}

	public String chanRenaming(String intName) {
		return chanRenames.get(intName);
	}

	public Set<String> chanRenameKeys() {
		return chanRenames.keySet();
	}

	public void addProcInvocation(ProcInvocationDef pid) {
		//logger.trace("adding process invocation: " + pid.name() + procInvocCtr);
		procInvocations.put(pid.name() + procInvocCtr++, pid);
		//logger.trace("just added invoc: " + procInvocations.keySet());
	}

	public Set<String> procInvocKeys() {
		return procInvocations.keySet();
	}

	public ProcInvocationDef procInvocation(String s) {
		return procInvocations.get(s);
	}

	public Map<String, List<String>> indexes() {
		return indexes;
	}	

	public void setIndexes(Map<String, List<String>> indexes) {
		this.indexes = indexes;
	}

	public void setIndexExprs(ExpressionListContext indexes) {
		logger.trace("setIndexExprs: " + indexes.getText());
		indexExprs = indexes;
		ExpressionContext ec = indexes.expression(0);
		if (ec instanceof ExprTypeContext) {
			ExprTypeContext etc = (ExprTypeContext) ec;
			ExpressionContext e = etc.expression();
			indexingID = e;
			logger.trace(name() + " indexingID: " + indexingID.getText());
		}
	}

	public ExpressionListContext indexExprs() {
		return indexExprs;
	}	

	public ExpressionContext indexingID() {
		return indexingID;
	}

	public String toString() {
		return defStateToString() 
			+ "<" + requiresProcessObject + ">" 
			+ compType + localDefs.toString() 
			+ chanRefs.keySet().toString()
			+ indexedChanRefs.keySet().toString() 
			+ LDEchanRefs.keySet().toString()
			+ LDEindexedChanRefs.keySet().toString()
			+ chanRenames.keySet().toString()
			+ setRefs.keySet().toString()
			+ listRefs.keySet().toString()
			+ extRefs.keySet().toString()
			+ funcRefs.keySet().toString()
			;
	}

}
