/**
 * VarSetDef
 */
package io.github.jamesdibley.cspider.translator.def.var;

import io.github.jamesdibley.cspider.translator.ValueType;
import io.github.jamesdibley.cspider.translator.def.VarBaseDef;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;

public class VarSetDef extends VarBaseDef {
	public ValueType memberType;

	public VarSetDef(ParserRuleContext ctx, String name) {
		super(ctx, ValueType.SET, name);
	}

	public void setMemberType(ValueType memberType) {
		this.memberType = memberType;
		this.markComplete();
	}	

	public String toString() {
		String member = "";
		switch (memberType) {
		case INT: 
			member = "map[int]bool";
		case BOOL:
			member = "map[bool]bool";
		case CHAR:
			member = "map[rune]bool";
		}
		return member; 
	}
}
