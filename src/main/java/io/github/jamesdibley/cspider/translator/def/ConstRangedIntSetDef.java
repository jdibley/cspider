/**
 * ConstRangedIntSetDef
 */
package io.github.jamesdibley.cspider.translator.def;

import io.github.jamesdibley.cspider.parser.gen.CSPMParser.PatternDeclContext;
import io.github.jamesdibley.cspider.parser.gen.CSPMParser.ChanDeclWithSpecContext;
import io.github.jamesdibley.cspider.translator.BaseDef;
import io.github.jamesdibley.cspider.translator.ValueType;

public class ConstRangedIntSetDef extends BaseDef {
	public String beginExpr;
	public String endExpr;
	public ValueType memberType;

	public ConstRangedIntSetDef(PatternDeclContext ctx, String begin, 
			String end, String name) {
		super(ctx, name);
		beginExpr = begin;
		endExpr = end;
		setType(ValueType.SET);
		memberType = ValueType.INT;
		markComplete();
	}

	public ConstRangedIntSetDef(ChanDeclWithSpecContext ctx, String begin, 
			String end, String name) {
		super(ctx, name);
		beginExpr = begin;
		endExpr = end;
		markComplete();
	}

	public String toString() {
		return defStateToString() + "-> (" + 
			beginExpr + ":" + endExpr + ")[" + memberType + "]";
	}

	public ValueType getMemberType() {
		return ValueType.INT;
	}
}
