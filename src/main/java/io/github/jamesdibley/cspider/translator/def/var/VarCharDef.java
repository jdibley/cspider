/**
 * VarCharDef
 */
package io.github.jamesdibley.cspider.translator.def.var;

import io.github.jamesdibley.cspider.translator.ValueType;
import io.github.jamesdibley.cspider.translator.def.VarBaseDef;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;


public class VarCharDef extends VarBaseDef {

	public VarCharDef(ParserRuleContext ctx, String name) {
		super(ctx, ValueType.CHAR, name);
		this.markComplete();
	}
}
