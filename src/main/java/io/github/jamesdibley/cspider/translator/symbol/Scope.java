package io.github.jamesdibley.cspider.translator.symbol;

import io.github.jamesdibley.cspider.translator.BaseDef;
import io.github.jamesdibley.cspider.translator.def.FunctionDef;

import java.util.Map;
import java.util.LinkedHashMap;
import java.util.ArrayList;
import java.util.List;

public class Scope {
	private final int scopeId;

	public Scope parentScope;
	public ScopeType scopeType;
	protected Map<String, Symbol> symbolMap = new LinkedHashMap<String, Symbol>();

	public Scope(Scope parent, ScopeType type, int scopeId) {
		this.parentScope = parent;
		this.scopeType = type;
		this.scopeId = scopeId;
	}

	public void define(BaseDef def, String name) {
		Symbol symbol = new Symbol(def, name);
		define(symbol);
	}

	public void define(Symbol symbol) {
		symbol.setScope(this);
		symbolMap.put(symbol.name, symbol);
	}

	public Symbol resolve(String name) {
		Symbol symbol = symbolMap.get(name);
		if (symbol != null) return symbol;
		if (parentScope != null) {
			return parentScope.resolve(name); 
		} else {
			return null;
		}
	}

	public boolean remove(String name) {
		Symbol symbol = symbolMap.get(name);
		if (symbol != null) {
			symbolMap.remove(name);
			return true;
		} 
		if (parentScope != null) {
			return parentScope.remove(name);
		} else {
			return false;
		}
	}

	public Scope parentScope() {
		return parentScope;
	}

	public ScopeType type() {
		return scopeType;
	}

	public Symbol symbol(String name) {
		return symbolMap.get(name);
	}

	public int getScopeID() {
		return scopeId;
	}

	@Override 
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("scopeId: " + getScopeID() + "\n");
		if (parentScope != null) {
			sb.append("parentScope: [[ "+parentScope.getScopeID()+" ]] \n");
		} else {
			sb.append("parentScope: null\n");
		}
		sb.append("scopeType: " + scopeType.toString() + "\n");
		sb.append("symbolMap:\n");
		for (String s : symbolMap.keySet()) {
			sb.append(s + ": " + (symbolMap.get(s)).toString() + "\n");
		}
		return sb.toString();
	}
}
