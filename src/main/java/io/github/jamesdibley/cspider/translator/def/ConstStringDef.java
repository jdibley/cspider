/**
 * ConstStringDef
 */
package io.github.jamesdibley.cspider.translator.def;

import io.github.jamesdibley.cspider.parser.gen.CSPMParser.PatternDeclContext;
import io.github.jamesdibley.cspider.translator.BaseDef;
import io.github.jamesdibley.cspider.translator.ValueType;

public class ConstStringDef extends BaseDef {
	public String value;
	public ValueType type;

	public ConstStringDef(PatternDeclContext ctx, String value, String name) {
		super(ctx, name);
		this.value = value;
		setType(ValueType.STRING);
		markComplete();
	}
}
