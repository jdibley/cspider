/**
 * CspiderSTErrorListener
 */
package io.github.jamesdibley.cspider.translator.go;

import org.stringtemplate.v4.*;
import org.stringtemplate.v4.misc.STMessage;

public abstract class CspiderSTErrorListener implements STErrorListener {

	public void compileTimeError(STMessage msg) {
	}

	public void runTimeError(STMessage msg) {
	}

	public void IOError(STMessage msg) {
	}

	public void internalError(STMessage msg) {
	}
}
