/**
 * Pass11
 */
package io.github.jamesdibley.cspider.translator.pass;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
import org.antlr.v4.runtime.tree.xpath.XPath;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import io.github.jamesdibley.cspider.parser.gen.*;
import io.github.jamesdibley.cspider.parser.gen.CSPMParser.*;
import io.github.jamesdibley.cspider.translator.def.*;
import io.github.jamesdibley.cspider.translator.symbol.*;
import io.github.jamesdibley.cspider.translator.BaseDef;
import io.github.jamesdibley.cspider.translator.BasePass;
import io.github.jamesdibley.cspider.translator.TranState;

public class Pass11 extends BasePass {
	private static final Logger logger = LogManager.getLogger(Pass11.class);
	private String ldePatternDeclXPath = "//letClause/patternDecl";
	private String ldeParaPatternDeclXPath = "//letClause/parametricPatternDecl";

	public Pass11(TranState tState, ParseTreeWalker walker,
			CSPMParser parser) {
		super(tState, walker, parser);
		logger.trace("Gather up LDEs into their parents");
	}

	public void process(SourcefileContext tree) {
		for (ParseTree t : XPath.findAll((ParseTree) tree, 
					ldePatternDeclXPath, parser)) {
			gatherLDEPatternDefs(t);
		}
		for (ParseTree t : XPath.findAll((ParseTree) tree,
					ldeParaPatternDeclXPath, parser)) {
			gatherLDEPatternDefs(t);
		}
	}

	private void gatherLDEPatternDefs(ParseTree t) {
		// The defs we gather need to be placed in the correct scope
		LetClauseDef lcd = (LetClauseDef) def(t.getParent());
		Scope currentScope = lcd.getScope();

		BaseDef bd = def(t);
		if (bd == null) { logger.error("Null basedef from: " + t.getText()); return; }
		String name = bd.name();
		BaseDef parentbd = def(t.getParent().getParent());
		//logger.trace("gathered: " + name + " [" + bd.toString() + "]");
		
		// Finally we can define these local defs in the correct scope 
		currentScope.define(bd, name);
	
		// Is the BaseDef a ProcessDef? If so, the owner of the lcd
		// must be updated as 'requiring a procStruct'
		checkProcStructRequired(bd, parentbd);

		ProcessDef parent = (ProcessDef) parentbd;
		parent.addLocalDef(bd.name(), bd);

		if (bd instanceof ProcessDef) {
			ProcessDef child = (ProcessDef) bd;
			copyRefs(parent, child);
		}
	}

	private void checkProcStructRequired(BaseDef bd, BaseDef parentbd) {
		if ((parentbd instanceof ProcessDef) && (bd instanceof ProcessDef)) {
			ProcessDef child = (ProcessDef) bd;
			ProcessDef parent = (ProcessDef) parentbd;
			//logger.trace(">>> " + parent.name() 
			//		+ " requires a ProcessObject!!");
			parent.setRequiresProcessObject();
			child.setParent(parent);
			parent.addLocalDef(bd.name(), bd);
		}
	}

	private void copyRefs(ProcessDef parent, ProcessDef child) {
		for (String csrk : child.setRefKeys()) {
			parent.addSetRef(child.setRef(csrk));
		}
		for (String clrk : child.listRefKeys()) {
			parent.addListRef(child.listRef(clrk));
		}
		for (String cerk : child.extRefKeys()) {
			parent.addExtRef(child.extRef(cerk));
		}
		for (String ccrk : child.constRefKeys()) {
			parent.addConstRef(child.constRef(ccrk));
		}
	}
}
