/**
 * Pass14
 */
package io.github.jamesdibley.cspider.translator.pass;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
import org.antlr.v4.runtime.tree.xpath.XPath;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import io.github.jamesdibley.cspider.parser.gen.*;
import io.github.jamesdibley.cspider.parser.gen.CSPMParser.*;
import io.github.jamesdibley.cspider.translator.BaseDef;
import io.github.jamesdibley.cspider.translator.BasePass;
import io.github.jamesdibley.cspider.translator.TranState;
import io.github.jamesdibley.cspider.translator.def.*;
import io.github.jamesdibley.cspider.translator.symbol.*;

public class Pass14 extends BasePass {
	private static final Logger logger = LogManager.getLogger(Pass14.class);
	private String globalPDXPath = 
		"/sourcefile/declaration/patternDecl";
	private String globalPPDXPath = 
		"/sourcefile/declaration/parametricPatternDecl";
	private ProcessDef tmpParent = null;
	private ConstIntDef tmpConstParent = null;
	// walking the parse tree looking for process invocations
	// in compositions
	private boolean tmpRegisteringCPI = false;	
	// walking the parse tree looking for process SUBSTATE
	// invocations
	private boolean tmpRegisteringPSI = false;
	private boolean tmpWithinComposition = false;
	// walking the parse tree looking for function calls
	private boolean tmpRegisteringFC = false; 	
	// snaffling renamings
	private Map<String, String> tmpRenaming = null;
	private Map<ExpressionContext, ExpressionContext> tmpRenamingExprs = null;
	// loading and unloading scope as we traverse processes 
	private Scope tmpScope = null;

	public Pass14(TranState tState, ParseTreeWalker walker,
			CSPMParser parser) {
		super(tState, walker, parser);
		tmpRenaming = new LinkedHashMap<String,String>();
		tmpRenamingExprs = new LinkedHashMap<ExpressionContext, ExpressionContext>();
		logger.trace("Capture process invocations, replicated process "
				+ "compositions, and composition alphabets."); 
	}

	public void process(SourcefileContext tree) {
		registerInterfaceParallelChannels(tree);
		registerReplAlphaParallelChannels(tree);
		registerAlphaParallelChannels(tree);
		registerProcessInvocations(tree, globalPDXPath);
		registerProcessInvocations(tree, globalPPDXPath);
		registerFunctionCalls(tree, globalPDXPath);
		registerFunctionCalls(tree, globalPPDXPath);
	}


	private void registerInterfaceParallelChannels(SourcefileContext tree) {
		String xp = "//expression";
		for (ParseTree t : XPath.findAll((ParseTree) tree, xp, parser)) {
			ExpressionContext ec = (ExpressionContext) t;
			if (!(ec instanceof ExprInterfaceParallelContext)) { 
				continue; 
			}
			ExprInterfaceParallelContext eipc = 
				(ExprInterfaceParallelContext) ec;
			
			List<String> chanNames = 
				getChannelSet(eipc.primaryExpr());

			//logger.trace("Interface parallel: retrieved the following chan names: "
			//		+ chanNames); 
			for (String s : chanNames) {
				List<String> dotExprs = 
					new ArrayList<String>(Arrays.asList(s.split("\\.")));
				String chName = dotExprs.remove(0);
				Symbol chSym = 
					tState.symTable.currentScope().resolve(chName);
				ChannelDef chDef = 
					(ChannelDef) chSym.def();
				chDef.setVisibilityNetwork();
			}
		}
	}

	private void registerReplAlphaParallelChannels(SourcefileContext tree) {
		String xp = "//expression";
		for (ParseTree t : XPath.findAll((ParseTree) tree, xp, parser)) {
			ExpressionContext ec = (ExpressionContext) t;
			if (!(ec instanceof ExprReplAlphaParallelContext)) { 
				continue; 
			}
			ExprReplAlphaParallelContext erapc = 
				(ExprReplAlphaParallelContext) ec;
			
			List<String> chanNames = 
				getChannelSet(erapc.primaryExpr());

//			logger.trace("ReplAlpha: retrieved the following chan names: "
//					+ chanNames); 
			for (String s : chanNames) {
				List<String> dotExprs = 
					new ArrayList<String>(Arrays.asList(s.split("\\.")));
				String chName = dotExprs.remove(0);
				Symbol chSym = 
					tState.symTable.currentScope().resolve(chName);
				ChannelDef chDef = 
					(ChannelDef) chSym.def();
				chDef.setVisibilityNetwork();
			}
		}
	}

	private void registerAlphaParallelChannels(SourcefileContext tree) {
		String xp = "//expression";
		for (ParseTree t : XPath.findAll((ParseTree) tree, xp, parser)) {
			ExpressionContext ec = (ExpressionContext) t;
			if (!(ec instanceof ExprAlphaParallelContext)) { 
				continue; 
			}
			ExprAlphaParallelContext eapc = 
				(ExprAlphaParallelContext) ec;
			
			List<String> chanNames0 = 
				getChannelSet(eapc.primaryExpr(0));
			List<String> chanNames1 =
				getChannelSet(eapc.primaryExpr(1));

			//logger.trace("Alpha: retrieved chan names: "
			//		+ chanNames0 + " and " + chanNames1); 
			for (String s0 : chanNames0) {
				List<String> dotExprs0 = 
					new ArrayList<String>(Arrays.asList(s0.split("\\.")));
				for (String s1 : chanNames1) {
					List<String> dotExprs1 =
						new ArrayList<String>(Arrays.asList(s1.split("\\.")));
					String chName0 = dotExprs0.remove(0);
					String chName1 = dotExprs1.remove(0);
					if (chName0.matches(chName1)) {
						Symbol chSym = 
							tState.symTable.currentScope().resolve(chName0);
						ChannelDef chDef = 
							(ChannelDef) chSym.def();
						chDef.setVisibilityNetwork();
						//logger.trace("set " + chName0 + " as network channel.");
					}
				}
			}
		}
	}

	private List<String> getChannelSet(PrimaryExprContext pec) {
		List<String> setParts = new ArrayList<String>();

		if (pec instanceof PExprSetExprContext) {
			PExprSetExprContext pesec = 
				(PExprSetExprContext) pec;
			String setStr = null;
			
			if (pesec.setExpr() instanceof SetContext) {
				SetContext sc = (SetContext) pesec.setExpr();
				setStr = sc.expressionList().getText();
			} else if (pesec.setExpr() instanceof SetEnumeratedContext) {
				SetEnumeratedContext sec = 
					(SetEnumeratedContext) pesec.setExpr();
				setStr = sec.expressionList().getText();
			}
			
			if (setStr == null) {
				return setParts;
			}

			List<String> sExprParts = 
				new ArrayList<String>(Arrays.asList(setStr.split(",")));
			for (String str : sExprParts) {
				setParts.add(str.trim());
			} 
			
			return setParts;
		} else if (pec instanceof PExprNameContext) {
			PExprNameContext penc = (PExprNameContext) pec;
			String setName = penc.Name().getText();

			Symbol sym =
				tState.symTable.currentScope().resolve(setName);
			if (sym == null) { return null; }
			if (sym.def() instanceof SetDef) {
				SetDef sd = (SetDef) sym.def();
				return sd.members();
			} else {
				logger.error(setName + " doesn't resolve to set def."
						+ sym.def());
			}
		} else if (pec instanceof PExprUserFuncCallContext) {
			PExprUserFuncCallContext peufc = 
				(PExprUserFuncCallContext) pec;
			String setName = peufc.Name().getText();

			Symbol sym = 
				tState.symTable.currentScope().resolve(setName);
			if (sym == null) { return null; }
			if (sym.def() instanceof SetDef) {
				SetDef sd = (SetDef) sym.def();
				return sd.members();
			} else {
				logger.error(setName + " doesn't resolve to set def."
						+ sym.def());
			}
		}	
		return null;
	}

	private void registerProcessInvocations(SourcefileContext tree, String xp) {
		//logger.trace("<< registerProcessInvocations with "
		//		+ xp + " and tmpRegisteringCPI");
		for (ParseTree t: XPath.findAll((ParseTree) tree, xp, parser)) {
			tmpRegisteringCPI = true;
			walker.walk(this, t);
		}
		tmpRegisteringCPI = false;
		//logger.trace("tmpRegisteringCPI := false");
		//logger.trace("<<--- registerProcessInvocations with " 
		//		+ xp + " and tmpRegisteringPSI --->>");
		for (ParseTree t: XPath.findAll((ParseTree) tree, xp, parser)) {
			tmpRegisteringPSI = true;
			walker.walk(this, t);
		}
		tmpRegisteringPSI = false;
		//logger.trace("<<--- tmpRegisteringPSI := false --->>");
	}

	private List<ExpressionContext> getInvocArgs(ExpressionContext ec) {
		List<ExpressionContext> args = 
			new ArrayList<ExpressionContext>();
		if (!(ec instanceof ExprPrimaryContext)) {
			logger.error("Unexpected expr in getInvocArgs: ",
					ec.getText());
			return args;
		}
		ExprPrimaryContext epc = (ExprPrimaryContext) ec;
		PrimaryExprContext pec = epc.primaryExpr();
		if (pec instanceof PExprNameContext) { // no args
			return args;
		} else if (!(pec instanceof PExprUserFuncCallContext)) {
			logger.error("Unexpected expr in getInvocArgs: ",
					ec.getText());
			return args;
		}
		PExprUserFuncCallContext ctx = 
				(PExprUserFuncCallContext) pec;
		for (ExpressionContext e :
				ctx.expressionList().expression()) {
			args.add(e);
		}
		//logger.trace("getInvocArgs: fetched " + args.size() 
		//		+ " args for " + ec.getText());
		return args;
		
	}

	private List<ExpressionContext> getInvocArgs(PrimaryExprContext pec) {
		List<ExpressionContext> args = 
			new ArrayList<ExpressionContext>();
		if (pec instanceof PExprNameContext) {
			return args;
		} else if (!(pec instanceof PExprUserFuncCallContext)) {
			logger.error("Unexpected expr in getInvocArgs: ",
					pec.getText());
			return args;
		}
		PExprUserFuncCallContext ctx = (PExprUserFuncCallContext) pec;
		for (ExpressionContext e : ctx.expressionList().expression()) {
			args.add(e);
		}
		return args;
	}

	private void storeInvocation(ExpressionContext ec, ProcessDef pd,
			List<String> invocationParts, 
			Map<String,String> renaming) {
		List<ExpressionContext> invocArgs = null;
		if (ec instanceof ExprRenameContext) {
			ExprRenameContext erc =
				(ExprRenameContext) ec;
			invocArgs = getInvocArgs(erc.expression());
		} else if (ec instanceof ExprPrimaryContext) {
			invocArgs = getInvocArgs(ec);
		}
		
		ProcInvocationDef procInvoc0 = 
			new ProcInvocationDef(ec, pd, invocationParts, 
					invocArgs, renaming, tmpRenamingExprs, tmpRegisteringPSI);
		setDef(ec, procInvoc0);	
	}

	private void storeInvocation(PrimaryExprContext pec, ProcessDef pd,
			List<String> invocationParts,
			Map<String, String> renaming) {
		List<ExpressionContext> invocArgs = null;
		invocArgs = getInvocArgs(pec);
		ProcInvocationDef procInvoc0 =
			new ProcInvocationDef(pec, pd, invocationParts,
					invocArgs, renaming, tmpRenamingExprs, tmpRegisteringPSI);
		setDef(pec, procInvoc0);
	}

	private void storeRenamings(ProcessDef pd, Map<String, String> renamings) {
		for (String k : renamings.keySet()) {
			pd.addChanRenaming(k, renamings.get(k));
		}
	}

	private ProcessDef getProcDef(String procName) {
		Symbol sym = 
			tState.symTable.currentScope().resolve(procName);
		if (sym == null) { return null; }
		
		if (!(sym.def() instanceof ProcessDef)) {
			return null;
		} 
		return (ProcessDef) sym.def();
	}

	private List<String> getProcParts(ExprPrimaryContext epc, 
			List<String> procParts) {
		if (epc.primaryExpr() instanceof PExprUserFuncCallContext) {
			PExprUserFuncCallContext peufc = 
				(PExprUserFuncCallContext) epc.primaryExpr();
			String argStr = 
				peufc.expressionList().getText();
			List<String> argParts =
				new ArrayList<String>(Arrays.asList(argStr.split(",")));
			procParts.add(peufc.Name().getText());
			for (String str : argParts) {
				procParts.add(str.trim());
			}
		} else if (epc.primaryExpr() instanceof PExprNameContext) {
			procParts.add(epc.getText());
		} else {
			logger.error("Wrong expr in getProcParts.");
		}
		return procParts;

	}

	private List<String> getProcessLabel(ExpressionContext ec) {	
		List<String> procParts = new ArrayList<String>();
	//	logger.trace(ec.getText());
		if (ec instanceof ExprPrimaryContext) {
			ExprPrimaryContext epc = (ExprPrimaryContext) ec;
			return getProcParts(epc, procParts);
		} else if (ec instanceof ExprRenameContext) {
			// First catalogue renaming clause, then walk
			// the process expression
			ExprRenameContext erc = (ExprRenameContext) ec;
			
			String renamingStr = erc.renamingClause().getText();
			renamingStr = renamingStr.substring(2, renamingStr.length()-2);
			List<String> renamingParts = 
				new ArrayList<String>(Arrays.asList(renamingStr.split(",")));
			for (String str : renamingParts) {
				List<String> renaming = 
					new ArrayList<String>(Arrays.asList(str.split("<-")));
			 	tmpRenaming.put(renaming.get(0).trim(), renaming.get(1).trim());
			}

			// !!! Explanation:
			//
			// Typical situation which lands us here is something like
			// 	P(0, <>, false) [[ a <- b, c <- d.0 ]]
			//
			// Having snaffled the renaming clause into tmpRenaming, we now go about
			// grabbing the process label and arguments
			if (erc.expression() instanceof ExprPrimaryContext) {
				ExprPrimaryContext epc = 
					(ExprPrimaryContext) erc.expression();
				return getProcParts(epc, procParts);
			} else {
				logger.error("Unexpected EC in renaming clause!");
			}

			// grab renaming expressions -as expressions-
			for (RenamingContext rc : erc.renamingClause().renaming()) {
				tmpRenamingExprs.put(rc.expression(0), rc.expression(1));
			}
			logger.trace("tmpRenamingExprs: " + tmpRenamingExprs);

		}
		return procParts;
	}

	private void captureIndexes(String indexingStr, ProcessDef indexedProc) {
		indexedProc.setIndexes(getIndexingMap(indexingStr));
		//logger.trace(indexedProc.indexes());
	}

	private Map<String, List<String>> getIndexingMap(String indexingString) {
		List<String> indexExprs =
			new ArrayList<String>(Arrays.asList(indexingString.split("\\,")));
		Map<String, List<String>> indexingMap =
			new LinkedHashMap<String, List<String>>();

		for (String s : indexExprs) {
			List<String> indexExprParts =
				new ArrayList<String>(Arrays.asList(s.split(":")));
			String rangeString = indexExprParts.get(1);
			rangeString = rangeString.substring(1, rangeString.length()-1);
			List<String> indexValues =
				new ArrayList<String>(Arrays.asList(rangeString.split("\\.\\.")));
			indexingMap.put(indexExprParts.get(0), indexValues);
		}
		return indexingMap;
	}

	private void registerFunctionCalls(SourcefileContext tree, String xp) {
		//logger.trace("<< registerFunctionCalls");
		//logger.trace("tmpRegisteringFC := true");
		tmpRegisteringFC = true;
		for (ParseTree t: XPath.findAll((ParseTree) tree, xp, parser)) {
			walker.walk(this, t);
		}
		//logger.trace("tmpRegisteringFC := false");
		tmpRegisteringFC = false;
	}

	// Listener overrides 
	
	@Override
	public void enterPatternDecl(PatternDeclContext ctx) {
		//if (!(tmpRegisteringCPI || tmpRegisteringPSI)) { return; }
		BaseDef bd = def(ctx);
		if (bd instanceof ProcessDef) {
			tmpParent = (ProcessDef) bd;
			tmpScope = tmpParent.containedScope();
		} else if (bd instanceof ConstIntDef) {
			tmpConstParent = (ConstIntDef) bd; 
		}
	}

	@Override
	public void exitPatternDecl(PatternDeclContext ctx) {
		if (tmpParent != null) {
			tmpScope = tmpParent.getScope();
			tmpParent = null;
		}
		tmpConstParent = null;
	}

	@Override
	public void enterParametricPatternDecl(ParametricPatternDeclContext ctx) {
		//if (!(tmpRegisteringCPI || tmpRegisteringPSI)) { return; }
		BaseDef bd = def(ctx);
		if (!(bd instanceof ProcessDef)) { return; }
		tmpParent = (ProcessDef) bd;
		tmpScope = tmpParent.containedScope();
	}

	@Override
	public void exitParametricPatternDecl(ParametricPatternDeclContext ctx) {
		if (tmpParent != null) {
			tmpScope = tmpParent.getScope();
			tmpParent = null;
		}
	}

	@Override
	public void enterExprParens(ExprParensContext ctx) {
		if (ctx.getParent() instanceof ExprRenameContext) {
			//logger.trace("<<< We found a big parens expression with a renaming just outside it >>>");
			ExprRenameContext erc = (ExprRenameContext) ctx.getParent();	
			String renamingStr = erc.renamingClause().getText();
			renamingStr = renamingStr.substring(2, renamingStr.length()-2);
			List<String> renamingParts = 
				new ArrayList<String>(Arrays.asList(renamingStr.split(",")));
			for (String str : renamingParts) {
				List<String> renaming = 
					new ArrayList<String>(Arrays.asList(str.split("<-")));
				//logger.trace("[" + renaming.get(0).trim() + "] <- [" 
				//		+ renaming.get(1).trim() + "]");
			 	tmpRenaming.put(renaming.get(0).trim(), renaming.get(1).trim());
			}
			 
			// Grab renaming expressions -as expressions-
			for (RenamingContext rc : erc.renamingClause().renaming()) {
				tmpRenamingExprs.put(rc.expression(0), rc.expression(1));
			}
			logger.trace("tmpRenamingExprs: " + tmpRenamingExprs);
		}
	}

	@Override
	public void enterExprAlphaParallel(ExprAlphaParallelContext ctx) {
		tmpWithinComposition = true;
	}

	@Override
	public void exitExprAlphaParallel(ExprAlphaParallelContext ctx) {
		if (!(tmpRegisteringCPI)) { 
			tmpWithinComposition = false;
			return; 
		}
		//logger.trace("Alpha parallel: " + ctx.getText());
		List<String> proc0Parts = 
			getProcessLabel(ctx.expression(0));
		Map<String, String> proc0renaming = 
			new LinkedHashMap<String, String>(tmpRenaming);
		List<String> proc1Parts = 
			getProcessLabel(ctx.expression(1));
		Map<String, String> proc1renaming = 
			new LinkedHashMap<String, String>(tmpRenaming);

		ProcessDef proc0 = getProcDef(proc0Parts.remove(0));
		ProcessDef proc1 = getProcDef(proc1Parts.remove(0));
	
		storeInvocation(ctx.expression(0), proc0, 
				proc0Parts, proc0renaming);		
		storeInvocation(ctx.expression(1), proc1,
				proc1Parts, proc1renaming);		

		storeRenamings(proc0, proc0renaming);
		storeRenamings(proc1, proc1renaming);

		//logger.trace("Walked alpha par comp and grabbed renamings and invocations");
		//logger.trace(proc0);
		//logger.trace((ProcInvocationDef) def(ctx.expression(0)));
		//logger.trace(proc1);
		//logger.trace((ProcInvocationDef) def(ctx.expression(1)));

		tmpRenaming.clear();
		tmpRenamingExprs.clear();
		tmpWithinComposition = false;
	}

	@Override
	public void enterExprInterfaceParallel(ExprInterfaceParallelContext ctx) {
		tmpWithinComposition = true;
	}

	@Override
	public void exitExprInterfaceParallel(ExprInterfaceParallelContext ctx) {
		if (!(tmpRegisteringCPI)) { 
			tmpWithinComposition = false;
			return; 
		}
		//logger.trace("Interface parallel: " + ctx.getText());
		List<String> proc0Parts = 
			getProcessLabel(ctx.expression(0));
		Map<String, String> proc0renaming = 
			new LinkedHashMap<String, String>(tmpRenaming);
		List<String> proc1Parts = 
			getProcessLabel(ctx.expression(1));
		Map<String, String> proc1renaming = 
			new LinkedHashMap<String, String>(tmpRenaming);


		ProcessDef proc0 = getProcDef(proc0Parts.remove(0));
		ProcessDef proc1 = getProcDef(proc1Parts.remove(0));
//		logger.trace(">>> " + ctx.getText() + " >>> tmpParent: " );	
		storeInvocation(ctx.expression(0), proc0, 
				proc0Parts, proc0renaming);		
		storeInvocation(ctx.expression(1), proc1,
				proc1Parts, proc1renaming);		

		storeRenamings(proc0, proc0renaming);
		storeRenamings(proc1, proc1renaming);

		//logger.trace("Walked interface par comp and grabbed "
		//		+ "renamings and invocations");
		//logger.trace((ProcInvocationDef) def(ctx.expression(0)));
		//logger.trace((ProcInvocationDef) def(ctx.expression(1)));
		tmpRenaming.clear();
		tmpRenamingExprs.clear();
		tmpWithinComposition = false;
	}

	@Override
	public void enterExprReplAlphaParallel(ExprReplAlphaParallelContext ctx) {
		tmpWithinComposition = true;
	}

	@Override
	public void exitExprReplAlphaParallel(ExprReplAlphaParallelContext ctx) {
		if (!(tmpRegisteringCPI)) { 
			tmpWithinComposition = false;
			return; 
		}
		//logger.trace("Repl alpha parallel: " + ctx.getText());
		List<String> procParts = 
			getProcessLabel(ctx.expression());
		Map<String, String> procRenaming = 
			new LinkedHashMap<String, String>(tmpRenaming);

		//logger.trace(tmpParent.name());

		ProcessDef proc = getProcDef(procParts.remove(0));
		captureIndexes(ctx.expressionList().getText(), proc);	
		proc.setIndexExprs(ctx.expressionList());

		storeInvocation(ctx.expression(), proc, 
				procParts, procRenaming);
		storeRenamings(proc, procRenaming);
		//logger.trace("Walked replicated alpha par comp and grabbed "
		//		+ "renamings and invocations");
		//logger.trace((ProcInvocationDef) def(ctx.expression()));
		tmpRenaming.clear();
		tmpRenamingExprs.clear();
		tmpWithinComposition = false;
	}

	private void registerFunctionCall(PExprUserFuncCallContext ctx) {
		String name = ctx.Name().getText();
		Symbol sym = tmpScope.resolve(name);
		if (sym == null) {
			return;
		}
		if (!(sym.def() instanceof FunctionDef)) {
			return;
		}
		FunctionDef fd = (FunctionDef) sym.def();
		if (tmpParent != null) {
			if (tmpParent.parent() != null) {
				ProcessDef gp = tmpParent.parent();
				//logger.trace("Adding function ref to: "
				//		+ fd.name() + " to: "
				//		+ gp.name());
				gp.addFuncRef(fd);
				mergeRefSets(fd, gp);
			}
		} else if (tmpConstParent != null) {
			tmpConstParent.addFuncRef(fd);
			logger.trace(">> Added funcRef " + fd.name()
					+ " to const: " + tmpConstParent.name());
		}
	}

	private void registerProcessSubstateInvocation(PExprUserFuncCallContext ctx) {
		String name = ctx.Name().getText();
		Symbol sym = tmpScope.resolve(name);
		if (sym == null) {
			return;
		}
		if (!(sym.def() instanceof ProcessDef)) {
			return;
		}
		ExpressionContext ec = (ExpressionContext) ctx.getParent();
		ProcessDef pd = (ProcessDef) sym.def();
		List<String> procParts = getProcessLabel(ec);
		// In other contexts we -want- the first entry in procParts to help us
		// look up the process. Not so here, so we discard it.
		procParts.remove(0);
		
		Map<String, String> procRenaming =
			new LinkedHashMap<String, String>(tmpRenaming);
		storeInvocation(ctx, pd, procParts, procRenaming);	
		tmpRenaming.clear();
		tmpRenamingExprs.clear();
	}

	@Override
	public void exitPExprUserFuncCall(PExprUserFuncCallContext ctx) {
		//logger.trace("########## exitPExprUserFuncCal: " + ctx.getText()
		//		+ "\n  tRFC: " + tmpRegisteringFC
		//		+ "  tPSI: " + tmpRegisteringPSI
		//		+ "  tWC: " + tmpWithinComposition);
		if (tmpRegisteringFC) {
			registerFunctionCall(ctx);
		} else if (tmpRegisteringPSI && !tmpWithinComposition) {
			registerProcessSubstateInvocation(ctx);
		}
	}

	private void mergeRefSets(FunctionDef fd, ProcessDef pd) {
		//logger.trace("merging ref sets from function referenced somewhere in process " + pd.name());
		for (String s : fd.constRefKeys()) {
			pd.addConstRef(fd.constRef(s));
		}
		for (String s : fd.setRefKeys()) {
			pd.addSetRef(fd.setRef(s));
		} 
		for (String s : fd.listRefKeys()) {
			pd.addListRef(fd.listRef(s));
		}
		for (String s : fd.extRefKeys()) {
			pd.addExtRef(fd.extRef(s));
		}
	}
}

