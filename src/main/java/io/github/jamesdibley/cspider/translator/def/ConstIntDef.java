/**
 * ConstIntDef
 */
package io.github.jamesdibley.cspider.translator.def;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import io.github.jamesdibley.cspider.parser.gen.CSPMParser.PatternDeclContext;
import io.github.jamesdibley.cspider.translator.BaseDef;
import io.github.jamesdibley.cspider.translator.ValueType;

public class ConstIntDef extends BaseDef {
	public String value;
	private Map<String, ExtBaseDef> extRefs;
	private Map<String, FunctionDef> funcRefs;

	public ConstIntDef(PatternDeclContext ctx, String value, String name) {
		super(ctx, name);
		this.value = value; 
		extRefs = new LinkedHashMap<String, ExtBaseDef>();
		funcRefs = new LinkedHashMap<String, FunctionDef>();
		setType(ValueType.INT);
		markComplete();
	}

	public void addExtRef(ExtBaseDef ebd) {
		extRefs.put(ebd.name(), ebd);
	}

	public Collection<ExtBaseDef> extRefs() {
		return extRefs.values();
	}

	public void addFuncRef(FunctionDef fd) {
		funcRefs.put(fd.name(), fd);
	}

	public Collection<FunctionDef> funcRefs() {
		return funcRefs.values();
	}
}
