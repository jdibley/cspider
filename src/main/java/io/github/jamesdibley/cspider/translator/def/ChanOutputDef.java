/**
 * ChanOutputDef
 */ 
package io.github.jamesdibley.cspider.translator.def;

import java.util.ArrayList;
import java.util.List;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
import io.github.jamesdibley.cspider.parser.gen.*;
import io.github.jamesdibley.cspider.parser.gen.CSPMParser.*;
import io.github.jamesdibley.cspider.translator.BaseDef;

public class ChanOutputDef extends BaseDef {
	private ChannelDef dst;
	private boolean requiresMsgStruct;
	private List<String> indexes;
	private List<String> expressions;
	private List<ExpressionContext> indexExprs;
	private List<ExpressionContext> expressionExprs;
	private String valueType;

	public ChanOutputDef(ParserRuleContext ctx, String name, 
			ChannelDef dst) {
		super(ctx, name);
		this.dst = dst;
		indexes = new ArrayList<String>();
		expressions = new ArrayList<String>();
		indexExprs = new ArrayList<ExpressionContext>();
		expressionExprs = new ArrayList<ExpressionContext>();

		switch(dst.chanType()) {
		case EVENT:
		case INDEXED_EVENT:
		case DATA:
		case INDEXED_DATA:
			requiresMsgStruct = false;
			break;
		case MULTI_DATA:
		case INDEXED_MULTI_DATA:
			requiresMsgStruct = true;
			break;
		}

		switch(dst.valueType()) {
		case INT:
			valueType = "Int";
			break;
		case BOOL:
			valueType = "Bool";
			break;
		case CHAR:
			valueType = "Char";
			break;
		case EVENT:
			valueType = "Event";
			break;
		case UNKNOWN:
			valueType = "Unknown"; 
			break;
		}
	}

	public ChannelDef dst() {
		return dst;
	}

	public String valueType() {
		return valueType;
	}

	public void setRequiresMsgStruct() {
		requiresMsgStruct = true;
	}

	public boolean requiresMsgStruct() {
		return requiresMsgStruct; 
	}

	public void addIndex(String idx) {
		indexes.add(idx);
	}

	public List<String> indexes() {
		return indexes;
	}

	public void addIndexExpr(ExpressionContext ctx) {
		indexExprs.add(ctx);
	}

	public List<ExpressionContext> indexExprs() {
		return indexExprs;
	}

	public void addExpression(String id) {
		expressions.add(id);
	}

	public List<String> expressions() {
		return expressions;
	}

	public void addExpressionExpr(ExpressionContext ctx) {
		expressionExprs.add(ctx);
	}

	public List<ExpressionContext> expressionExprs() {
		return expressionExprs;
	}

	public String expression(int i) {
		if (i < expressions.size()) {
			return expressions.get(i);
		}
		return null;
	}

	public String toString() {
		return name() + defStateToString() + "->"
			+ " (" + requiresMsgStruct + ") " 
			+ " " + valueType + " " 
			+ " " + indexes + " " 
			+ expressions;
	}
}
