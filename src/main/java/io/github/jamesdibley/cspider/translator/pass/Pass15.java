/**
 * Pass15
 */
package io.github.jamesdibley.cspider.translator.pass;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
import org.antlr.v4.runtime.tree.xpath.XPath;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import io.github.jamesdibley.cspider.parser.gen.*;
import io.github.jamesdibley.cspider.parser.gen.CSPMParser.*;
import io.github.jamesdibley.cspider.translator.BasePass;
import io.github.jamesdibley.cspider.translator.TranState;


public class Pass15 extends BasePass {
	private static final Logger logger = LogManager.getLogger(Pass15.class);
	private String xpath0 = "/sourcefile/declaration/patternDecl//primaryExpr";
	private String xpath1 = "/sourcefile/declaration/parametricPatternDecl//primaryExpr";

	public Pass15(TranState tState, ParseTreeWalker walker,
			CSPMParser parser) {
		super(tState, walker, parser);
		logger.trace("Catalogue process invocations for process substates");
	}

	public void process(SourcefileContext tree) {
		for (ParseTree t : XPath.findAll((ParseTree) tree, xpath0, parser)) {
			PrimaryExprContext pec = (PrimaryExprContext) t;
			if (pec instanceof PExprUserFuncCallContext) {
				processCall(pec);
			} else if (pec instanceof PExprNameContext) {
				processName(pec);
			}
		}
	}

	private void processCall(PrimaryExprContext pec) {
		PExprUserFuncCallContext ctx = 
			(PExprUserFuncCallContext) pec;
	}

	private void processName(PrimaryExprContext pec) {
		PExprNameContext ctx = (PExprNameContext) pec;
	
	}
}
