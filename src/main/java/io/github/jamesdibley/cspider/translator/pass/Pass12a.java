/**
 * Pass12a
 */
package io.github.jamesdibley.cspider.translator.pass;

import java.lang.Boolean;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
import org.antlr.v4.runtime.tree.xpath.XPath;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import io.github.jamesdibley.cspider.parser.gen.*;
import io.github.jamesdibley.cspider.parser.gen.CSPMParser.*;
import io.github.jamesdibley.cspider.translator.BaseDef;
import io.github.jamesdibley.cspider.translator.BasePass;
import io.github.jamesdibley.cspider.translator.TranState;
import io.github.jamesdibley.cspider.translator.ValueType;
import io.github.jamesdibley.cspider.translator.def.*;
import io.github.jamesdibley.cspider.translator.symbol.*;

public class Pass12a extends ProcFuncCommon {
	private static final Logger logger = LogManager.getLogger(Pass12a.class);
	private String xpath = "/sourcefile/declaration/parametricPatternDecl";
	private Map<String, Boolean> tmpChanRefs;
	// First pass captures set functions defined in terms of known symbols
	// Second pass captures set functions defined in terms of other set functions.
	private List<ParseTree> secondPassSubtrees;
	private boolean secondPassNeeded;
	private boolean secondPass;
	private String tmpName;

	public Pass12a(TranState tState, ParseTreeWalker walker,
			CSPMParser parser) {
		super(tState, walker, parser);
		tmpChanRefs = new LinkedHashMap<String, Boolean>();
		secondPassSubtrees = new ArrayList<ParseTree>();
		secondPassNeeded = false;
		logger.trace("Cataloguing functions that define sync sets");
	}

	public void process(SourcefileContext tree) {
		processSyncSetFunctions(tree, xpath);	
	}

	// All functions that define sync sets are declared globally
	private void processSyncSetFunctions(SourcefileContext tree, 
			String xpath) {
		secondPass = false;
		for (ParseTree t : XPath.findAll(tree, xpath, parser)) {
			secondPassNeeded = false;

			ParametricPatternDeclContext ppdc = 
				(ParametricPatternDeclContext) t;
			NamePatternContext npc = ppdc.patternLHS().namePattern();

			if (tState.isInMap(t)) { continue; }
			if (!isParametricPattern(npc)) {
				logger.error("Unsupported decl found. "
						+ "Check input for type "
						+ "annotations. " 
						+ npc.getText());
				continue;
			}
			NpArgsContext nac = (NpArgsContext) npc;
			tmpName = getTmpName(npc);

			String patternType = ppdc.expression(0).getText();
			// Skip functions that don't return event sets
			// (A double-check, as such functions should already be 
			// in the tState map anyway.)
			if (!(patternType.matches("\\{Event\\}"))) {
				continue;
			}
			
			// Processing of these functions is very crude: 
			// their exact meaning is far more significant to
			// CSPM model-checking than it is to Go translation.
			// What we are interested in are the names of the 
			// channels that provide an interface/alphabet for a
			// given composition. 
			// 	It is not necessary for us to capture 
			// channel indexes, for example, so we don't need to
			// examine parameters defined over this function. 
			// In the case of a channel with one or more index 
			// arrays, we obtain the indices for any I/O operation
			// from translating the process expression and/or 
			// renaming clause. So we simply walk the subtree and
			// catalogue the channel names that appear.
	
			// do the walk
			walker.walk(this, ppdc.expression(1));

			if (secondPassNeeded) {
				//logger.trace("adding " + tmpName 
				//		+ " as second pass subtree.");
				secondPassSubtrees.add(t);
			}

			List<String> chanRefs = new ArrayList<String>();
			for (String str : tmpChanRefs.keySet()) {
				chanRefs.add(str);
			}
			SetDef syncSetDef = 
				new SetDef(ppdc, chanRefs, ValueType.EVENT, tmpName,
						tState.symTable.currentScope());

			syncSetDef.setTranState(tState);
			setDef(ppdc, syncSetDef);
			tState.symTable.currentScope().define(syncSetDef, tmpName);
			tmpChanRefs.clear();
		}

		logger.trace("Now walking second pass subtrees.");
		secondPass = true;
		for (ParseTree t : secondPassSubtrees) {
			ParametricPatternDeclContext ppdc = 
				(ParametricPatternDeclContext) t;
			NamePatternContext npc = ppdc.patternLHS().namePattern();
			NpArgsContext nac = (NpArgsContext) npc;
			tmpName = getTmpName(npc);

			walker.walk(this, ppdc.expression(1));
			
			List<String> chanRefs = new ArrayList<String>();
			for (String str : tmpChanRefs.keySet()) {
				chanRefs.add(str);
			}
			
			SetDef syncSetDef = 
				new SetDef(ppdc, chanRefs, ValueType.EVENT, tmpName,
						tState.symTable.currentScope());

			syncSetDef.setTranState(tState);
			setDef(ppdc, syncSetDef);
			tState.symTable.currentScope().define(syncSetDef, tmpName);	
			tmpChanRefs.clear();
		}
	}

	// Overridden rules for Listener on this pass:
	public void exitSetUnion(SetUnionContext ctx) {
		// Presence of a CSPm set function indicates we need to
		// do a second pass over this subtree once we've gathered
		// all the 'ground-level' declarations
		//
		// TODO: the assumption in this code is that the -only-
		// set function we support (in -these- declarations) is 
		// 'union'. 
		if (!secondPass) {
			//logger.trace(tmpName + " needs a second pass.");
			secondPassNeeded = true;	
		}
	}

	public void exitPExprUserFuncCall(PExprUserFuncCallContext ctx) {
		String name = ctx.Name().getText();
		if (secondPass) {
			Symbol sym = 
				tState.symTable.currentScope().resolve(name);
			if (sym != null && sym.def() instanceof SetDef) {
				SetDef sd = (SetDef) sym.def();
				
				if (sd.memberType() != ValueType.EVENT) { 
					return; 
				}
				for (String str : sd.members()) {
					tmpChanRefs.put(str, true);
				}
			}

		}
	}

	public void exitPExprName(PExprNameContext ctx) {
		String name = ctx.Name().getText();

		if (secondPass) {
			Symbol sym = 
				tState.symTable.currentScope().resolve(name);
			if (sym == null) { return; }

			if (sym.def() instanceof ChannelDef) {
				tmpChanRefs.put(name, true);
			} else if (sym.def() instanceof SetDef) {
				SetDef sd = (SetDef) sym.def();
				
				if (sd.memberType() != ValueType.EVENT) { 
					return; 
				}
				for (String str : sd.members()) {
					tmpChanRefs.put(str, true);
				}
			}
		} else {
			Symbol sym = 
				tState.symTable.currentScope().resolve(name);
			if (sym == null) { return; }

			if (sym.def() instanceof ChannelDef) {
				tmpChanRefs.put(name, true);
			} else if (sym.def() instanceof SetDef) {
				SetDef sd = (SetDef) sym.def();

				if (sd.memberType() != ValueType.EVENT) {
					return;
				}
				for (String str : sd.members()) {
					tmpChanRefs.put(str, true);
				}
			}
		}
	}

}
