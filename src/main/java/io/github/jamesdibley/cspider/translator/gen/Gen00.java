/**
 * Gen00
 */
package io.github.jamesdibley.cspider.translator.gen;

import java.util.ArrayList;
import java.util.List;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
import org.antlr.v4.runtime.tree.xpath.XPath;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import io.github.jamesdibley.cspider.parser.gen.*;
import io.github.jamesdibley.cspider.parser.gen.CSPMParser.*;
import io.github.jamesdibley.cspider.translator.BaseDef;
import io.github.jamesdibley.cspider.translator.BaseGen;
import io.github.jamesdibley.cspider.translator.TranState;
import io.github.jamesdibley.cspider.translator.def.*;
import io.github.jamesdibley.cspider.translator.go.OutputModel;

public class Gen00 extends BaseGen {
	private static final Logger logger = LogManager.getLogger(Gen00.class);
	private String pRoot 
		= "/sourcefile/declaration";
	private String pExtDecl 
		= "/extPatternDecl";
	private String pChanDecl0	
		= "/chanDecl";
	private String pChanDecl1 	
		= "/chanDeclWithSpec";
	private String pPattDecl 	
		= "/patternDecl";
	private String pParaPattDecl
		= "/parametricPatternDecl";
	private OutputModel o = null;

	public Gen00(TranState tState, ParseTreeWalker walker,
			CSPMParser parser) {
		super(tState, walker, parser);
		o = tState.output;
		logger.trace("Populate output model maps of global input model defs"); 
	}

	public void process(SourcefileContext tree) {
		gatherExtDefs(tree);
		gatherChanDefs(tree);
		gatherConstDefs(tree);
		gatherSetDefs(tree);
		gatherListDefs(tree);
		gatherProcFuncDefs(tree);
		gatherProcInvocationDefs(tree);
		gatherParametersForExport();
		
	}

	public void gatherExtDefs(SourcefileContext tree) {
		String xp = pRoot + pExtDecl;
		for (ParseTree t : XPath.findAll((ParseTree) tree, xp, parser)) {
			//logger.trace("found a subtree");
			ExtBaseDef ebd =
				(ExtBaseDef) def(t);
			//logger.trace("gatherExtDefs found an ext base def: "
			//		+ ebd.name());
			o.extBaseDefMap.put(ebd.name(), ebd);
		}
	}

	public void gatherChanDefs(SourcefileContext tree) {
		String xp = pRoot + pChanDecl0;
		for (ParseTree t : XPath.findAll((ParseTree) tree, xp, parser)) {
			ChanDeclContext ctx = (ChanDeclContext) t;
			for (TerminalNode n : ctx.Name()) {
				ChannelDef cd = (ChannelDef) def(n);
				o.chanDefMap.put(cd.name(), cd);
			}
		}
		
		xp = pRoot + pChanDecl1;
		for (ParseTree t : XPath.findAll((ParseTree) tree, xp, parser)) {
			ChanDeclWithSpecContext ctx = 
				(ChanDeclWithSpecContext) t; 
			for (TerminalNode n : ctx.Name()) {
				ChannelDef cd = (ChannelDef) def(n);
				o.chanDefMap.put(cd.name(), cd);
				if (cd.msgStruct() != null) {
					MsgStructDef msd = cd.msgStruct();
					o.msgStructDefMap.put(msd.name(), msd);
				}
			}
		}
	}

	public void gatherConstDefs(SourcefileContext tree) {
		String xp = pRoot + pPattDecl;
		for (ParseTree t : XPath.findAll((ParseTree) tree, xp, parser)) {
			PatternDeclContext pdc = (PatternDeclContext) t;
			BaseDef bd = def(pdc);
			if (bd instanceof ConstCharDef) {
				ConstCharDef ccd = (ConstCharDef) bd;
				o.constCharDefMap.put(ccd.name(), ccd);
			} else if (bd instanceof ConstIntDef) {
				ConstIntDef cid = (ConstIntDef) bd;
				o.constIntDefMap.put(cid.name(), cid);
			} else if (bd instanceof ConstStringDef) {
				ConstStringDef csd = (ConstStringDef) bd;
				o.constStringDefMap.put(csd.name(), csd);
			}
		}
	}

	public void gatherSetDefs(SourcefileContext tree) {
		String xp = pRoot + pPattDecl;
		for (ParseTree t : XPath.findAll((ParseTree) tree, xp, parser)) {
			PatternDeclContext pdc = (PatternDeclContext) t;
			if (def(pdc) instanceof SetDef) {
				SetDef sd = (SetDef) def(pdc);
				if (sd.referencedFromProcs() || sd.referencedFromFuncs()) {
					o.setDefMap.put(sd.name(), sd);
				} else {
				//	logger.trace("only referenced by a channel spec"); 
				}
			}
		}
	}

	public void gatherListDefs(SourcefileContext tree) {
		String xp = pRoot + pPattDecl;
		for (ParseTree t : XPath.findAll((ParseTree) tree, xp, parser)) {
			PatternDeclContext pdc = (PatternDeclContext) t;
			if (def(pdc) instanceof ListDef) {
				ListDef ld = (ListDef) def(pdc);
				o.listDefMap.put(ld.name(), ld);
			}
		}
	}

	public void gatherProcFuncDefs(SourcefileContext tree) {
		String xp = pRoot + pPattDecl;
		for (ParseTree t : XPath.findAll((ParseTree) tree, xp, parser)) {
			PatternDeclContext pdc = (PatternDeclContext) t;
			if (def(pdc) instanceof ProcessDef) {
				ProcessDef pd = (ProcessDef) def(pdc);
				o.processDefMap.put(pd.name(), pd);
			//	switch (pd.compType()) {
			//	case REPL_INTERLEAVED:
			//	case REPL_SYNCHRONISED:
			//	case REPL_ALPHABETISED:
			//	case REPL_INTERFACE:
			//		//logger.trace(pd.indexes());
			//	}
				
			}
		}

		xp = pRoot + pParaPattDecl;
		for (ParseTree t : XPath.findAll((ParseTree) tree, xp, parser)) {
			ParametricPatternDeclContext ppdc = 
				(ParametricPatternDeclContext) t;
			if (def(ppdc) instanceof FunctionDef) {
				FunctionDef fd = (FunctionDef) def(ppdc);
				o.functionDefMap.put(fd.name(), fd);
			} else if (def(ppdc) instanceof ProcessDef) {
				ProcessDef pd = (ProcessDef) def(ppdc);
				o.processDefMap.put(pd.name(), pd);
			//	switch (pd.compType()) {
			//	case REPL_INTERLEAVED:
			//	case REPL_SYNCHRONISED:
			//	case REPL_ALPHABETISED:
			//	case REPL_INTERFACE:
			//		//logger.trace(pd.indexes());
			//	}
			}
		}

	}

	public void gatherProcInvocationDefs(SourcefileContext tree) {
		String xp = "//expression";
		for (ParseTree t : XPath.findAll((ParseTree) tree, xp, parser)) {
			ExpressionContext ec = (ExpressionContext) t; 
			if (def(ec) instanceof ProcInvocationDef) {
				ProcInvocationDef pid = 
					(ProcInvocationDef) def(ec);
				if (!pid.substate()) {
					o.procInvocationDefMap.put(pid.name(), pid);
				}
				//o.procInvocationDefMap.put(pid.name(), pid);
				//switch (pid.proc().compType()) {
				//default:
				//	o.procInvocationDefMap.put(pid.name(), pid);
				//}
			}
		}
		
		//for (ProcInvocationDef p : o.procInvocationDefMap.values()) {
		//	logger.trace(p);
		//}
		
	}

	public void gatherParametersForExport() {
		List<ProcessDef> uninvokedProcs = 
			new ArrayList<ProcessDef>();
		for (ProcessDef p : o.processDefMap.values()) {
			boolean invoked = false;
			for (ProcInvocationDef pid : o.procInvocationDefMap.values()) {
				if (p.name().matches(pid.name())) {
					invoked = true;
				}
			}
			if (!(invoked)) {
				uninvokedProcs.add(p);
			}
		}
		logger.trace("uninvokedProcs: ");
		for (ProcessDef p : uninvokedProcs) {
			logger.trace(p.name());
			for (ParamDef prd : p.params()) {
				o.addExportParam(prd);
				logger.trace("added exportable parameter: " + prd.name());
			}
		}
	}
}
