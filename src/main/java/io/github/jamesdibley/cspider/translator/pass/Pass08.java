/**
 * Pass08
 */
package io.github.jamesdibley.cspider.translator.pass;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
import org.antlr.v4.runtime.tree.xpath.XPath;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import io.github.jamesdibley.cspider.parser.gen.*;
import io.github.jamesdibley.cspider.parser.gen.CSPMParser.*;
import io.github.jamesdibley.cspider.translator.BasePass;
import io.github.jamesdibley.cspider.translator.BaseDef;
import io.github.jamesdibley.cspider.translator.TranState;
import io.github.jamesdibley.cspider.translator.ValueType;
import io.github.jamesdibley.cspider.translator.def.*;
import io.github.jamesdibley.cspider.translator.def.var.*;
import io.github.jamesdibley.cspider.translator.symbol.*;

public class Pass08 extends ProcFuncCommon {
	private static final Logger logger = LogManager.getLogger(Pass08.class);
	private String rootXPath = "/sourcefile/declaration";
	private String paraXPath = "/parametricPatternDecl";
	
	public Pass08(TranState tState, ParseTreeWalker walker,
			CSPMParser parser) {
		super(tState, walker, parser);
		logger.trace("Gather global parameterised processes and functions.");
	}

	public void process(SourcefileContext tree) {
		process((ParseTree) tree);
	}

	private void process(ParseTree tree) {
		processGlobalParametrics(tree, rootXPath+paraXPath);
	}

	private void processGlobalParametrics(ParseTree tree, String xpath) {
		for (ParseTree t : XPath.findAll(tree, xpath, parser)) {
			resetWP();
			String tmpName = null;
			boolean isProcess = false;
			List<ParamDef> tmpParams = new ArrayList<ParamDef>();

			ParametricPatternDeclContext ppdc =
				(ParametricPatternDeclContext) t;
			NamePatternContext npc = ppdc.patternLHS().namePattern();

			if (tState.isInMap(t)) { continue; }
			if (!isParametricPattern(npc)) {
				logger.error("Unsupported decl found. Check "
						+ "input for type annotations. "
						+ npc.getText());
				continue;
			}
			NpArgsContext nac = (NpArgsContext) npc;
			tmpName = getTmpName(npc);

			String patternType = ppdc.expression(0).getText();
			if (patternType.matches("\\{Event\\}")) {
				//logger.trace("Skipping function " 
				//	+ tmpName 
				//	+ " that defines an event set.");
				continue;
			}

		// Push a new scope here to hold any identifiers we encounter
		// in the process expression (as well as an LDE, if it exists,
		// although we don't gather these til a subsequent pass.)
			tState.symTable.pushScope();
			registerTmpParams(ppdc, nac, tmpParams);

			Map<String, String> tmpRenameMap = 
				processRenamingBlock(ppdc);
			//logger.trace("processed renaming block for " + tmpName + ": "  + tmpRenameMap);

			//logger.trace(tmpName + " " + tmpRenameMap);
			ExpressionContext e = ppdc.expression(1);
			classifying = true;
			walker.walk(this, e);
			classifying = false;
		// Having walked the pattern, we now need to decide if it's a 
		// process or a function. There are three criteria for this:
		// 	1. expression(0) matches 'Proc' => Proc
		// 	2. presence of a renaming block => Proc
		// 	3. look at what the walker discovered... 
			String tmpRetType = ppdc.expression(0).getText();

			if (tmpRetType.matches("Proc")) {
				isProcess = true;
			} else if (ppdc.expression() instanceof ExprRenameContext) {
				isProcess = true;
			} else {
				if (walkedProcessDecl) {
					 isProcess = true;
				}
			}
			//logger.trace("Pass08 tmp rename map: " + tmpRenameMap);
			//logger.trace(tmpName);
			if (isProcess) {
				registerParametricProcess(ppdc, tmpName, 
						tmpParams, tmpRenameMap);
			} else {
				registerFunction(ppdc, tmpName, tmpParams);
			}
			tState.symTable.popScope();
			//walkedChanRefs.clear();
			//logger.trace(tmpName + " ... " + walkedCompType);
			walkedSetRefs.clear();
		}
	}
}
