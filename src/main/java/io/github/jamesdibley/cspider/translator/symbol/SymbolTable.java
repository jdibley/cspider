/**
 * SymbolTable
 */
package io.github.jamesdibley.cspider.translator.symbol;

import java.util.Stack;
import java.util.ArrayList;


public class SymbolTable {
	public Stack<Scope> tScopeStack;
	public ArrayList<Scope> tScopeList;
	public int tScopeCtr;

	public SymbolTable() {
		init();
	}

	protected void init() {
		tScopeStack = new Stack<>();
		tScopeList = new ArrayList<>();
		
		tScopeCtr = 0;
		
		Scope globals = new Scope(null, ScopeType.GLOBAL, incrScopeCounter());
		tScopeStack.push(globals);
		tScopeList.add(globals);
	}

	public Scope pushScope() {
		Scope parent = tScopeStack.peek();
		Scope newScope = new Scope(parent, ScopeType.LOCAL, incrScopeCounter());
		
		tScopeStack.push(newScope);
		tScopeList.add(newScope);

		return newScope;
	}

	public Scope pushScope(Scope parent) {
		Scope newScope = new Scope(parent, ScopeType.LOCAL, incrScopeCounter());
		tScopeStack.push(newScope);
		tScopeList.add(newScope);

		return newScope;
	}

	public void popScope() {
		tScopeStack.pop();
	}

	public Scope currentScope() {
		if (tScopeStack.size() > 0) {
			return tScopeStack.peek();
		} 
		return tScopeList.get(0);
	}

	public int getScopeCount() {
		return tScopeCtr;
	}

	protected int incrScopeCounter() {
		tScopeCtr++;
		return tScopeCtr;
	}

	public String dumpEntireTable() {
		StringBuilder sb = new StringBuilder();
		for (Scope scope : tScopeList) {
			sb.append(scope.toString());
			sb.append("\n");
		}
		return sb.toString();
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (Scope scope : tScopeStack) {
			sb.append(scope.toString());			
		}
		return sb.toString();
	}

	@Override
	public SymbolTable clone() {
		SymbolTable nst = new SymbolTable();
		for (Scope scope : tScopeStack) {
			if (scope.type() == ScopeType.GLOBAL) {
				continue;
			} else {
				for (String name : scope.symbolMap.keySet()) {
					Symbol sym = scope.resolve(name);
				}
			}

		}
		return nst;
	}
}
