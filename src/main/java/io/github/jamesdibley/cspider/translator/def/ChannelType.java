/**
 * ChannelType
 */
package io.github.jamesdibley.cspider.translator.def;

public enum ChannelType {
	UNKNOWN,
	EVENT,			// no associated data
				// (implemented in Go as struct{})
	INDEXED_EVENT,		// one index field,
				// no associated data 
				// (implemented in Go as struct{})
	DATA,			// no index field,
				// one value field
				// (implemented in Go as simple int/bool/rune chan)
	INDEXED_DATA,		// one index field,
				// one value field
				// (implemented in Go as simple int/bool/rune chan)
	MULTI_DATA,		// zero index fields,
				// more than one value fields
				// (implemented in Go using named msgStruct)
	INDEXED_MULTI_DATA	// one or more index fields,
				// more than one value fields
				// (implemented in Go using named msgStruct)
}
