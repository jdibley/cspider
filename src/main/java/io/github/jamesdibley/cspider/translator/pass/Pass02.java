/**
 * Pass02
 * Gather simple pattern decls
 */
package io.github.jamesdibley.cspider.translator.pass;

import java.util.LinkedHashMap;
import java.util.Map;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
import org.antlr.v4.runtime.tree.xpath.XPath;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import io.github.jamesdibley.cspider.parser.gen.*;
import io.github.jamesdibley.cspider.parser.gen.CSPMParser.*;
import io.github.jamesdibley.cspider.translator.BasePass;
import io.github.jamesdibley.cspider.translator.TranState;
import io.github.jamesdibley.cspider.translator.def.ConstCharDef;
import io.github.jamesdibley.cspider.translator.def.ConstIntDef;
import io.github.jamesdibley.cspider.translator.def.ConstStringDef;
import io.github.jamesdibley.cspider.translator.def.ExtBaseDef;
import io.github.jamesdibley.cspider.translator.def.ext.*;
import io.github.jamesdibley.cspider.translator.symbol.*;

public class Pass02 extends BasePass {
	private static final Logger logger = LogManager.getLogger(Pass02.class);
	private String rootPath = "/sourcefile/declaration";
	private String extPatternPath = "/extPatternDecl";
	private String literalPath =
		"/patternDecl/expression/primaryExpr/literal";
	private String patternPath = 
		"/patternDecl";
	private String letPath =
		"//letClause";
	private boolean walkedSimpleMathDecl;
	private Map<String, ExtBaseDef> walkedExtRefs;

	public Pass02(TranState tState, ParseTreeWalker walker, CSPMParser parser) {
		super(tState, walker, parser);
		walkedExtRefs = new LinkedHashMap<String, ExtBaseDef>();
		logger.trace("Gather simple pattern decls.");
	}

	public void process(SourcefileContext tree) {
		processGlobalDecls(tree);
		processLDEDecls(tree);
	}

	private void processGlobalDecls(SourcefileContext tree) {
		// gather external var decls
		for (ParseTree t : XPath.findAll((ParseTree) tree, 
					rootPath+extPatternPath, parser)) {
			ExtPatternDeclContext epdc =
				(ExtPatternDeclContext) t;
			String name = epdc.Name().getText();
			String type = epdc.expression().getText();
//			logger.trace("epdc: " + epdc.getText() + "[" + name + "]" + 
//					" [" + type + "]");
			if (type.matches("Int")) {
//				logger.trace("Found external Int declaration");
				ExtIntDef eid = new ExtIntDef(epdc, name);
				eid.setTranState(tState);
				setDef(epdc, eid);
				tState.symTable.currentScope().define(eid, name);
			} else if (type.matches("Bool")) {
//				logger.trace("Found external Bool declaration");
				ExtBoolDef ebd = new ExtBoolDef(epdc, name);
				ebd.setTranState(tState);
				setDef(epdc, ebd);
				tState.symTable.currentScope().define(ebd, name);
			} else if (type.matches("Char")) { 
//				logger.trace("Found external Char declaration");
				ExtCharDef ecd = new ExtCharDef(epdc, name);
				ecd.setTranState(tState);
				setDef(epdc, ecd);
				tState.symTable.currentScope().define(ecd, name);
			} else {
				logger.error("Unsupported type in external "
						+ " pattern declaration: " + type);
				tState.abortTranslation();
			}
		}

		// gather string and char decls
		for (ParseTree t : XPath.findAll((ParseTree) tree, 
					rootPath+literalPath, parser)) {
			PatternDeclContext pdc = 
				(PatternDeclContext) t.getParent().getParent().getParent(); 
			NamePatternContext np = pdc.patternLHS().namePattern();

			if (np == null || !(np instanceof NpNameContext)) {
				logger.trace("Not a simple declaration, skipping: "
						+ np.getText());
				continue;
			}
			if (pdc.letClause() != null) { 
				logger.trace("Not a simple declaration, skipping: "
						+ np.getText());
				continue;
			}
			String name = ((NpNameContext)np).Name().getText();

			if (t instanceof LitCharContext) {
				registerConstCharDecl(pdc, name, 
						(LitCharContext)t, true);
				continue;
			} else if (t instanceof LitStringContext) {
				logger.trace("registering const string decl");
				registerConstStringDecl(pdc, name, 
						(LitStringContext)t, true);
				continue;
			}
		}

		// gather simple math declarations
		for (ParseTree t : XPath.findAll((ParseTree) tree, 
					rootPath+patternPath, parser)) {
			this.walkedSimpleMathDecl = true;
			if (!(t instanceof PatternDeclContext)) {
				logger.error("Wrong subtree in process().");
				continue;
			}
			PatternDeclContext pdc = (PatternDeclContext) t;
			NamePatternContext np = pdc.patternLHS().namePattern();

			if (np == null || !(np instanceof NpNameContext)) {
				logger.trace("Not a simple decl, skipping."
						+ t.getText());
				continue;
			}
			if (pdc.letClause() != null) { 
				//logger.trace("Skipping process definition: "
				//		+ np.getText());
				continue;
			} 

			String name = ((NpNameContext)np).Name().getText();
			// walk pdc.expression();
			ParseTree e = (ParseTree) pdc.expression();
			walker.walk(this, e);
			if (walkedSimpleMathDecl) {
				registerConstIntDecl(pdc, name, 
						pdc.expression().getText(), true);
			} // else not a simple math decl
			this.walkedSimpleMathDecl = true;
			walkedExtRefs.clear();
		}

	}

	private void processLDEDecls(SourcefileContext tree) {
		// Now we deal with const declarations that appear inside 
		// the Let...Within clauses of process or function definitions.
		for (ParseTree t : XPath.findAll((ParseTree) tree,
					letPath+patternPath, parser)) {
			if (!(t instanceof PatternDeclContext)) {
				logger.error("Wrong subtree in letClause.");
				continue;
			}
			// One difference: we don't fetch the name from the
			// parse tree because we -don't want to write to the
			// symbol table yet-. We will set up a <Def> and 
			// annotate the parse tree - we will find this (and 
			// name it) looking at process subtrees in Pass07.
			PatternDeclContext pdc = (PatternDeclContext) t;
			NamePatternContext np = pdc.patternLHS().namePattern();
			if (np == null || !(np instanceof NpNameContext)) {
				logger.trace("Not a simple declaration, skipping: "
						+ np.getText());
				continue;
			}
			if (pdc.letClause() != null) {
				//logger.trace("Skipping process definition: "
				//		+ np.getText());
				continue;
			} 
			if (t instanceof LitCharContext) {
				registerConstCharDecl(pdc, null, 
						(LitCharContext)t, false);
				continue;
			} else if (t instanceof LitStringContext) {
				registerConstStringDecl(pdc, null, 
						(LitStringContext)t, false);
				continue;
			}
		}		
		// Let...Within version of simple math declarations
		for (ParseTree t : XPath.findAll((ParseTree) tree, 
					letPath+patternPath, parser)) {
			this.walkedSimpleMathDecl = true;
			if (!(t instanceof PatternDeclContext)) {
				logger.error("Wrong subtree in process().");
				continue;
			}
			PatternDeclContext pdc = (PatternDeclContext) t;
			NamePatternContext np = pdc.patternLHS().namePattern();

			if (np == null || !(np instanceof NpNameContext)) {
				logger.trace("Not a simple decl, skipping."
						+ t.getText());
				continue;
			}
			if (pdc.letClause() != null) {
				//logger.trace("Skipping process definition: "
				//		+ np.getText());
				continue;
			} 
			// walk pdc.expression();
			ParseTree e = (ParseTree) pdc.expression();
			walker.walk(this, e);
			if (walkedSimpleMathDecl) {
				//logger.trace(np.getText());
				registerConstIntDecl(pdc, np.getText(), 
						pdc.expression().getText(), false);
			} // else not a simple math decl
			this.walkedSimpleMathDecl = true;
		}


	}

	private void registerConstIntDecl(PatternDeclContext pdc, String name, 
			String value, boolean topLevelDecl) {
		ConstIntDef ciDef = new ConstIntDef(pdc, value, name);
		for (ExtBaseDef ebd : walkedExtRefs.values()) {
			ciDef.addExtRef(ebd);
		}
		ciDef.setTranState(tState);
		setDef(pdc, ciDef);
		ciDef.markComplete();
		logger.trace("registered: " + ciDef.name());
		if (topLevelDecl) {
			ciDef.setGlobal();
			tState.symTable.currentScope().define(ciDef, name);
		}
	}

	private void registerConstCharDecl(PatternDeclContext pdc, String name,
			LitCharContext lic, boolean topLevelDecl) {
		String value = lic.Char().getText();
		
		ConstCharDef ccDef = new ConstCharDef(pdc, value, name);
		ccDef.setTranState(tState);
		setDef(pdc, ccDef);
		ccDef.markComplete();
		if (topLevelDecl) {
			ccDef.setGlobal();
			tState.symTable.currentScope().define(ccDef, name);
		}
	}

	private void registerConstStringDecl(PatternDeclContext pdc, String name, 
			LitStringContext lic, boolean topLevelDecl) {
		String value = lic.String().getText();
		
		ConstStringDef csDef = new ConstStringDef(pdc, value, name);
		csDef.setTranState(tState);
		setDef(pdc, csDef);
		csDef.markComplete();
		if (topLevelDecl) {
			csDef.setGlobal();
			tState.symTable.currentScope().define(csDef, name);
		}
	}

	@Override
	public void enterExprConcat(ExprConcatContext ctx) {
		this.walkedSimpleMathDecl = false;	
	}

	@Override
	public void enterExprLength(ExprLengthContext ctx) {
		this.walkedSimpleMathDecl = false;	
	}

	@Override
	public void enterExprMul(ExprMulContext ctx) {
	}

	@Override
	public void enterExprDiv(ExprDivContext ctx) {
	}

	@Override
	public void enterExprMod(ExprModContext ctx) {
	}

	@Override
	public void enterExprNeg(ExprNegContext ctx) {
	}

	@Override
	public void enterExprAdd(ExprAddContext ctx) {
	}

	@Override
	public void enterExprSub(ExprSubContext ctx) {
	}

	@Override
	public void enterExprEq(ExprEqContext ctx) {
		this.walkedSimpleMathDecl = false;	
	}

	@Override
	public void enterExprNEq(ExprNEqContext ctx) {
		this.walkedSimpleMathDecl = false;	
	}

	@Override
	public void enterExprLT(ExprLTContext ctx) {
		this.walkedSimpleMathDecl = false;	
	}

	@Override
	public void enterExprLTE(ExprLTEContext ctx) {
		this.walkedSimpleMathDecl = false;	
	}

	@Override
	public void enterExprGT(ExprGTContext ctx) {
		this.walkedSimpleMathDecl = false;	
	}

	@Override
	public void enterExprGTE(ExprGTEContext ctx) {
		this.walkedSimpleMathDecl = false;	
	}

	@Override
	public void enterExprNot(ExprNotContext ctx) {
		this.walkedSimpleMathDecl = false;	
	}

	@Override
	public void enterExprAnd(ExprAndContext ctx) {
		this.walkedSimpleMathDecl = false;	
	}

	@Override
	public void enterExprOr(ExprOrContext ctx) {
		this.walkedSimpleMathDecl = false;	
	}

	@Override
	public void enterExprType(ExprTypeContext ctx) {
		this.walkedSimpleMathDecl = false;	
	}

	@Override
	public void enterExprDotted(ExprDottedContext ctx) {
		this.walkedSimpleMathDecl = false;	
	}

	@Override
	public void enterExprInput(ExprInputContext ctx) {
		this.walkedSimpleMathDecl = false;	
	}

	@Override
	public void enterExprOutput(ExprOutputContext ctx) {
		this.walkedSimpleMathDecl = false;	
	}

	@Override
	public void enterExprPrefix(ExprPrefixContext ctx) {
		this.walkedSimpleMathDecl = false;	
	}

	@Override
	public void enterExprGuarded(ExprGuardedContext ctx) {
		this.walkedSimpleMathDecl = false;	
	}

	@Override
	public void enterExprSeqComp(ExprSeqCompContext ctx) {
		this.walkedSimpleMathDecl = false;	
	}

	@Override
	public void enterExprTimeout(ExprTimeoutContext ctx) {
		this.walkedSimpleMathDecl = false;	
	}

	@Override
	public void enterExprInterrupt(ExprInterruptContext ctx) {
		this.walkedSimpleMathDecl = false;	
	}

	@Override
	public void enterExprExtCh(ExprExtChContext ctx) {
		this.walkedSimpleMathDecl = false;	
	}

	@Override
	public void enterExprIntCh(ExprIntChContext ctx) {
		this.walkedSimpleMathDecl = false;	
	}

	@Override
	public void enterExprException(ExprExceptionContext ctx) {
		this.walkedSimpleMathDecl = false;	
	}

	@Override
	public void enterExprInterfaceParallel(ExprInterfaceParallelContext ctx) {
		this.walkedSimpleMathDecl = false;	
	}

	@Override
	public void enterExprAlphaParallel(ExprAlphaParallelContext ctx) {
		this.walkedSimpleMathDecl = false;	
	}

	@Override
	public void enterExprLinkedParallel(ExprLinkedParallelContext ctx) {
		this.walkedSimpleMathDecl = false;	
	}

	@Override
	public void enterExprInterleave(ExprInterleaveContext ctx) {
		this.walkedSimpleMathDecl = false;	
	}

	@Override
	public void enterExprHide(ExprHideContext ctx) {
		this.walkedSimpleMathDecl = false;	
	}

	@Override
	public void enterExprReplAlphaParallel(ExprReplAlphaParallelContext ctx) {
		this.walkedSimpleMathDecl = false;	
	}

	@Override
	public void enterExprReplExtCh(ExprReplExtChContext ctx) {
		this.walkedSimpleMathDecl = false;	
	}

	@Override
	public void enterExprReplInterfaceParallel(ExprReplInterfaceParallelContext ctx) {
		this.walkedSimpleMathDecl = false;	
	}

	@Override
	public void enterExprReplInterleave(ExprReplInterleaveContext ctx) {
		this.walkedSimpleMathDecl = false;	
	}

	@Override
	public void enterExprReplIntCh(ExprReplIntChContext ctx) {
		this.walkedSimpleMathDecl = false;	
	}

	@Override
	public void enterExprIf(ExprIfContext ctx) {
		this.walkedSimpleMathDecl = false;	
	}
	
	@Override
	public void enterLetClause(LetClauseContext ctx) {
		this.walkedSimpleMathDecl = false;	
	}

	@Override
	public void enterPExprSetExpr(PExprSetExprContext ctx) {
		this.walkedSimpleMathDecl = false;
	}

	@Override
	public void enterPExprSeqExpr(PExprSeqExprContext ctx) {
		this.walkedSimpleMathDecl = false;
	}

	@Override
	public void enterCspChaosProcess(CspChaosProcessContext ctx) {
		this.walkedSimpleMathDecl = false;	
	}

	@Override
	public void enterSetSeqOverSetInf(SetSeqOverSetInfContext ctx) {
		this.walkedSimpleMathDecl = false;	
	}

	@Override
	public void enterLitChar(LitCharContext ctx) {
		this.walkedSimpleMathDecl = false;
	}

	@Override
	public void enterLitString(LitStringContext ctx) {
		this.walkedSimpleMathDecl = false;
	}

	@Override
	public void exitPExprName(PExprNameContext ctx) {
		String name = ctx.getText();
		Symbol sym = tState.symTable.currentScope().resolve(name);

		if (sym != null && sym.def() != null
				&& sym.def() instanceof ExtBaseDef) {
			walkedExtRefs.put(name, (ExtBaseDef) sym.def());
		}
	}
}
