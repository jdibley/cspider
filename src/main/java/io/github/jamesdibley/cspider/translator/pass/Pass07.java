/**
 * Pass07
 */
package io.github.jamesdibley.cspider.translator.pass;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
import org.antlr.v4.runtime.tree.xpath.XPath;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import io.github.jamesdibley.cspider.parser.gen.*;
import io.github.jamesdibley.cspider.parser.gen.CSPMParser.*;
import io.github.jamesdibley.cspider.translator.BaseDef;
import io.github.jamesdibley.cspider.translator.BasePass;
import io.github.jamesdibley.cspider.translator.TranState;
import io.github.jamesdibley.cspider.translator.ValueType;
import io.github.jamesdibley.cspider.translator.symbol.*;
import io.github.jamesdibley.cspider.translator.def.*;
import io.github.jamesdibley.cspider.translator.def.var.*;

public class Pass07 extends ProcFuncCommon {
	private static final Logger logger = 
		LogManager.getLogger(Pass07.class);
	private String rootXPath = "/sourcefile/declaration";
	private String patternXPath = "/patternDecl";
	private String expressionXPath = "//expression";
	
	public Pass07(TranState tState, ParseTreeWalker walker, 
			CSPMParser parser) {
		super(tState, walker, parser);
		logger.trace("Gather global non-parameterised processes.");
	}

	public void process(SourcefileContext tree) {
		process((ParseTree) tree);
	}

	private void process(ParseTree tree) {
		processGlobalProcesses(tree, rootXPath+patternXPath);
	}

	private void processGlobalProcesses(ParseTree tree, String path) {
		for (ParseTree t : XPath.findAll(tree, path, parser)) {
			resetWP();
			String tmpName = null;
			
			PatternDeclContext pdc = (PatternDeclContext) t;
			NamePatternContext npc = pdc.patternLHS().namePattern();
			
			if (tState.isInMap(t)) { continue; }
			if (!isRegularPattern(npc)) {
				logger.error("Unsupported decl found. Check "
						+ "input for type annotations. "
						+ npc.getText());
				tState.abortTranslation();
				return;
			}

			tmpName = getTmpName(npc);
		// NB: We push a new scope here to hold any 
		// identifiers we encounter in the process expression
		// (as well as an LDE, if it exists, although we don't
		// gather these until a subsequent pass.)
			tState.symTable.pushScope();
			ExpressionContext e = pdc.expression();
			Map<String, String> tmpRenameMap = 
				processRenamingBlock(pdc);

			//logger.trace(tmpName + " " + tmpRenameMap);

			classifying = true;
			walker.walk(this, e);
			classifying = false;
		// We define the process within its parent scope, not 
		// the one we just pushed.
			Scope parentScope = 
				tState.symTable.currentScope().parentScope();
			ProcessDef pd = 
				new ProcessDef(pdc, null, parentScope, tmpName);
		// We also stow the scope for the -contents- of this process and
		// its expression:
			pd.setContainedScope(tState.symTable.currentScope());

			scanIOOperations(e, pd);

			if (walkedCompType != CompType.UNKNOWN) {
				pd.setCompType(walkedCompType);
				resetWalkedCompType();
			}

			for (String s : tmpRenameMap.keySet()) {
				pd.addChanRenaming(s, tmpRenameMap.get(s));
			}

			for (SetDef sd : walkedSetRefs.values()) {
				sd.addReferencingProc(pd);
				pd.addSetRef(sd);
			}

			for (ListDef ld : walkedListRefs.values()) {
				ld.addReferencingProc(pd);
				pd.addListRef(ld);
			}

			for (BaseDef bd : walkedConstRefs.values()) {
				pd.addConstRef(bd);
				if (bd instanceof ConstIntDef) {
					ConstIntDef cid = (ConstIntDef) bd;
					for (ExtBaseDef ebd : cid.extRefs()) {
						ebd.addReferencingProc(pd);
						pd.addExtRef(ebd);
					}
				}
			}

			for (ExtBaseDef ebd : tmpExtRefs.values()) {
				ebd.addReferencingProc(pd);
				pd.addExtRef(ebd);
			}
			tmpExtRefs.clear();

		// Set up a LetClauseDef for later processing.
			if (pdc.letClause() != null) { 
				LetClauseContext lcc = pdc.letClause();
				LetClauseDef lcd = new LetClauseDef(lcc, 
						tState.symTable.currentScope(), pd);
				lcd.setTranState(tState);				
				setDef(lcc, lcd);
				//logger.trace("set LDE on process: " + tmpName);
			}

			//logger.trace(pd.name());
			//logger.trace("params: ");
			//for (ParamDef prd : pd.params()) {
			//	logger.trace(prd.name());
			//}
			//logger.trace("extrefs: " + pd.extRefKeys());

			pd.setTranState(tState);
			setDef(pdc, pd);
			tState.symTable.currentScope().parentScope().define(pd, tmpName);

			//walkedChanRefs.clear();
		// We pop the scope we have been working on (and 
		// return to the global scope.)
			tState.symTable.popScope();
		}
	}


}
