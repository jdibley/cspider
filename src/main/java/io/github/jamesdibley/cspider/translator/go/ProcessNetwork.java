/**
 * ProcessNetwork
 */
package io.github.jamesdibley.cspider.translator.go;

import java.util.LinkedHashMap;
import java.util.Map;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.stringtemplate.v4.*;
import io.github.jamesdibley.cspider.parser.gen.*;
import io.github.jamesdibley.cspider.parser.gen.CSPMParser.*;
import io.github.jamesdibley.cspider.translator.ValueType;

public class ProcessNetwork {
	private static final Logger logger = 
		LogManager.getLogger(ProcessNetwork.class);
	public OutputModel om;
	public ST pn;
	public String n;
	public String pkn;
	public Map<String, ST> guardedChanFuncs;	
	public Map<String, String> clientChanIDs;

	public ProcessNetwork(ST pn, OutputModel om) {
		this.pn = pn;
		this.om = om;
		guardedChanFuncs = new LinkedHashMap<String, ST>();
		clientChanIDs = new LinkedHashMap<String, String>();
	}

	public void registerInputName(String iName) {
		pkn = iName;
		n = iName.substring(0,1).toUpperCase() 
			+ iName.substring(1);
		pn.add("pkn", pkn);
		pn.add("n", n);
	}

	public void addParam(String s, ST t) {
		pn.add("pnParamName", s);
		pn.add("pnParamType", t);
	}

	public void addStVar(String s, ST t, String v) {
		pn.add("pnStateVarName", s);
		pn.add("pnStateVarType", t);
		pn.add("pnStateVarInit", v);
	}

	public void addClientChannel(String s, String t) {
		pn.add("pnClientChanName", s);
		pn.add("pnClientChanType", 
			om.getTemplate("chan").add("t", t));
		clientChanIDs.put(s.substring(0,1).toLowerCase() + s.substring(1), s);
	}

	public void addRenamedClientChannel(String s, String t, String u) {
		pn.add("pnRenamedChanClientName", s);
		pn.add("pnRenamedChanType",
				om.getTemplate("chan").add("t", t));
		pn.add("pnRenamedChanNetworkName", u);
		clientChanIDs.put(s.substring(0,1).toLowerCase() + s.substring(1), s);
	}

	public void addClientChannelArray(String s, String t) {
		pn.add("pnClientChanArrayName", s);
		pn.add("pnClientChanArrayType", 
			om.getTemplate("slice").add("t", 
				om.getTemplate("chan").add("t", t)));
		clientChanIDs.put(s.substring(0,1).toLowerCase() + s.substring(1), s);
	}

	public void addNetworkChannel(String s, String t) {
		pn.add("nwChanName", s);
		pn.add("nwChanType", t);
	}

	public void addNetworkChannelArray(String s, String t, String size) {
		pn.add("nwChanArrayName", s);
		pn.add("nwChanArrayType", t);
		pn.add("nwChanArraySize", size);
	}

	public void addFunction(ST st) {
		pn.add("pnFuncs", st);
	}

	public void addProcessObject(ProcessObject po) {
		pn.add("procName", po.name);
		pn.add("procLiteral", po.literal);
	}

	public void addReplProcessObject(ProcessObject po, String size, 
			ExpressionContext ctr) {
		pn.add("procArrayName", po.name);
		pn.add("procArrayCtr", ctr.getText());
		pn.add("procArraySize", size);
		pn.add("procArrayLiteral", po.literal);
	}

	public void addCommStruct(ST struct) {
		pn.add("pnCommStruct", struct);
	}

	public void addGuardedChanFunc(String type, String name) {
		ST gcf = om.getTemplate("gcf");
		gcf.add("t", type);
		gcf.add("n", name);
		guardedChanFuncs.put(type, gcf);
	}

	public void genGuardedChanFuncs() {
		for (ST gcf : guardedChanFuncs.values()) {
			pn.add("pnGuardedChanFuncs", gcf);
		}
	}

	public String render() {
		return pn.render();
	}

	public String toString() {
		return "ProcessNetwork. package " + pkn + " and name " + n;
	}
}
