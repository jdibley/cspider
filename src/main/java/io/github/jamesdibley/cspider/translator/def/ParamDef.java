/**
 * ParamDef
 */
package io.github.jamesdibley.cspider.translator.def;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
import io.github.jamesdibley.cspider.translator.BaseDef;
import io.github.jamesdibley.cspider.translator.ValueType;
import io.github.jamesdibley.cspider.translator.def.VarBaseDef;
import io.github.jamesdibley.cspider.translator.def.var.*;

public class ParamDef extends BaseDef {
	private VarBaseDef vbd;

	public ParamDef(ParserRuleContext ctx, String name) {
		super(ctx, name);
		setType(ValueType.UNKNOWN);
		vbd = null;
	}

	public void setVar(VarBaseDef vbd) {
		this.vbd = vbd;
		setType(vbd.type());
		markComplete();
	}

	public String toString() {
		switch (type()) {
		case UNKNOWN:
			return "u";
		case INT:
			return ((VarIntDef)vbd).toString();
		case BOOL:
			return ((VarBoolDef)vbd).toString();
		case CHAR:
			return ((VarCharDef)vbd).toString();
		case STRING:
			return ((VarListDef)vbd).toString();
		case LIST:
			return ((VarListDef)vbd).toString();
		case TUPLE:
			return "t";
		case SET:
			return ((VarSetDef)vbd).toString(); 
		}
		return "";
	}
}
