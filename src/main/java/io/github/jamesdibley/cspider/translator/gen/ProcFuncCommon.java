/**
 * ProcFuncCommon
 */
package io.github.jamesdibley.cspider.translator.gen;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
import org.antlr.v4.runtime.tree.xpath.XPath;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.stringtemplate.v4.*;
import io.github.jamesdibley.cspider.parser.gen.*;
import io.github.jamesdibley.cspider.parser.gen.CSPMParser.*;
import io.github.jamesdibley.cspider.translator.BaseDef;
import io.github.jamesdibley.cspider.translator.BaseGen;
import io.github.jamesdibley.cspider.translator.TranState;
import io.github.jamesdibley.cspider.translator.ValueType;
import io.github.jamesdibley.cspider.translator.def.*;
import io.github.jamesdibley.cspider.translator.go.*;
import io.github.jamesdibley.cspider.translator.symbol.*;


public class ProcFuncCommon extends BaseGen {
	private static final Logger logger =
		LogManager.getLogger(ProcFuncCommon.class);

	protected String patternXPath =
		"/sourcefile/declaration/patternDecl";
	protected String paraPatterXPath = 
		"/sourcefile/declaration/parametricPatternDecl";

	private boolean debug = false;
	protected Scope tmpScope = null;
	protected ProcessDef currentSubstate = null;
	protected OutputModel out;
	protected boolean parensProtect = false;
	protected boolean scanningFunction = false;
	protected boolean scanForProcNet = false;
	protected String procPfx = null;
	protected String rxAbbr = null;
	protected boolean proxyChanRequired = false;
	// has implications for how we deal with guarded expressions, possibly
	protected boolean inExtChoice = false; 

	public ProcFuncCommon(TranState tState, ParseTreeWalker walker,
			CSPMParser parser) {
		super(tState, walker, parser);
		out = tState.output;
	}	

	protected void setProcPfx(String procPfx) {
		this.procPfx = procPfx;
		rxAbbr = procPfx.substring(0,1).toLowerCase();
		if (debug) {
			logger.trace("procPfx set to: " + procPfx + "and rxAbbr is: " + rxAbbr);
		}
	}

	protected void setCurrentSubstate(ProcessDef substateProc) {
		currentSubstate = substateProc;
	}

	protected void resetCurrentSubstate() {
		currentSubstate = null;
	}

	protected boolean proxyChanRequired() {
		return proxyChanRequired;
	}

	protected void scanningFunction() {
		scanningFunction = true;
	}

	protected void resetScanningFunction() {
		scanningFunction = false;
	}


	protected void setupFunction(FunctionDef fd, ProcessObject po) {
		ParametricPatternDeclContext ppdc =
			(ParametricPatternDeclContext) fd.ctx();

		ST funcST = out.getTemplate("func");
		if (po != null) {
			funcST.add("rxName", po.name);
			funcST.add("rxAbbr", po.getPOInitial()); 
		} 	
		funcST.add("n", fd.name());
		funcST.add("t", out.getGoType(fd.returnType().type()));

		setFuncParams(fd, funcST);

		tmpScope = fd.containedScope();
		if (po == null) { scanForProcNet = true; }
		walker.walk(this, ppdc.expression(1));
		if (scanForProcNet) { scanForProcNet = false; }
		if (!(ppdc.expression(1) instanceof ExprIfContext)) {
			ST stReturn = out.getTemplate("return");
			stReturn.add("e", getST(ppdc.expression(1)));
			funcST.add("line", stReturn);
		} else {
			funcST.add("line", getST(ppdc.expression(1)));
		}
		if (po != null) {
			po.st.add("userDefMethods", funcST);
		} else {
			out.procNet.pn.add("pnFuncs", funcST);
		}
		tmpScope = null;
	}


	private void setFuncParams(FunctionDef fd, ST funcST) {
		for (ParamDef prd : fd.params()) {
			ST arg = out.getTemplate("arg");
			arg.add("n", prd.name());
			arg.add("t", out.getGoType(prd.type()));
			funcST.add("args", arg);
		}
	}


	// expression alternatives -- BEGIN 
	
	// We have to deal with the fact that sometimes parens are used in CSP
	// scripts in ways that the Go compiler will spit and hiss at. Our 
	// solution is to make parens disappear unless we encounter them in 
	// the context of an if-then-else or while scanning a function.
	@Override
	public void exitExprParens(ExprParensContext ctx) {
		if (parensProtect || scanningFunction) {
			ST st = getTemplate("parens");
			st.add("e", getST(ctx.expression()));
			setST(ctx, st);
		} else {
			// disappearing
			setST(ctx, getST(ctx.expression()));
		}
		if (getST(ctx) != null && debug) {
			logger.trace("exitExprParens: " + getST(ctx).render());
		}
	}

	@Override
	public void exitExprConcat(ExprConcatContext ctx) {
		ST st = getTemplate("cspISQAddBack").add("o", getST(ctx.expression(0)));
		st.add("e", getST(ctx.expression(1)));
		setST(ctx, st);
		if (getST(ctx) != null && debug) {
			logger.trace("exitExprConcat: " + getST(ctx).render());
		}
	}

	@Override
	public void exitExprLength(ExprLengthContext ctx) {
		ST st = getTemplate("cspISQLength").add("o", ctx.expression());
		setST(ctx, st);
		if (getST(ctx) != null && debug) {
			logger.trace("exitExprLength: " + getST(ctx).render());
		}
	}
		
	@Override
	public void exitExprNeg(ExprNegContext ctx) {
		ST st = getTemplate("neg").add("e", ctx.expression());
		setST(ctx, st);
		if (getST(ctx) != null && debug) {
			logger.trace("exitExprNeg: " + getST(ctx).render());
		}
	}

	@Override
	public void exitExprMod(ExprModContext ctx) {
		ST st = getTemplate("mod");
		st.add("e0", getST(ctx.expression(0)));
		st.add("e1", getST(ctx.expression(1)));
		setST(ctx, st);
		if (getST(ctx) != null && debug) {
			logger.trace("exitExprMod: " + getST(ctx).render());
		}
	}

	@Override
	public void exitExprDiv(ExprDivContext ctx) {
		ST st = getTemplate("div");
		st.add("e0", getST(ctx.expression(0)));
		st.add("e1", getST(ctx.expression(1)));
		setST(ctx, st);
		if (getST(ctx) != null && debug) {
			logger.trace("exitExprDiv: " + getST(ctx).render());
		}
	}
	
	@Override
	public void exitExprMul(ExprMulContext ctx) {
		ST st = getTemplate("mul");
		st.add("e0", getST(ctx.expression(0)));
		st.add("e1", getST(ctx.expression(1)));
		setST(ctx, st);
		if (getST(ctx) != null && debug) {
			logger.trace("exitExprMul: " + getST(ctx).render());
		}
	}
	
	@Override
	public void exitExprAdd(ExprAddContext ctx) {
		ST st = getTemplate("add");
		st.add("e0", getST(ctx.expression(0)));
		st.add("e1", getST(ctx.expression(1)));
		setST(ctx, st);
		if (getST(ctx) != null && debug) {
			logger.trace("exitExprAdd: " + getST(ctx).render());
		}
	}

	@Override
	public void exitExprSub(ExprSubContext ctx) {
		ST st = getTemplate("sub");
		st.add("e0", getST(ctx.expression(0)));
		st.add("e1", getST(ctx.expression(1)));
		setST(ctx, st);
		if (getST(ctx) != null && debug) {
			logger.trace("exitExprSub: " + getST(ctx).render());
		}
	}

	@Override
	public void exitExprEq(ExprEqContext ctx) {
		ST st = getTemplate("eq");
		st.add("e0", getST(ctx.expression(0)));
		st.add("e1", getST(ctx.expression(1)));
		setST(ctx, st);
		if (getST(ctx) != null && debug) {
			logger.trace("exitExprEq: " + getST(ctx).render());
		}
	}

	@Override
	public void exitExprNEq(ExprNEqContext ctx) {
		ST st = getTemplate("neq");
		st.add("e0", getST(ctx.expression(0)));
		st.add("e1", getST(ctx.expression(1)));
		setST(ctx, st);
		if (getST(ctx) != null && debug) {
			logger.trace("exitExprNEq: " + getST(ctx).render());
		}
	}
	
	@Override
	public void exitExprLT(ExprLTContext ctx) {
		ST st = getTemplate("lt");
		st.add("e0", getST(ctx.expression(0)));
		st.add("e1", getST(ctx.expression(1)));
		setST(ctx, st);
		if (getST(ctx) != null && debug) {
			logger.trace("exitExprLT: " + getST(ctx).render());
		}
	}

	@Override
	public void exitExprLTE(ExprLTEContext ctx) {
		ST st = getTemplate("lte");
		st.add("e0", getST(ctx.expression(0)));
		st.add("e1", getST(ctx.expression(1)));
		setST(ctx, st);
		if (getST(ctx) != null && debug) {
			logger.trace("exitExprLTE: " + getST(ctx).render());
		}
	}
	
	@Override
	public void exitExprGT(ExprGTContext ctx) {
		ST st = getTemplate("gt");
		st.add("e0", getST(ctx.expression(0)));
		st.add("e1", getST(ctx.expression(1)));
		setST(ctx, st);
		if (getST(ctx) != null && debug) {
			logger.trace("exitExprGT: " + getST(ctx).render());
		}
	}

	@Override
	public void exitExprGTE(ExprGTEContext ctx) {
		ST st = getTemplate("gte");
		st.add("e0", getST(ctx.expression(0)));
		st.add("e1", getST(ctx.expression(1)));
		setST(ctx, st);
		if (getST(ctx) != null && debug) {
			logger.trace("exitExprGTE: " + getST(ctx).render());
		}
	}

	@Override
	public void exitExprNot(ExprNotContext ctx) {
		setST(ctx, getTemplate("not").add("e", getST(ctx.expression())));
		if (getST(ctx) != null && debug) {
			logger.trace("exitExprNot: " + getST(ctx).render());
		}
	}

	@Override
	public void exitExprAnd(ExprAndContext ctx) {
		ST st = getTemplate("and");
		st.add("e0", getST(ctx.expression(0)));
		st.add("e1", getST(ctx.expression(1)));
		setST(ctx, st);
		if (getST(ctx) != null && debug) {
			logger.trace("exitExprAnd: " + getST(ctx).render());
		}
	}

	@Override
	public void exitExprOr(ExprOrContext ctx) {
		ST st = getTemplate("or");
		st.add("e0", getST(ctx.expression(0)));
		st.add("e1", getST(ctx.expression(1)));
		setST(ctx, st);
		if (getST(ctx) != null && debug) {
			logger.trace("exitExprOr: " + getST(ctx).render());
		}
	}

	private ST genChanInput(ChanInputDef cid, boolean guarded) {
		ST st = null;
		if (cid.requiresMsgStruct()) {
			st = getTemplate("block");
			ST chIn = getTemplate("chIn");
			if (!(guarded)) {
				chIn.add("ch", rxAbbr + "." + cid.src().name());
				for (ExpressionContext ec : cid.indexExprs()) {
					chIn.add("i", getST(ec));
				}	
				chIn.add("v", cid.name());
			} 
			//chIn.add("t", cid.valueType());
			st.add("line", chIn);
			for (String s : cid.identifiers()) {
				ST stAssign = getTemplate("sVarDecl");
				stAssign.add("v", s);
				stAssign.add("e", cid.name() + ".f0" + cid.identifiers().indexOf(s));
				st.add("line", stAssign);
			}
			return st;
		} 
		// simple ones here
		st = getTemplate("chIn");
		if (!(guarded)) {
			st.add("ch", rxAbbr + "." + cid.src().name());
			for (ExpressionContext ec : cid.indexExprs()) {
				st.add("i", getST(ec));
			}
		} else {
			st.add("nop", "nop");
		}
		for (String s : cid.identifiers()) {
			st.add("v", s);
		}
		return st;
	}

	private ST genChanOutput(ChanOutputDef cod, boolean guarded) {
		ST st = null;
		if (cod.requiresMsgStruct()) {
			st = getTemplate("chOut");
			if (!(guarded)) {
				st.add("ch", rxAbbr + "." + cod.dst().name());
				for (ExpressionContext ec : cod.indexExprs()) {
					st.add("i", getST(ec));
				}	
			} else {
				st.add("nop", "nop");
			}
			
			ST strLit = getTemplate("strLit");
			strLit.add("n", cod.dst().msgStructName());
			for (ExpressionContext ec : cod.expressionExprs()) {
				strLit.add("k", "f0" + cod.expressionExprs().indexOf(ec));
				strLit.add("v", getST(ec));
			}
			st.add("e", strLit);
			return st;
		}
		// simple ones here
		st = getTemplate("chOut");
		if (!(guarded)) {
			st.add("ch", rxAbbr + "." + cod.dst().name());
			for (ExpressionContext ec : cod.indexExprs()) {
				st.add("i", getST(ec));
			}
		} else {
			st.add("nop", "nop");
		}
		for (ExpressionContext ec : cod.expressionExprs()) {
			st.add("e", getST(ec));
		}
		return st;
	}

	@Override
	public void exitExprInput(ExprInputContext ctx) {
		BaseDef bd = def(ctx);
		if (bd == null || !(bd instanceof ChanInputDef)) {
			logger.error("Unexpected or missing chanInput def in: " + ctx.getText());
		}
		ChanInputDef cid = (ChanInputDef) bd;
		setST(ctx, genChanInput(cid, false));
		if (getST(ctx) != null && debug) {
			logger.trace("exitExprInput: " + getST(ctx).render());
		}
	}
	
	@Override
	public void exitExprOutput(ExprOutputContext ctx) {
		BaseDef bd = def(ctx);
		if (bd == null || !(bd instanceof ChanOutputDef)) {
			logger.error("Unexpected or missing chanOutput def in: " + ctx.getText());
		}
		ChanOutputDef cod = (ChanOutputDef) bd;
		setST(ctx, genChanOutput(cod, false));
		if (getST(ctx) != null && debug) {
			logger.trace("exitExprOutput: " + getST(ctx).render());
		}
	}

	@Override
	public void exitExprPrefix(ExprPrefixContext ctx) {
		ST st = getTemplate("block");
		st.add("line", getST(ctx.expression(0)));
		st.add("line", getST(ctx.expression(1)));
		setST(ctx, st);
		if (getST(ctx) != null && debug) {
			logger.trace("exitExprPrefix: " + getST(ctx).render());
		}
	}

	@Override
	public void exitExprGuarded(ExprGuardedContext ctx) {
		// TODO: maybe support this outside of extchoice someday
		;
	}

	@Override 
	public void enterExprExtCh(CSPMParser.ExprExtChContext ctx) {
		inExtChoice = true;
	}

	private void genGCF(ChanInputDef cid) {
		ST st = getTemplate("gcf");
		if (cid.requiresMsgStruct()) {
			String type = cid.src().msgStructName();
			out.procNet.addGuardedChanFunc(type,
					type.substring(0,1).toUpperCase() 
					+ type.substring(1));
		} else {
			String type = out.getGoType(cid.valueType()).render();
			out.procNet.addGuardedChanFunc(type,
					type.substring(0,1).toUpperCase() 
					+ type.substring(1));
		}
		if (debug) { logger.trace("genGCF from CID: " + st.render()); }
	}

	private void genGCF(ChanOutputDef cod) {
		ST st = getTemplate("gcf");
		if (cod.requiresMsgStruct()) {
			String type = cod.dst().msgStructName();
			out.procNet.addGuardedChanFunc(type, 
					type.substring(0,1).toUpperCase() 
					+ type.substring(1));
		} else {
			String type = out.getGoType(cod.valueType()).render();
			out.procNet.addGuardedChanFunc(type,
					type.substring(0,1).toUpperCase() + type.substring(1));
		}
		if (debug) { logger.trace("genGCF from COD: " + st.render()); }
	}

	private void genGCF() {
		ST st = getTemplate("gcf");
		out.procNet.addGuardedChanFunc(getTemplate("emptyStruct").render(),
				"Signal");
		if (debug) { logger.trace("genGCF for proxy channel: " + st.render()); }
	}

	private void genGuardedInput(ST altCase, AlternativeDef ad, ChanInputDef cid) {
		if (debug) {
			logger.trace(">> into genGuardedInput on channel: " + cid.src().name());
			logger.trace("requiresMsgStruct: " + cid.requiresMsgStruct());
		}
		ST trigger = getTemplate("guardInputTrig");
		if (cid.requiresMsgStruct()) {
			trigger.add("t", cid.src().msgStructName().substring(0,1).toUpperCase() 
					+ cid.src().msgStructName().substring(1));
		} else {
			trigger.add("t", cid.valueType());
		}	
		trigger.add("cond", getST(ad.guardCtx));
		ST chanST = getTemplate("subscript").add("v", rxAbbr + "." + cid.src().name());
		for (ExpressionContext ec : cid.indexExprs()) {
			chanST.add("i", getST(ec));
		}
		trigger.add("ch", chanST);
		if (debug) { 
			logger.trace(">>> trigger debug: " + trigger.render()); 
		}
		//if (cid.requiresMsgStruct()) {
		//	altCase.add("line"
		//}
		//trigger.add("v", genChanInput(cid, true));
		altCase.add("trigger", trigger);
		if (cid.requiresMsgStruct()) {
			trigger.add("v", cid.name());
			for (String s : cid.identifiers()) {
				ST stAssign = getTemplate("sVarDecl");
				stAssign.add("v", s);
				stAssign.add("e", cid.name() + ".f0" + cid.identifiers().indexOf(s));
				altCase.add("line", stAssign);
			}
		} else {
			trigger.add("v", cid.identifiers().get(0));
		}
		altCase.add("line", getST(ad.rest));
		// create corresponding GCF
		genGCF(cid);
	}

	private void genGuardedOutput(ST altCase, AlternativeDef ad, ChanOutputDef cod) {
		if (debug) {
			logger.trace(">> into genGuardedOutput");
			logger.trace("requiresMsgStruct: " + cod.requiresMsgStruct());
		}
		ST trigger = getTemplate("guardOutputTrig");
		if (cod.requiresMsgStruct()) {
			trigger.add("t", cod.dst().msgStructName());
		} else {
			trigger.add("t", cod.valueType());
		}
		trigger.add("e", genChanOutput(cod, true));
		trigger.add("cond", getST(ad.guardCtx));
		ST chanST = getTemplate("subscript").add("v", rxAbbr + "." + cod.dst().name());
		for (ExpressionContext ec : cod.indexExprs()) {
			chanST.add("i", getST(ec));
		}
		trigger.add("ch", chanST);
		if (debug) {
			logger.trace(">>> trigger debug: " + trigger.render());
		}
		altCase.add("trigger", trigger);
		altCase.add("line", getST(ad.rest));
		// create corresponding GCF
		genGCF(cod);

	}

	private ST genProxyInput(ST altCase, AlternativeDef ad) {
		ST chIn = getTemplate("chIn");
		chIn.add("ch", rxAbbr + "." + getTemplate("proxy").render());
		altCase.add("trigger", chIn);
		altCase.add("line", getTemplate("chOut").add("ch", rxAbbr + "." + getTemplate("proxy").render()));
		altCase.add("line", getST(ad.rest));
		return chIn;
	}

	private void genGuardedProxyInput(ST altCase, AlternativeDef ad) {
		//logger.trace(">> into genGuardedProxyInput");
		ST trigger = getTemplate("guardInputTrig");
		trigger.add("cond", getST(ad.guardCtx));
		trigger.add("ch", rxAbbr + "." + getTemplate("proxy").render());
		altCase.add("trigger", trigger);
		altCase.add("line", getTemplate("chOut").add("ch", 
					rxAbbr + "." + getTemplate("proxy").render()));
		altCase.add("line", getST(ad.rest));
		if (debug) { 
			logger.trace("genGuardedProxyInput debug: \n" 
					+  altCase.render()); 
		}
		genGCF();
	}

	private void genGuardedCase(ST altCase, AlternativeDef ad) {
		//logger.trace(">> into genGuardedCase");
		if (!(ad.requiresProxyChannel)) {
			ST gcfCall = null;
			BaseDef bd = def(ad.first);
			if (bd instanceof ChanInputDef) {
				ChanInputDef cid = (ChanInputDef) bd;
				genGuardedInput(altCase, ad, cid);
			} else {
				ChanOutputDef cod = (ChanOutputDef) bd;
				genGuardedOutput(altCase, ad, cod);
			}
		} else { // requires a proxy channel
			proxyChanRequired = true;
			genGuardedProxyInput(altCase, ad);
		}
	}


	@Override 
	public void exitExprExtCh(CSPMParser.ExprExtChContext ctx) { 
		ST st = getTemplate("select");
		if (def(ctx) == null) { return ; }
		ExternalChoiceDef ecDef = (ExternalChoiceDef) def(ctx);
		for (AlternativeDef ad : ecDef.alternatives) {
			// An alternative may:
			// (a) be guarded by a boolean condition and either 
			// 	(i) be prefixed by an event to perform
			// 	(ii) have no prefix (in which case we require a proxy channel)
			ST altCase = getTemplate("case");
			if (ad.guarded) {
				genGuardedCase(altCase, ad);	
				st.add("case", altCase);
				continue;
			}
			// (b) have no guard and no prefix
			if (ad.requiresProxyChannel) {
				proxyChanRequired = true;
				altCase.add("trigger", "<< placeholder for proxy chanop");
				genProxyInput(altCase, ad);
				st.add("case", altCase);
				continue;
			}
			// (c) have no guard and be prefixed by an event to perform
			altCase.add("trigger", getST(ad.first));
			altCase.add("line", getST(ad.rest));
			st.add("case", altCase);	
		}
		setST(ctx, st);
		if (getST(ctx) != null && debug) {
			logger.trace("exitExprExtCh: " + getST(ctx).render());
		}
		if (!(ctx.getParent() instanceof ExprExtChContext)) {
			inExtChoice = false;	
		}
	}

	@Override public void enterExprIf(CSPMParser.ExprIfContext ctx) { 
		parensProtect = true;	
	}

	@Override public void exitExprIf(CSPMParser.ExprIfContext ctx) { 
		ST st = getTemplate("ifStmt");
		st.add("cond", getST(ctx.expression(0)));
		if (scanningFunction 
				&& (!(ctx.expression(1) instanceof ExprIfContext))
				&& (!(ctx.expression(1).getText().matches(".*error.*")))) {
			ST returnST = getTemplate("return");
			returnST.add("e", getST(ctx.expression(1)));
			st.add("thenLine", returnST);
		} else {
			st.add("thenLine", getST(ctx.expression(1)));
		}
		if (scanningFunction
				&& (!(ctx.expression(2).getText().matches(".*error.*")))
				&& ((ctx.expression(2) != null) 
					&& (!(ctx.expression(2) instanceof ExprIfContext)))) {
			ST returnST = getTemplate("return");
			returnST.add("e", getST(ctx.expression(2)));
			st.add("elseLine", returnST);
		} else {
			st.add("elseLine", getST(ctx.expression(2)));
		}	
		setST(ctx, st);
		if (getST(ctx) != null && debug) {
			logger.trace("exitExprIf: " + getST(ctx).render());
		}
		parensProtect = false;
	}

	@Override public void exitExprPrimary(CSPMParser.ExprPrimaryContext ctx) { 
		setST(ctx, getST(ctx.primaryExpr()));
		if (getST(ctx) != null && debug) {
			logger.trace("exitExprPrimary: " + getST(ctx).render());
		}
	}
	// expression alternatives -- END 


	// primaryExpr alternatives -- BEGIN
	@Override public void exitPExprBuiltInCall(CSPMParser.PExprBuiltInCallContext ctx) { 
		setST(ctx, getST(ctx.builtInFunc()));
		if (getST(ctx) != null && debug) {	
			logger.trace("exitPExprBuiltInCall: " + getST(ctx).render());
		}
	}

	@Override public void exitPExprBuiltInProcess(CSPMParser.PExprBuiltInProcessContext ctx) { 
		setST(ctx, getST(ctx.builtInProcess()));
		if (getST(ctx) != null && debug) {	
			logger.trace("exitPExprBuiltInCall: " + getST(ctx).render());
		}
	}

	@Override public void exitPExprBuiltInIdentifier(CSPMParser.PExprBuiltInIdentifierContext ctx) { 
		setST(ctx, getST(ctx.builtInIdentifier()));
		if (getST(ctx) != null  && debug) {
			logger.trace("exitPExprBuiltInIdentifier: " + getST(ctx).render());
		}
	}
	
	@Override public void exitPExprSeqExpr(CSPMParser.PExprSeqExprContext ctx) { 
		setST(ctx, getST(ctx.seqExpr()));
		if (getST(ctx) != null && debug) {	
			logger.trace("exitPExprSeqExpr: " + getST(ctx).render());
		}
	}
	
	@Override public void exitPExprSetExpr(CSPMParser.PExprSetExprContext ctx) { 
		setST(ctx, getST(ctx.setExpr()));
		if (getST(ctx) != null && debug) {	
			logger.trace("exitPExprSetExpr: " + getST(ctx).render());
		}
	}

	private ST genSubstateHandover(PExprUserFuncCallContext ctx,
			ProcInvocationDef pid) {
		ST st = getTemplate("block");
		ProcessDef nextSubstate = pid.proc();
		//logger.trace("genSubstateHandover: " + target.name());
		
		// 1. Assign values from 'next' substate invocation to state variables
		for (ParamDef prd : nextSubstate.params()) {
			int idx = nextSubstate.params().indexOf(prd);
			if (!(prd.name().contains(pid.argExprs().get(idx).getText()))) {	
			//	logger.trace("param #" + idx + ": " + prd.name() 
			//			+ " <-- " + pid.argExprs().get(idx).getText());
				ST assignST = getTemplate("assign").add("v", 
						rxAbbr + "." + prd.name());
				assignST.add("e", getST(pid.argExprs().get(idx)));
				assignST.add("c", pid.proc().name() + "("
						+ prd.name() + " := " 
						+ pid.argExprs().get(idx).getText() + ")");
				st.add("line", assignST);
			}
			
		}
		// 2. Zero-out values on state variables that aren't used by
		// the next substate 
		for (ParamDef currParam : currentSubstate.params()) {
			boolean stillInUse = false;
			for (ParamDef nextParam : nextSubstate.params()) {
				if (currParam.name().contains(nextParam.name())) {
					stillInUse = true;
				}
			}
			if (!(stillInUse)) {
				ST assignST = getTemplate("assign").add("v",
						rxAbbr + "." + currParam.name());
				assignST.add("c", currParam.name() + " no longer in use: zeroing-out value");
				switch (currParam.type()) {
					case BOOL:
						assignST.add("e", getTemplate("cFalse"));
						break;
					case INT: 
						assignST.add("e", "0");
						break;
					case SET:
						assignST.add("e", getTemplate("cspNewIST"));
						break;
					case LIST:
						assignST.add("e", getTemplate("cspNewISQ"));
						break;
				}
				st.add("line", assignST);
			}
		}
		// 3. Tell the process object what the next substate is
		st.add("line", getTemplate("return").add("e", procPfx + "_" + nextSubstate.name()));
		return st;
	}

	@Override 
	public void exitPExprUserFuncCall(PExprUserFuncCallContext ctx) { 
		// This is either a call to a user-defined function or 
		// a process invocation.
		
		ST st = null;
		
		if (def(ctx) instanceof ProcInvocationDef) {
			//logger.trace("we got a process invocation on: " + ctx.getText());
			ProcInvocationDef pid = (ProcInvocationDef) def(ctx);
			st = genSubstateHandover(ctx, pid);
			if (getST(ctx) != null && debug) {
				logger.trace("exitPExprUserFuncCall: " + getST(ctx).render());
			}
		} else {
			String name = ctx.Name().getText();
			Symbol nameSym = tmpScope.resolve(name);
			if (nameSym == null) {
				logger.error("Symbol resolution failed for: " + name);
			}
			BaseDef bd = nameSym.def();
			if (bd instanceof FunctionDef) {
				if (scanForProcNet) {
					st = getTemplate("fCall").add("n", name);
					st.add("args", getST(ctx.expressionList()));
				} else {
					st = getTemplate("mCall").add("o", rxAbbr);
					st.add("m", name);
					st.add("args", getST(ctx.expressionList()));
				}
			} else {
				logger.error("Unanticipated def against this name: " + ctx.getText());
			}
		}
		
		setST(ctx, st);
		
		if (getST(ctx) != null && debug) {
			logger.trace("exitPExprUserFuncCall: " + getST(ctx).render());
		} 
	}
	
	@Override public void exitPExprName(CSPMParser.PExprNameContext ctx) { 
		String name = ctx.Name().getText();
		Symbol nameSym = tmpScope.resolve(name);
		if (nameSym == null) { 
			logger.error("Symbol resolution failed for: " + name);
		}
		BaseDef bd = nameSym.def();
		if (bd instanceof ChannelDef) {
			if (name.matches("(.*)Out")) {
				ST chanST = getTemplate("mAttrib").add("o", rxAbbr);
				chanST.add("a", name);
				ST st = getTemplate("chOut").add("ch", chanST);
				setST(ctx, st);
			} else if (name.matches("(.*)In")) {
				ST chanST = getTemplate("mAttrib").add("o", rxAbbr);
				chanST.add("a", name);
				ST st = getTemplate("chIn").add("ch", chanST);
				setST(ctx, st);
			} else {
				ST st = getTemplate("mAttrib").add("o", rxAbbr);
				st.add("a", name);
				setST(ctx, st);
			}
//		} else if (bd instanceof ConstCharDef) {
//			ST st = getTemplate("mAttrib").add("o", rxAbbr);
//			st.add("a", name);
//			setST(ctx, st);
//		} else if (bd instanceof ConstIntDef) {
//			ST st = getTemplate("mAttrib").add("o", rxAbbr);
//			st.add("a", name);
//			setST(ctx, st);
//		} else if (bd instanceof ConstStringDef) {
//			ST st = getTemplate("mAttrib").add("o", rxAbbr);
//			st.add("a", name);
//			setST(ctx, st);
//		} else if (bd instanceof ExtBaseDef) {
//			ST st = getTemplate("mAttrib").add("o", rxAbbr);
//			st.add("a", name);
//			setST(ctx, st);
		} else if (bd instanceof MsgStructDef) {
			setST(ctx, getTemplate("pe").add("e", ctx.Name().getText()));
		} else if (!(scanningFunction) && bd instanceof ParamDef) {
			ST st = getTemplate("mAttrib").add("o", rxAbbr);
			st.add("a", name);
			setST(ctx, st);
		} else if (scanningFunction && bd instanceof ParamDef) {
			setST(ctx, getTemplate("pe").add("e", ctx.Name().getText()));
//		} else if (bd instanceof FunctionDef) {
//			ST st = getTemplate("mAttrib").add("o", rxAbbr);
//			st.add("a", name);
//			setST(ctx, st);
		} 	else if (bd instanceof VarBaseDef) {
			setST(ctx, getTemplate("pe").add("e", ctx.Name().getText()));
		} else {
			ST st = getTemplate("mAttrib").add("o", rxAbbr);
			st.add("a", name);
			setST(ctx, st);
		}
		if (getST(ctx) != null && debug) {
			logger.trace("exitPExprName: " + getST(ctx).render());
		}
	}
	
	@Override public void exitPExprLiteral(CSPMParser.PExprLiteralContext ctx) { 
		setST(ctx, getST(ctx.literal()));
		if (getST(ctx) != null && debug) {	
			logger.trace("exitPExprLiteral: " + getST(ctx).render());
		}
	}
	// primaryExpr alternatives -- END

	// literal alternatives -- BEGIN
	@Override public void exitLitInt(CSPMParser.LitIntContext ctx) { 
		setST(ctx, getTemplate("pe").add("e", ctx.Int().getText()));
		if (getST(ctx) != null && debug) {	
		logger.trace("exitLitInt: " + getST(ctx).render());
		}
	}
	
	@Override public void exitLitChar(CSPMParser.LitCharContext ctx) { 
		setST(ctx, getTemplate("pe").add("e", ctx.Char().getText()));
		if (getST(ctx) != null && debug) {	
		logger.trace("exitLitChar: " + getST(ctx).render());
		}
	}

	@Override public void exitLitString(CSPMParser.LitStringContext ctx) { 
		setST(ctx, getTemplate("pe").add("e", ctx.String().getText()));
		if (getST(ctx) != null && debug) {	
		logger.trace("exitLitString: " + getST(ctx).render());
		}
	}
	// literal alternatives -- END

	@Override public void exitExpressionList(CSPMParser.ExpressionListContext ctx) { 
		ST st = getTemplate("list");
		for (ExpressionContext ec : ctx.expression()) {
			st.add("item", getST(ec));
		}
		setST(ctx, st);
		if (getST(ctx) != null && debug) {
			logger.trace("exitExpressionList: " + getST(ctx).render());
		}
	}

	// seqExpr alternatives -- BEGIN
	@Override public void exitListEmpty(CSPMParser.ListEmptyContext ctx) { 
		setST(ctx, getTemplate("cspNewISQ"));
		if (getST(ctx) != null && debug) {
			logger.trace("exitListEmpty: " + getST(ctx).render());
		}
	}
	
	@Override public void exitListRangedInt(CSPMParser.ListRangedIntContext ctx) { 
		ST st = getTemplate("cspNewISQ");
		st = getTemplate("cspISQAddRange").add("o", st);
		st.add("e", getST(ctx.expression(0)));
		st.add("e", getST(ctx.expression(1)));
		setST(ctx, st);
		if (getST(ctx) != null && debug) {
			logger.trace("exitSeqRangedInt: " + getST(ctx).render());
		}
	}
	
	@Override
	public void exitList(ListContext ctx) {
		ST st = getTemplate("cspNewISQ");
		st.add("e", getST(ctx.expressionList()));
		setST(ctx, st);
		if (getST(ctx) != null && debug) {
			logger.trace("exitList: " + getST(ctx).render());
		}
	}
		
	@Override public void exitListTail(CSPMParser.ListTailContext ctx) { 
		ST st = getTemplate("cspISQTail").add("o", 
				getST(ctx.primaryExpr()));
		setST(ctx, st);
		if (getST(ctx) != null && debug) {
			logger.trace("exitListTail: " + getST(ctx).render());
		}
	}
	
	@Override public void exitListFromSet(CSPMParser.ListFromSetContext ctx) { 
		ST st = getTemplate("cspISTSeq").add("o", 
				getST(ctx.primaryExpr()));
		setST(ctx, st);
		if (getST(ctx) != null && debug) {
			logger.trace("exitListFromSet: " + getST(ctx).render());
		}
	}
	// seqExpr alternatives -- END 


	// setExpr alternatives -- BEGIN
	@Override public void exitSetEmpty(CSPMParser.SetEmptyContext ctx) { 
		setST(ctx, getTemplate("cspNewIST"));
		if (getST(ctx) != null && debug) {
			logger.trace("exitSetEmpty: " + getST(ctx).render());
		}
	}

	@Override public void exitSetRangedInt(CSPMParser.SetRangedIntContext ctx) { 
		ST st = getTemplate("cspNewIST");
		st = getTemplate("cspISTAddRange").add("o", st);
		st.add("e", getST(ctx.expression(0)));
		st.add("e", getST(ctx.expression(1)));
		setST(ctx, st);
		if (getST(ctx) != null && debug) {
			logger.trace("exitSetRangedInt: " + getST(ctx).render());
		}
	}

	@Override public void exitSet(CSPMParser.SetContext ctx) { 
		ST st = getTemplate("cspNewIST");
		st.add("e", getST(ctx.expressionList()));
		setST(ctx, st);
		if (getST(ctx) != null && debug) {
			logger.trace("exitSet: " + getST(ctx).render());
		}
	}
	
	@Override public void exitSetDiff(CSPMParser.SetDiffContext ctx) { 
		ST st = getTemplate("cspISTDiff").add("o", 
				getST(ctx.primaryExpr(0)));
		st.add("e", getST(ctx.primaryExpr(1)));
		logger.trace("<< exitSetDiff begin ");
		logger.trace(getST(ctx.primaryExpr(0)).render());
		logger.trace(getST(ctx.primaryExpr(1)).render());
		logger.trace(ctx.getText());
		logger.trace("<< exitSetDiff end ");
		setST(ctx, st);
		if (getST(ctx) != null && debug) {
			logger.trace("exitSetDiff: " + getST(ctx).render());
		}
	}

	@Override public void exitSetInter(CSPMParser.SetInterContext ctx) { 
		ST st = getTemplate("cspISTInter").add("o",
				getST(ctx.primaryExpr(0)));
		st.add("e", getST(ctx.primaryExpr(1)));
		logger.trace("<< exitSetInter begin ");
		logger.trace(ctx.primaryExpr(0).getText());
		logger.trace(ctx.primaryExpr(1).getText());
		logger.trace(ctx.getText());
		logger.trace("<< exitSetInter end");
		setST(ctx, st);
		if (getST(ctx) != null  && debug) {
			logger.trace("exitSetInter: " + getST(ctx).render());
		}
	}
	
	@Override public void exitSetUnion(CSPMParser.SetUnionContext ctx) { 
		ST st = getTemplate("cspISTUnion").add("o",
				getST(ctx.primaryExpr(0)));
		st.add("e", getST(ctx.primaryExpr(1)));
		logger.trace("<< exitSetUnion begin ");
		logger.trace(ctx.primaryExpr(0).getText());
		logger.trace(ctx.primaryExpr(1).getText());
		logger.trace(ctx.getText());
		logger.trace("<< exitSetUnion end ");
		setST(ctx, st);
		if (getST(ctx) != null && debug) {
			logger.trace("exitSetUnion: " + getST(ctx).render());
		}
	}
	
	@Override public void exitSetFromList(CSPMParser.SetFromListContext ctx) { 
		ST st = getTemplate("cspISQSet").add("o",
				getST(ctx.primaryExpr()));
		setST(ctx, st);
		if (getST(ctx) != null && debug) {
			logger.trace("exitSetFromList: " + getST(ctx).render());
		}
	}
	// setExpr alternatives -- END 

	// builtInFunc alternatives -- BEGIN 
	@Override
	public void exitSetCard(SetCardContext ctx) {
		setST(ctx, getTemplate("cspISTCard").add("o", 
					getST(ctx.primaryExpr())));
		if (getST(ctx) != null&& debug) {
			logger.trace("exitSetCard: " + getST(ctx).render());
		}
	}

	@Override
	public void exitSeqElemTest(SeqElemTestContext ctx) {
		ST elemST = getTemplate("cspISQElem");
		elemST.add("e", getST(ctx.primaryExpr(0)));
		elemST.add("o", getST(ctx.primaryExpr(1)));
		setST(ctx, elemST);
		if (getST(ctx) != null && debug) {
			logger.trace("exitSeqElem: " + getST(ctx).render());
		}
	}

	@Override
	public void exitSetEmptyTest(SetEmptyTestContext ctx) {
		setST(ctx, getTemplate("cspISTEmpty").add("o",
					getST(ctx.primaryExpr())));
		if (getST(ctx) != null && debug) { 
			logger.trace("exitSeqEmpty: " + getST(ctx).render());
		}
	}


	@Override
	public void exitLogError(LogErrorContext ctx) {
		setST(ctx, getTemplate("panic").add("s", ctx.String().getText()));	
		if (getST(ctx) != null && debug) { 
			logger.trace("exitLogError: " + getST(ctx).render());
		}
	}

	@Override
	public void exitSeqHead(SeqHeadContext ctx) {
		setST(ctx, getTemplate("cspISQHead").add("o",
					getST(ctx.primaryExpr())));
		if (getST(ctx) != null && debug) { 
			logger.trace("exitSeqHead: " + getST(ctx).render());
		}
	}

	@Override
	public void exitSeqLen(SeqLenContext ctx) {
		setST(ctx, getTemplate("cspISQLength").add("o",
					getST(ctx.primaryExpr())));
		if (getST(ctx) != null && debug) { 
			logger.trace("exitSeqLen: " + getST(ctx).render());
		}
	}

	@Override
	public void exitSetMemberTest(SetMemberTestContext ctx) {
		ST st = getTemplate("cspISTMember").add("e",
				getST(ctx.primaryExpr(0)));
		st.add("o", ctx.primaryExpr(1));
		setST(ctx, st);
		if (getST(ctx) != null && debug) { 
			logger.trace("exitSetMember: " + getST(ctx).render());
		}
	}

	@Override
	public void exitSeqNullTest(SeqNullTestContext ctx) {
		setST(ctx, getTemplate("cspISQNull").add("o",
					getST(ctx.primaryExpr())));
		if (getST(ctx) != null && debug) { 
			logger.trace("exitSeqNull: " + getST(ctx).render());
		}
	}
	// builtInFunc alternatives -- END 

	// The only built-in identifiers that mean anything to us at the process
	// expression level are 'True/true', 'False/false' and the wildcard '_'
	@Override
	public void exitBuiltInIdentifier(BuiltInIdentifierContext ctx) {
		if (ctx.False() != null) {
			setST(ctx, getTemplate("cFalse"));
		} else if (ctx.True() != null ) {
			setST(ctx, getTemplate("cTrue"));
		} else if (ctx.Wildcard() != null) {
			setST(ctx, getTemplate("cWildcard"));
		} 
		if (getST(ctx) != null && debug) {
			logger.trace("exitBuiltInIdentifier: " + getST(ctx).render());
		}
	}

	// We have no sensible implementation for DIV but SKIP and STOP are both 
	// implemented as different forms of shutdown.
	@Override
	public void exitBuiltInProcess(BuiltInProcessContext ctx) {
		if (ctx.Stop() != null ) {
			setST(ctx, getTemplate("return").add("e", 
						procPfx +  getTemplate("cStop").render()));
		} else if (ctx.Skip() != null ) {
			setST(ctx, getTemplate("return").add("e", 
						procPfx +  getTemplate("cSkip").render()));
		}
		if (getST(ctx) != null && debug) {
			logger.trace("exitBuiltInProcess: " + getST(ctx).render());
		}
	}

}
