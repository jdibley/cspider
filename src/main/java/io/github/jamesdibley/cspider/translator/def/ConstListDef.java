/**
 * ConstListDef
 */
package io.github.jamesdibley.cspider.translator.def;

import java.util.ArrayList;
import java.util.List;
import io.github.jamesdibley.cspider.parser.gen.CSPMParser.PatternDeclContext;
import io.github.jamesdibley.cspider.translator.BaseDef;
import io.github.jamesdibley.cspider.translator.ValueType;
import io.github.jamesdibley.cspider.translator.def.ProcessDef;

public class ConstListDef extends BaseDef {
	public List<String> members;
	public ValueType memberType;
	public List<ProcessDef> referencedBy;

	public ConstListDef(PatternDeclContext ctx, 
			List<String> members, ValueType memberType,
			String name) {
		super(ctx, name);
		this.members = new ArrayList<String>(members);
		this.memberType = memberType;
		referencedBy = new ArrayList<ProcessDef>();
		setType(ValueType.LIST);
		markComplete();
	}

	public ValueType getMemberType() {
		return memberType;
	}

	public void setMemberType(ValueType vt) {
		this.memberType = vt;
	}

	public String toString() {
		return defStateToString() + "-> (" +
			members.toString() + ") [" + memberType + "]";
	}

	public void addReferee(ProcessDef pd) {
		referencedBy.add(pd);
	}

	public List<ProcessDef> referencedBy() {
		return referencedBy;
	}
}
