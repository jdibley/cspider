/**
 * VarBaseDef
 */

package io.github.jamesdibley.cspider.translator.def;

import io.github.jamesdibley.cspider.translator.BaseDef;
import io.github.jamesdibley.cspider.translator.ValueType;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;

public abstract class VarBaseDef extends BaseDef {

	public VarBaseDef(ParserRuleContext ctx, ValueType type, String name) {
		super(ctx, name);
		setType(type);
	}

	
	public String toString() {
		switch(type()) {
		case UNKNOWN:
			return "byte";
		case INT:
			return "int";
		case BOOL:
			return "bool";
		case CHAR:
			return "rune";
		case STRING:
			return "set";
		}
		return "";
	}

}
