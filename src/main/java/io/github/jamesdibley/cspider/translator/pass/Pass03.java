/**
 * Pass03
 * Gather signal chan decls
 */
package io.github.jamesdibley.cspider.translator.pass;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
import org.antlr.v4.runtime.tree.xpath.XPath;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import io.github.jamesdibley.cspider.parser.gen.*;
import io.github.jamesdibley.cspider.parser.gen.CSPMParser.*;
import io.github.jamesdibley.cspider.translator.BasePass;
import io.github.jamesdibley.cspider.translator.TranState;
import io.github.jamesdibley.cspider.translator.def.ChannelDef; 
import io.github.jamesdibley.cspider.translator.def.ChannelType;

public class Pass03 extends BasePass {
	private static final Logger logger = LogManager.getLogger(Pass03.class);
	private String xpath = "/sourcefile/declaration/chanDecl";

	public Pass03(TranState tState, ParseTreeWalker walker, CSPMParser parser) {
		super(tState, walker, parser);
		logger.trace("Gather simplest chan decls");
	}

	public void process(SourcefileContext tree) {
		for (ParseTree t : XPath.findAll((ParseTree) tree, xpath, parser)) {
			if (t instanceof ChanDeclContext) {
				registerChanDecl((ChanDeclContext) t);
			}
		}
	}

	public void registerChanDecl(ChanDeclContext ctx) {
		for (TerminalNode n : ctx.Name()) {
			ChannelDef cDef = 
				new ChannelDef(ctx, n.getText(), ChannelType.EVENT);
			cDef.setTranState(tState);
			setDef(n, cDef);
			String name = n.getText();
			// Channels suffixed 'In' or 'Out' are only visible
			// to process objects.
			if (name.matches("(.*)In")) {
				cDef.setChannelInput();
			} else if (name.matches("(.*)Out")) {
				cDef.setChannelOutput();
			}
			tState.symTable.currentScope().define(cDef, name);
		}
	}
}
