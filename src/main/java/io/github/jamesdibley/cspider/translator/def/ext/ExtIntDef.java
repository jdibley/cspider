/**
 * ExtIntDef
 */
package io.github.jamesdibley.cspider.translator.def.ext;
 
import io.github.jamesdibley.cspider.translator.ValueType;
import io.github.jamesdibley.cspider.translator.def.ExtBaseDef;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;

public class ExtIntDef extends ExtBaseDef {
	public ExtIntDef(ParserRuleContext ctx, String name) {
		super(ctx, ValueType.INT, name);
		this.markComplete();
	}
}
