/**
 * SeqDef
 */
package io.github.jamesdibley.cspider.translator.def;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.LinkedHashMap;
import java.util.Map;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
import io.github.jamesdibley.cspider.parser.gen.CSPMParser.PatternDeclContext;
import io.github.jamesdibley.cspider.translator.BaseDef;
import io.github.jamesdibley.cspider.translator.ValueType;

public class SeqDef extends BaseDef {
	private List<String> elements;
	private ValueType elementType;
	private String beginRange;
	private String endRange;
	private boolean rangedIntSeq;
	private Map<String, ProcessDef> referencingProcs;
	private Map<String, FunctionDef> referencingFuncs;
	
	public SeqDef(ParserRuleContext ctx, List<String> elements,
			ValueType elementType, String name) {
		super(ctx, name);
		setType(ValueType.LIST);
		this.elements = new ArrayList<String>(elements);
		this.elementType = elementType;
		referencingProcs = new LinkedHashMap<String, ProcessDef>();
		referencingFuncs = new LinkedHashMap<String, FunctionDef>();
		rangedIntSeq = false;
	}

	public SeqDef(ParserRuleContext ctx, String beginRange, String endRange,
			String name) {
		super(ctx, name);
		setType(ValueType.LIST);
		this.elementType = ValueType.INT;
		this.beginRange = beginRange;
		this.endRange = endRange;
		elements = null;
		rangedIntSeq = true;
		referencingProcs = new LinkedHashMap<String, ProcessDef>();
		referencingFuncs = new LinkedHashMap<String, FunctionDef>();
	}

	public List<String> elements() {
		return elements;
	}

	public ValueType elementType() {
		return elementType;
	}

	public void setElementType(ValueType vt) {
		this.elementType = vt;
	}

	public void addReferencingProc(ProcessDef pd) {
		referencingProcs.put(pd.name(), pd);
	}

	public void addReferencingFunc(FunctionDef fd) {
		referencingFuncs.put(fd.name(), fd);
	}

	public Collection<ProcessDef> referencingProcs() {
		return referencingProcs.values();
	}

	public Collection<FunctionDef> referencingFuncs() {
		return referencingFuncs.values();
	}

	public boolean referencedFromProcs() {
		return !(referencingProcs.isEmpty());
	}

	public boolean referencedFromFuncs() {
		return !(referencingFuncs.isEmpty());	
	}

	public String toString() {
		List<String> procNames = new ArrayList<String>();
		List<String> funcNames = new ArrayList<String>();
		for (ProcessDef p : referencingProcs.values()) {
			procNames.add(p.name());
		}
		for (FunctionDef f : referencingFuncs.values()) {
			funcNames.add(f.name());
		}
		if (rangedIntSeq) {
			return defStateToString() + "-> ["
				+ beginRange + ".." + endRange + "]"
				+ "(" + elementType + ")"
				+ procNames
				+ funcNames;
		} else {
			return defStateToString() + "-> ["
				+ elements.toString() 
				+ "(" + elementType + ")"
				+ procNames
				+ funcNames;
		}
	}
}
