/**
 * OutputFile
 */
package io.github.jamesdibley.cspider.translator.go;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set; 

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.stringtemplate.v4.*;
import io.github.jamesdibley.cspider.translator.go.OutputModel;

public class OutputFile {
	private static final Logger logger =
		LogManager.getLogger(OutputFile.class);

	private ST file;
	private String outputFileID;

	private Map<String, ST> importMap;
	private Map<String, ST> constMap;
	private Map<String, ST> varMap;
	private Map<String, ST> chanStructMap;
	private Map<String, ST> procStructMap;
	private Map<String, ST> procMap;
	private Map<String, ST> funcMap;
	private Boolean reqIntSetInclude;
	private Boolean reqIntSeqInclude;

	public OutputFile(String name, String packageName) {
		file = OutputModel.getTemplate("file");
		file.add("pname", packageName);
		outputFileID = name;

		chanStructMap = 
			new LinkedHashMap<String, ST>();
		constMap = 
			new LinkedHashMap<String, ST>();
		funcMap = 
			new LinkedHashMap<String, ST>();	
		importMap = 
			new LinkedHashMap<String, ST>();
		procMap = 
			new LinkedHashMap<String, ST>();
		procStructMap = 
			new LinkedHashMap<String, ST>();
		varMap = 
			new LinkedHashMap<String, ST>();
		reqIntSeqInclude = false;
		reqIntSetInclude = false;
	}

	public String render() {
		ST gImports = OutputModel.getTemplate("gImports");
		for (String str : importMap.keySet()) {
			gImports.add("importDecl", importMap.get(str));
		}
		file.add("gImports", gImports);

		ST gConsts = OutputModel.getTemplate("gConsts");
		for (String str : constMap.keySet()) {
			gConsts.add("constDecl", constMap.get(str));
		}
		file.add("gConsts", gConsts);

		ST gVars = OutputModel.getTemplate("gVars");
		for (String str : varMap.keySet()) {
			gVars.add("varDecl", varMap.get(str));
		}
		file.add("gVars", gVars);

		ST gChanStructs = OutputModel.getTemplate("gChanStructs");
		for (String str : chanStructMap.keySet()) {
			gChanStructs.add("chanStruct", chanStructMap.get(str));
		}
		file.add("gChanStructs", gChanStructs);

		ST gProcStructs = OutputModel.getTemplate("gProcStructs");
		for (String str : procStructMap.keySet()) {
			//logger.trace("outputting proc struct: " + str);
			gProcStructs.add("procStruct", procStructMap.get(str));
		}
		file.add("gProcStructs", gProcStructs);

		ST gFuncs = OutputModel.getTemplate("gFuncs");
		for (String str : funcMap.keySet()) {
			gFuncs.add("func", funcMap.get(str));
		}
		for (String str : procMap.keySet()) {
			gFuncs.add("func", procMap.get(str));
		}
		file.add("gFuncs", gFuncs);

		return file.render();
	}

	public void addIntSeqImport() {
		ST seqImport = OutputModel.getTemplate("pExpr");
		seqImport.add("expr", "bitbucket.com/jdibley/cspider/intSeq");
		importMap.put("intSeq", seqImport);	
	}

	public void addIntSetImport() {
		ST setImport = OutputModel.getTemplate("pExpr");
		setImport.add("expr", "bitbucket.com/jdibley/cspider/intSet");
		importMap.put("intSet", setImport);
	}

	public void addConst(String id, ST st) {
		constMap.put(id, st);
	}

	public void addVar(String id, ST st) {
		varMap.put(id, st);
	}
	
	public void addProcStruct(String id, ST stPrc) {
	 	procStructMap.put(id, stPrc);
	}

	public void addChanStruct(String id, ST st) {
		chanStructMap.put(id, st);
	}

	public void addProc(String id, ST st) {
		procMap.put(id, st);
	}

	public void addFunc(String id, ST st) {
		funcMap.put(id, st);
	}

	public Set<String> constKeys() {
		return constMap.keySet();
	}

	public Set<String> varKeys() {
		return varMap.keySet();
	}

	public Set<String> chanStructKeys() {
		return chanStructMap.keySet();
	}

	public Set<String> procStructKeys() {
		return procStructMap.keySet();
	}

	public Set<String> procKeys() {
		return procMap.keySet();
	}

	public Set<String> funcKeys() {
		return funcMap.keySet();
	}

	public ST getConst(String id) {
		return constMap.get(id);
	}

	public ST getVar(String id) {
		return varMap.get(id);
	}

	public ST getChanStruct(String id) {
		return chanStructMap.get(id);
	}

	public ST getProcStruct(String id) {
		return procStructMap.get(id);
	}

	public ST getProc(String id) {
		return procMap.get(id);
	}

	public ST getFunc(String id) {
		return funcMap.get(id);
	}
}
