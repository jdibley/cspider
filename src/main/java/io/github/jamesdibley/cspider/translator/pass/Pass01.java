/**
 * Pass01.java
 */
package io.github.jamesdibley.cspider.translator.pass;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import io.github.jamesdibley.cspider.parser.gen.*;
import io.github.jamesdibley.cspider.parser.gen.CSPMParser.*;
import io.github.jamesdibley.cspider.translator.BasePass;
import io.github.jamesdibley.cspider.translator.TranState;
import io.github.jamesdibley.cspider.translator.ValueType;
import io.github.jamesdibley.cspider.translator.def.SetDef;

public class Pass01 extends BasePass {
	private static final Logger logger = LogManager.getLogger(Pass01.class);

	public Pass01(TranState tState, ParseTreeWalker walker,
			CSPMParser parser) {
		super(tState, walker, parser);
		logger.trace("Set up base configuration for translation.");
	}

	public void process(SourcefileContext tree) {
		defineBaseSets(tree);
	}

	private void defineBaseSets(SourcefileContext tree) {
		// These allow us to represent the 'generic' CSPM types
		// in channel specifications.
		SetDef intSet = new SetDef(tree, ValueType.INT, "Int");
		SetDef boolSet = new SetDef(tree, ValueType.BOOL, "Bool");
		SetDef charSet = new SetDef(tree, ValueType.CHAR, "Char");
		SetDef indexSet = new SetDef(tree, ValueType.INT, "Index");
		intSet.setTranState(tState);
		boolSet.setTranState(tState);
		charSet.setTranState(tState);
		indexSet.setTranState(tState);
		tState.symTable.currentScope().define(intSet, intSet.name()); 
		tState.symTable.currentScope().define(boolSet, boolSet.name());
		tState.symTable.currentScope().define(charSet, charSet.name());
		tState.symTable.currentScope().define(indexSet, indexSet.name());
	}
}
