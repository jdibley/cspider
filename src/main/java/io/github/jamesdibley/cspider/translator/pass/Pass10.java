/**
 * Pass10
 */
package io.github.jamesdibley.cspider.translator.pass;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
import org.antlr.v4.runtime.tree.xpath.XPath;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import io.github.jamesdibley.cspider.parser.gen.*;
import io.github.jamesdibley.cspider.parser.gen.CSPMParser.*;
import io.github.jamesdibley.cspider.translator.BasePass;
import io.github.jamesdibley.cspider.translator.TranState;
import io.github.jamesdibley.cspider.translator.BaseDef;
import io.github.jamesdibley.cspider.translator.ValueType;
import io.github.jamesdibley.cspider.translator.def.*;
import io.github.jamesdibley.cspider.translator.def.var.*;
import io.github.jamesdibley.cspider.translator.symbol.*;

public class Pass10 extends ProcFuncCommon {
	private static final Logger logger = LogManager.getLogger(Pass10.class);
	private String xPath = "//letClause/parametricPatternDecl"; 

	public Pass10(TranState tState, ParseTreeWalker walker,
			CSPMParser parser) {
		super(tState, walker, parser);
		logger.trace("Gather parameterised processes and "
				+ "functions defined within LDEs.");
	}

	public void process(SourcefileContext tree) {
		processLDEParametrics(tree, xPath);
	}

	private void processLDEParametrics(ParseTree tree, String xPath) {
		for (ParseTree t : XPath.findAll(tree, xPath, parser)) {
			String tmpName = null;
			Scope parentScope = null;
			boolean isProcess = false;
			List<ParamDef> tmpParams = new ArrayList<ParamDef>();

			ParametricPatternDeclContext ppdc = 
				(ParametricPatternDeclContext) t; 
			NamePatternContext npc = ppdc.patternLHS().namePattern();
			
		// Fetch and load the parent scope.
		// The parent of t should be a LetClause, which should have a 
		// LetClauseDef associated with it.
			LetClauseDef lcd = (LetClauseDef) def(t.getParent());
			parentScope = lcd.getScope();
			
			if (tState.isInMap(t)) { continue; }
			if (!isParametricPattern(npc)) {
				logger.error("Unsupported decl found. Check "
						+ "input for type annotations. "
						+ npc.getText());
				continue;
			}
			NpArgsContext nac = (NpArgsContext) npc;
			tmpName = getTmpName(npc);
			//logger.trace(tmpName);	
		// Push a new scope here to hold any new identifiers
		// encountered in the process expression (as well as an LDE, 
		// should one exist.)
			tState.symTable.pushScope(parentScope);
			registerTmpParams(ppdc, nac, tmpParams);
	
			Map<String, String> tmpRenameMap = 
				processRenamingBlock(ppdc);
			copyParentRenaming(ppdc, tmpRenameMap);	

			ExpressionContext e = ppdc.expression(1);
			classifying = true;
			walker.walk(this, e);
			classifying = false;
		// Having walked the pattern, we now need to decide if it's a 
		// process or a function. There are three criteria for this:
		// 	1. expression(0) matches 'Proc' => Proc
		// 	2. presence of a renaming block => Proc
		// 	3. look at what the walker discovered... 
			String tmpRetType = ppdc.expression(0).getText();

			if (tmpRetType.matches("Proc")) {
				isProcess = true;
			} else if (ppdc.expression() instanceof ExprRenameContext) {
				isProcess = true;
			} else {
				if (walkedProcessDecl) {
					 isProcess = true;
				}
			}

			if (isProcess) {
				registerParametricProcess(ppdc, tmpName, 
						tmpParams, tmpRenameMap);
			} else {
				registerFunction(ppdc, tmpName, tmpParams);
			}
			tState.symTable.popScope();
			//walkedChanRefs.clear();
			walkedSetRefs.clear();
		}
	}

	private void copyParentRenaming(ParametricPatternDeclContext ppdc,
			Map<String, String> renamingMap) {
		if (!(ppdc.getParent() instanceof LetClauseContext)) {
			logger.error("Unexpected parent found in copyParentRenaming.");
			return;
		}
		LetClauseContext lcc = (LetClauseContext) ppdc.getParent();
		BaseDef bd = def(lcc);
		if (!(bd instanceof LetClauseDef)) {
			logger.error("Unexpected def found in copyParentRenaming.");
			return;
		}
		LetClauseDef lcd = (LetClauseDef) bd;
		ProcessDef parent = (ProcessDef) lcd.parent();

		for (String s : parent.chanRenameKeys()) {
			renamingMap.put(s, parent.chanRenaming(s));
		}
	}

}
