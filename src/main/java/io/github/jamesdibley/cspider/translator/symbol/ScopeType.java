/**
 * ScopeType
 */
package io.github.jamesdibley.cspider.translator.symbol;

public enum ScopeType {
	GLOBAL,
	LOCAL;
}
