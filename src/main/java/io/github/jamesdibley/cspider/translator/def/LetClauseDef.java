/**
 * LetClauseDef
 */
package io.github.jamesdibley.cspider.translator.def;

import io.github.jamesdibley.cspider.parser.gen.CSPMParser.LetClauseContext;
import io.github.jamesdibley.cspider.translator.BaseDef;
import io.github.jamesdibley.cspider.translator.symbol.Scope;
import io.github.jamesdibley.cspider.translator.def.ProcessDef;

public class LetClauseDef extends BaseDef {
	private BaseDef parent;
	private boolean containsProcDef;
	private Scope scope;

	public LetClauseDef(LetClauseContext ctx, Scope scope) {
		super(ctx, null);
		this.scope = scope;
		parent = null;
		containsProcDef = false;
	}

	public LetClauseDef(LetClauseContext ctx, Scope scope,
			ProcessDef pd) {
		super(ctx, null);
		this.scope = scope;
		parent = pd;
		containsProcDef = false;
	}

	public LetClauseDef(LetClauseContext ctx, Scope scope,
			FunctionDef fd) {
		super(ctx, null);
		this.scope = scope;
		parent = fd;
		containsProcDef = false;
	}

	public void setParent(ProcessDef parent) {
		this.parent = parent;
	}

	public BaseDef parent(){
		return parent;
	}

	public void setContainsProcDef() {
		if (parent instanceof ProcessDef) {
			containsProcDef = true;
		}
	}	

	public Scope getScope() {
		return scope;
	}

	public boolean containsProcDef() {
		return containsProcDef;
	}

	public String toString() {
		return parent.toString() + containsProcDef;
	}
}
