/**
 * ListDef
 */
package io.github.jamesdibley.cspider.translator.def;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.LinkedHashMap;
import java.util.Map;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
import org.stringtemplate.v4.*;
import io.github.jamesdibley.cspider.parser.gen.CSPMParser.*;
import io.github.jamesdibley.cspider.translator.BaseDef;
import io.github.jamesdibley.cspider.translator.ValueType;
import io.github.jamesdibley.cspider.translator.symbol.*;

public class ListDef extends BaseDef {
	private List<String> elements;
	public ValueType elementType;
	public Map<String, ProcessDef> referencingProcs;
	public Map<String, FunctionDef> referencingFuncs;	
	private Map<String, ExtBaseDef> extRefs;
	private ST declTemplate;

	public ListDef(ParserRuleContext ctx, List<String> elements,
			ValueType elementType, String name,
			Scope currentScope) {
		super(ctx, name);
		setType(ValueType.LIST);
		this.elements = new ArrayList<String>(elements);
		this.elementType = elementType;
		referencingProcs = 
			new LinkedHashMap<String, ProcessDef>();
		referencingFuncs = 
			new LinkedHashMap<String, FunctionDef>();
		extRefs = new LinkedHashMap<String, ExtBaseDef>();
		for (String s : elements) {
			checkElementForExtRef(s, currentScope);
		}
		declTemplate = null;
	}

	public ListDef(ParserRuleContext ctx, String sRange,
			String eRange, String name,
			Scope currentScope) {
		super(ctx, name);
		setType(ValueType.RANGED_LIST);
		elements = new ArrayList<String>();
		elements.add(sRange);
		elements.add(eRange);
		elementType = ValueType.INT;
		referencingProcs = 
			new LinkedHashMap<String, ProcessDef>();
		referencingFuncs = 
			new LinkedHashMap<String, FunctionDef>();
		extRefs = new LinkedHashMap<String, ExtBaseDef>();
		checkElementForExtRef(sRange, currentScope);
		checkElementForExtRef(eRange, currentScope);
		declTemplate = null;
	}

	private void checkElementForExtRef(String s, Scope currentScope) {
		Symbol sym = currentScope.resolve(s);
		if (sym != null && sym.def() != null
				&& sym.def() instanceof ExtBaseDef) {
			extRefs.put(s, (ExtBaseDef) sym.def());
		}
	}

	public Collection<ExtBaseDef> extRefs() {
		return extRefs.values();
	}

	public List<String> elements() {
		return elements;
	}

	public String element(int i) {
		return elements.get(i);
	}

	public ValueType getElementType() {
		return elementType;
	}

	public void setElementType(ValueType vt) {
		this.elementType = vt;
	}

	public void addReferencingProc(ProcessDef pd) {
		referencingProcs.put(pd.name(), pd);	
	}

	public ProcessDef referencingProc(String str) {
		return referencingProcs.get(str);
	}

	public Collection<ProcessDef> referencingProcs() {
		return referencingProcs.values();
	}
	
	public void addReferencingFunc(FunctionDef fd) {
		referencingFuncs.put(fd.name(), fd);	
	}

	public FunctionDef referencingFunc(String str) {
		return referencingFuncs.get(str);
	}

	public Collection<FunctionDef> referencingFuncs() {
		return referencingFuncs.values();
	}

	public boolean referencedFromProcs() {
		return !(referencingProcs.isEmpty());
	}

	public boolean referencedFromFuncs() {
		return !(referencingFuncs.isEmpty());
	}

	public void addDeclTemplate(ST st) {
		declTemplate = st;
	}

	public ST declTemplate() {
		return declTemplate;
	}
}
