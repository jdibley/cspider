/**
 * ConstCharDef
 */
package io.github.jamesdibley.cspider.translator.def;

import io.github.jamesdibley.cspider.parser.gen.CSPMParser.PatternDeclContext;
import io.github.jamesdibley.cspider.translator.BaseDef;
import io.github.jamesdibley.cspider.translator.ValueType;

public class ConstCharDef extends BaseDef {
	public String value;
	public ValueType type;

	public ConstCharDef(PatternDeclContext ctx, String value, String name) {
		super(ctx, name);
		this.value = value;
		type = ValueType.CHAR;
		markComplete();
	}
}
