/**
 * CompType
 */
package io.github.jamesdibley.cspider.translator.def;

public enum CompType {
	UNKNOWN,
	NOT_COMP,
	INTERLEAVED,
	SYNCHRONISED,
	ALPHABETISED,
	INTERFACE,
	LINKED,
	REPL_INTERLEAVED,
	REPL_SYNCHRONISED,
	REPL_ALPHABETISED,
	REPL_INTERFACE
}
