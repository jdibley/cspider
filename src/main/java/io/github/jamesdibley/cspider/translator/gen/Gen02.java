/**
 * Gen02
 */
package io.github.jamesdibley.cspider.translator.gen;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
import org.antlr.v4.runtime.tree.xpath.XPath;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.stringtemplate.v4.*;
import io.github.jamesdibley.cspider.parser.gen.*;
import io.github.jamesdibley.cspider.parser.gen.CSPMParser.*;
import io.github.jamesdibley.cspider.translator.BaseDef;
import io.github.jamesdibley.cspider.translator.BaseGen;
import io.github.jamesdibley.cspider.translator.TranState;
import io.github.jamesdibley.cspider.translator.ValueType;
import io.github.jamesdibley.cspider.translator.def.*;
import io.github.jamesdibley.cspider.translator.go.*;

public class Gen02 extends ProcFuncCommon {
	private static final Logger logger = LogManager.getLogger(Gen02.class);
	private String pRoot = "/sourcefile/declaration";
	private OutputModel o = null;
	private Map<String, ParamDef> processObjectStateVars = null;

	public Gen02(TranState tState, ParseTreeWalker walker,
			CSPMParser parser) {
		super(tState, walker, parser);
		o = tState.output;
		processObjectStateVars =
			new LinkedHashMap<String, ParamDef>();
		logger.trace("Set up process objects.");
	}

	public void process(SourcefileContext tree) {
		for (ProcInvocationDef pid : o.procInvocationDefMap.values()) {
			switch(pid.proc().compType()) {
			case NOT_COMP:
				setupProcObject(o.procNet, pid);
			}
		}
		o.procNet.genGuardedChanFuncs();
	}

	private void setupProcObject(ProcessNetwork n, ProcInvocationDef pid) {
		ProcessDef pd = pid.proc();
		ProcessObject po = genProcObject(pd, pid);
		configProcessObjectLiteral(po, pid);	
		addProcObjectToNet(pd, po);
		gatherProcObjectSubstates(pd, po);
		addSubstateParameters(po);
		addReferencedFunctions(pd, po);
		processObjectStateVars.clear();
	}
	
	private ProcessObject genProcObject(ProcessDef pd,
			ProcInvocationDef pid) {
		switch (pd.compType()) {
		case NOT_COMP:
			break;
		default: 
			return null;
		}
		ProcessObject po = o.newProcessObject(pd, pid);
		return po;
	}

	private void configProcessObjectLiteral(ProcessObject po,
			ProcInvocationDef pid) {
		ProcessDef pd = pid.proc();
		for (String argument : pid.arguments()) {
			int idx = pid.arguments().indexOf(argument);
			ParamDef prd = pd.params().get(idx);
			po.addLiteralInit(prd.name(), argument);
		}
		// This looks scary and makeshift but it's OK. All things
		// declared globally in the CSPM file have a corresponding
		// declaration and initialisation in the 'NewProcessNetwork'
		// function, so we can add and initialise any reference to
		// a previously-global entity by simply passing in its 
		// name /twice/ - first as the field identifier in the literal
		// struct, and second as the value (the literal only ever
		// appears in this function, so the identifier -will- be in 
		// scope).
		for (String srk : pd.setRefKeys()) {
			String setName = pd.setRef(srk).name();
			po.addLiteralInit(setName, setName);
			po.addParam(setName, o.getTemplate("cspISTType"));
			//logger.trace("added set ref key to po literal for " + pd.name());
		}
		for (String lrk : pd.listRefKeys()) {
			String listName = pd.listRef(lrk).name();
			po.addLiteralInit(listName, listName);
			po.addParam(listName, o.getTemplate("cspISQType"));
			//logger.trace("added list ref key to po literal for " + pd.name());
		}
		for (String crk : pd.constRefKeys()) {
			String constName = pd.constRef(crk).name();
			po.addLiteralInit(constName, constName);
			po.addParam(constName, o.getGoType(pd.constRef(crk).type()));
			//logger.trace("added const ref key to po literal for " + pd.name());
		}
		for (String erk : pd.extRefKeys()) {
			String extRefName = pd.extRef(erk).name();
			po.addLiteralInit(extRefName, extRefName);
			po.addParam(extRefName, o.getGoType(pd.extRef(erk).type()));
			//logger.trace("added ext ref key to po literal for " + pd.name());
		}
	}

	private void addProcObjectToNet(ProcessDef pd, ProcessObject po) {
		if (!(pd.indexes().isEmpty())) {
			// TODO: support multi-dim indexing someday
			Map<String, List<String>> idxMap = pd.indexes();
			for (String kStr : idxMap.keySet()) {
				List<String> list = idxMap.get(kStr);
				o.procNet.addReplProcessObject(po, list.get(1), pd.indexingID());
			}
		} else {
			o.procNet.addProcessObject(po);
		}	
	}

	private void gatherProcObjectSubstates(ProcessDef pd, ProcessObject po) {
		for (BaseDef bd : pd.localDefs()) {
			if (bd instanceof ProcessDef) { 
				ProcessDef p = (ProcessDef) bd;
				tmpScope = p.containedScope();
				processSubstate(pd, p, po);
				tmpScope = null;
			}
		}
		if (proxyChanRequired()) {
			po.addProxyChan();	
		}
	}

	private void processSubstate(ProcessDef pObjProc, ProcessDef substateProc,
			ProcessObject po) { 
		for (ParamDef prd : substateProc.params()) {
			processObjectStateVars.put(prd.name(), prd);
			switch (prd.type()) {
			case SET:
				po.addLiteralInit(prd.name(), 
						getTemplate("cspNewIST").render());
				break;
			case LIST:
				po.addLiteralInit(prd.name(),
						getTemplate("cspNewISQ").render());
				break;
			}
		}
		ST substate = po.addSubstateMethod(substateProc.name());
		setProcPfx(pObjProc.name());
		ParserRuleContext ctx = substateProc.ctx();
		if (ctx instanceof ParametricPatternDeclContext) {
			ParametricPatternDeclContext ppdc = 
				(ParametricPatternDeclContext) ctx;
			ctx = ppdc.expression(1);
		} else {
			PatternDeclContext pdc =
				(PatternDeclContext) ctx;
			ctx = pdc.expression();
		}
		setCurrentSubstate(substateProc);
		walker.walk(this, ctx);
		resetCurrentSubstate();
		substate.add("line", getST(ctx));
	}

	private void addSubstateParameters(ProcessObject po) {
		for (String s : processObjectStateVars.keySet()) {
			ParamDef prd = 
				processObjectStateVars.get(s);
			po.addParam(prd.name(), o.getGoType(prd.type()));
		}
	}

	private void addReferencedFunctions(ProcessDef pd, 
			ProcessObject po) {
		scanningFunction();
		for (String fdk : pd.funcRefKeys()) {
			FunctionDef fd = pd.funcRef(fdk);
			setupFunction(fd, po);
		}
		resetScanningFunction();
	}


}
