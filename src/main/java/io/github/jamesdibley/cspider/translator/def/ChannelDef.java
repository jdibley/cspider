/**
 * ChannelDef
 */
package io.github.jamesdibley.cspider.translator.def;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import io.github.jamesdibley.cspider.translator.BaseDef;
import io.github.jamesdibley.cspider.translator.ValueType;
import io.github.jamesdibley.cspider.translator.def.ChannelDirection;
import io.github.jamesdibley.cspider.translator.def.ChannelVisibility;
import io.github.jamesdibley.cspider.translator.def.ChannelType;
import io.github.jamesdibley.cspider.translator.def.MsgStructDef;
import io.github.jamesdibley.cspider.translator.symbol.*;

public class ChannelDef extends BaseDef {
	private ChannelType type;
	private ChannelDirection dir;
	private ChannelVisibility visibility;
	private ChannelDef extChan;
	private List<SetDef> spec;
	private List<SetDef> indexes;
	private String msgStructName;
	private MsgStructDef msgStruct;
	private int dataStartIdx;

	private static final Logger logger = LogManager.getLogger(ChannelDef.class);

	public ChannelDef(ParserRuleContext ctx, String name, ChannelType type) {
		super(ctx, name);
		this.type = type;
		switch (type) {
		case EVENT:
		case INDEXED_EVENT:
			setType(ValueType.EVENT);
			break;
		case DATA:
		case INDEXED_DATA:
		case MULTI_DATA:
		case INDEXED_MULTI_DATA:
			setType(ValueType.DATAEVENT);
			break;
		}
		dir = ChannelDirection.UNKNOWN;
		if (name.matches("(.*)In")) {
			visibility = ChannelVisibility.OBJECT;
		} else if (name.matches("(.*)Out")) {
			visibility = ChannelVisibility.OBJECT;
		} else {
			// This is our default; may be revised if
			// the channel is found in a synchro set
			visibility = ChannelVisibility.CLIENT;
		}
		spec = new ArrayList<SetDef>();
		indexes = new ArrayList<SetDef>();
		extChan = null;
		msgStruct = null;
		msgStructName = name + "Msg";
		dataStartIdx = 0;
	}

	public void setChannelType(ChannelType type) {
		this.type = type;
	}

	public ChannelType chanType() {
		return type;
	}

	public ChannelVisibility visibility() {
		return visibility;
	}

	public void setVisibilityObject() {
		if (visibility == ChannelVisibility.CLIENT) {
			logger.trace("setting " + this.name() + " to OBJECT visibility.");
			visibility = ChannelVisibility.OBJECT;
		} 
	}

	public void setVisibilityNetwork() {
		if (visibility == ChannelVisibility.CLIENT) {
			visibility = ChannelVisibility.NETWORK;
		}
	}

	public void setChannelInput() {
		dir = ChannelDirection.INPUT;
	}

	public void setChannelOutput() {
		dir = ChannelDirection.OUTPUT;
	}

	public ChannelDirection dir() {
		return dir;
	}

	public String msgStructName() {
		switch (visibility) {
		case CLIENT:
			return msgStructName.substring(0,1).toUpperCase()
				+ msgStructName.substring(1);
		default:
			return msgStructName; 
		}
	}

	public ValueType valueType() {
		switch (type) {
		case EVENT:
		case INDEXED_EVENT:
			return ValueType.EVENT; 
		case DATA:
		case INDEXED_DATA:
			SetDef lastSpecField = 
				spec.get(spec.size()-1);
			return lastSpecField.memberType(); 
		case MULTI_DATA:
		case INDEXED_MULTI_DATA:
			return ValueType.UNKNOWN;
		}
		return ValueType.UNKNOWN;
	}

	public List<SetDef> spec() {
		return spec;
	}

	public SetDef getSpecField(int i) {
		if (i < spec.size()) {
			return spec.get(i);
		} 
		return null;
	}

	public void addSpecField(SetDef sDef) {
		spec.add(sDef);
	}

	public List<SetDef> indexes() {
		return indexes;
	}

	public SetDef getIndexField(int i) {
		if (i < indexes.size()) {
			return indexes.get(i);
		}
		return null;
	}

	public int dataStartIdx() {
		return dataStartIdx;
	}

	public boolean dataChannel() {
		switch (type) {
		case EVENT:
		case INDEXED_EVENT:
			return false;
		case DATA:
		case INDEXED_DATA:
		case MULTI_DATA:
		case INDEXED_MULTI_DATA:
			return true;
		}
		return false;
	}


	public void markAsIndex(int fieldNum) {
		if (spec == null || spec.isEmpty()) { return; }
	
		// First we replace the field with an "Index" set
		// (index fields must be of underlying type INT)
		SetDef sd = spec.get(fieldNum);
		if (fieldNum >= indexes.size()) {
			indexes.add(sd);
		}
	
		// Type updating is based on how many fields remain from the 
		// original channel specification which have not yet been 
		// marked as indexes.
		// 	We are assuming - reasonably enough - that indexing 
		// fields always prefix the data fields.
		if (fieldNum < spec.size()) {
			dataStartIdx = fieldNum + 1;
		} else {
			dataStartIdx = -1;
		}
		int numRemainingDataFields = spec.size() - dataStartIdx;
		switch (type) {
		case DATA:
			if (numRemainingDataFields == 0) {
				type = ChannelType.INDEXED_EVENT;
			} else {
				type = ChannelType.INDEXED_DATA;
			}
			break;
		case MULTI_DATA:
			if (numRemainingDataFields == 1) {
				type = ChannelType.INDEXED_DATA;
			} else {
				type = ChannelType.INDEXED_MULTI_DATA;
			}
			break;
		}
	}

	/*
		Symbol indexSetSym = 
			this.tranState().symTable.currentScope().resolve("Index");
		SetDef indexSet = (SetDef) indexSetSym.def();
		spec.add(fieldNum, indexSet);
		*/

	public MsgStructDef msgStruct() {
		return msgStruct;
	}

	public void setMsgStruct(MsgStructDef msgStruct) {
		this.msgStruct = msgStruct;
	}

	public String toString() {
		StringBuilder specFieldNames = new StringBuilder();
		specFieldNames.append("(");
		for (SetDef sd : spec()) {
			specFieldNames.append(sd.name() + " "); 
		}
		specFieldNames.append(")");

		if (msgStruct != null) { 
			return defStateToString() + ": " 
				+ type.toString() + ", " 
				+ visibility.toString() + ", "
				+ dir.toString() + ", " 
				+ specFieldNames + ", "
				+ msgStruct.toString();
		} else {
			return defStateToString() + ": " 
				+ type.toString() + ", " 
				+ visibility.toString() + ", "
				+ dir.toString() + ", " 
				+ specFieldNames ;
		}
	}
}
