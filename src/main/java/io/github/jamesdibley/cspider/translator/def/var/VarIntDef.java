/**
 * VarIntDef
 */
package io.github.jamesdibley.cspider.translator.def.var;

import io.github.jamesdibley.cspider.translator.ValueType;
import io.github.jamesdibley.cspider.translator.def.VarBaseDef;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;

public class VarIntDef extends VarBaseDef {

	public VarIntDef(ParserRuleContext ctx, String name) {
		super(ctx, ValueType.INT, name);
		this.markComplete();
	}
}
