/**
 * Pass12
 */
package io.github.jamesdibley.cspider.translator.pass;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
import org.antlr.v4.runtime.tree.xpath.XPath;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import io.github.jamesdibley.cspider.parser.gen.*;
import io.github.jamesdibley.cspider.parser.gen.CSPMParser.*;
import io.github.jamesdibley.cspider.translator.BasePass;
import io.github.jamesdibley.cspider.translator.TranState;
import io.github.jamesdibley.cspider.translator.ValueType;
import io.github.jamesdibley.cspider.translator.def.*;
import io.github.jamesdibley.cspider.translator.def.var.*;

public class Pass12 extends BasePass {
	private static final Logger logger = LogManager.getLogger(Pass12.class);
	private String xpath = "/sourcefile/declaration/chanDeclWithSpec";

	public Pass12(TranState tState, ParseTreeWalker walker,
			CSPMParser parser) {
		super(tState, walker, parser);
		logger.trace("generate message structs for complex channels");
	}

	public void process(SourcefileContext tree) {
		for (ParseTree t : XPath.findAll((ParseTree) tree, xpath, parser)) {
			if (t instanceof ChanDeclWithSpecContext) {
				ChanDeclWithSpecContext cdwsc =
					(ChanDeclWithSpecContext) t;
				for (TerminalNode n : cdwsc.Name()) {
					ExpressionContext ec = 
						cdwsc.expression();
					ChannelDef cd = (ChannelDef) def(n);
					switch(cd.chanType()) {
					case MULTI_DATA:
						//logger.trace("Found a multi data chan: " 
						//		+ cd.name());
						registerMsgStruct(cd, ec);
						break;
					case INDEXED_MULTI_DATA:
						//logger.trace("Found an indexed multi data: "
						//		+ cd.name());
						registerMsgStruct(cd, ec);
						break;
					case EVENT:
					case INDEXED_EVENT:
					case DATA:
					case INDEXED_DATA:
						break;
					}
				}
			}
		}
	}

	private void registerMsgStruct(ChannelDef cd, 
			ExpressionContext ec) {
		if (cd.msgStruct() != null) { return; } // already registered

		MsgStructDef msd = 
			new MsgStructDef(ec, cd.msgStructName());

		
		for (int i = cd.indexes().size(); i < cd.spec().size(); i++) {
			
			SetDef s = cd.spec().get(i);
			
			switch(s.memberType()) {
			case INT:
				VarIntDef vid =
					new VarIntDef(null, "f0" 
						+ (i - cd.indexes().size()));
				msd.addField(vid);
				break;
			case BOOL:
				VarBoolDef vbd = 
					new VarBoolDef(null, "f0" 
						+ (i - cd.indexes().size()));
				msd.addField(vbd);
				break;
			case CHAR:
				VarCharDef vcd = 
					new VarCharDef(null, "f0" 
						+ (i - cd.indexes().size()));
				msd.addField(vcd);
				break;		
			}
		}

		cd.setMsgStruct(msd);
		msd.setTranState(tState);
		setDef(ec, msd);
		tState.symTable.currentScope().define(msd, cd.msgStructName());
		//logger.trace("Set msg struct " + msd.name() 
		//		+ " on channel named: " + cd.name()
		//		+ " with fields: " 
		//	 ); 
		for (VarBaseDef v : msd.fields()) {
		//	logger.trace(v.name() + ": " + v.type());
		}
	}
}
