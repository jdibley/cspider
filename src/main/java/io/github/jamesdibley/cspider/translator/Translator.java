/**
 * Translator 
 */

package io.github.jamesdibley.cspider.translator;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList; 

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
import org.antlr.v4.runtime.tree.xpath.XPath;
import org.stringtemplate.v4.*;

import io.github.jamesdibley.cspider.FileProcessor;
import io.github.jamesdibley.cspider.parser.gen.CSPMLexer;
import io.github.jamesdibley.cspider.parser.gen.CSPMParser;
import io.github.jamesdibley.cspider.parser.gen.CSPMParser.SourcefileContext;
import io.github.jamesdibley.cspider.translator.pass.*;
import io.github.jamesdibley.cspider.translator.gen.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Translator {
	private static final Logger logger = 
		LogManager.getLogger(Translator.class);	
	protected FileProcessor processor;
	private TranState tState; 
	private ParseTreeWalker walker;

	public Translator(FileProcessor processor) {
		logger.trace("Translator starting...");
		this.processor = processor;
		tState = new TranState();
		walker = new ParseTreeWalker();
	}
	
	public void translate(String sourceData) {
		try {
			CSPMParser parser = parse(sourceData, tState);
			SourcefileContext tree = parser.sourcefile();
			// Check for unsupported syntax
			boolean valid = validateInput(tree, parser);
			if (!valid) {
				logger.trace("Abort: Unsupported syntax.");
				return; 
			}
			// Gather definitions
			boolean proceed = gatherDefs(tree, parser);
			if (!proceed) { return; }
			// Debug symbol table
			//logger.trace(tState.symTable.toString());
			// Transform model and generate output
			generateOutput(tree, parser);
		} catch (IOException e) { // from parse(...)
			logger.error(this, e);
		}
	}

	private CSPMParser parse(String sourceData, TranState tState) throws IOException {
		CharStream input = CharStreams.fromString(sourceData);
		CSPMLexer lexer = new CSPMLexer(input);
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		tState.tokens = tokens;
		CSPMParser parser = new CSPMParser(tokens);
		parser.setBuildParseTree(true);
		return parser;
	}

	private boolean validateInput(SourcefileContext tree,
			CSPMParser parser) {	
		Pass00 pass00 = new Pass00(tState, walker, parser);
		pass00.process(tree);
		if (tState.aborted()) {
			return false; 
		}
		return true;
	}

	private boolean gatherDefs(SourcefileContext tree, 
			CSPMParser parser) {
		tState.nodeContextMap = new ParseTreeProperty<BaseDef>();
		Pass01 pass01 = new Pass01(tState, walker, parser);
		pass01.process(tree);
		if (tState.aborted()) return false; 
		Pass02 pass02 = new Pass02(tState, walker, parser);
		pass02.process(tree);
		if (tState.aborted()) return false; 
		Pass03 pass03 = new Pass03(tState, walker, parser);
		pass03.process(tree);
		if (tState.aborted()) return false; 
		Pass04 pass04 = new Pass04(tState, walker, parser);
		pass04.process(tree);
		if (tState.aborted()) return false; 
		Pass05 pass05 = new Pass05(tState, walker, parser);
		pass05.process(tree);
		if (tState.aborted()) return false;
		Pass06 pass06 = new Pass06(tState, walker, parser);
		pass06.process(tree);
		if (tState.aborted()) return false;
	
		Pass07 pass07 = new Pass07(tState, walker, parser);
		pass07.process(tree);
		if (tState.aborted()) return false;
		
		Pass08 pass08 = new Pass08(tState, walker, parser);
		pass08.process(tree);
		if (tState.aborted()) return false;
		Pass09 pass09 = new Pass09(tState, walker, parser);
		pass09.process(tree);
		if (tState.aborted()) return false;
		Pass10 pass10 = new Pass10(tState, walker, parser);
		pass10.process(tree);
		if (tState.aborted()) return false;
		Pass11 pass11 = new Pass11(tState, walker, parser);
		pass11.process(tree);
		if (tState.aborted()) return false;
		Pass12 pass12 = new Pass12(tState, walker, parser);
		pass12.process(tree);
		if (tState.aborted()) return false;
		Pass12a pass12a = new Pass12a(tState, walker, parser);
		pass12a.process(tree);
		if (tState.aborted()) return false;
		Pass14 pass14 = new Pass14(tState, walker, parser);
		pass14.process(tree);
		if (tState.aborted()) return false;
		Pass15 pass15 = new Pass15(tState, walker, parser);
		pass15.process(tree);
		if (tState.aborted()) return false;

		return true;
	}

	private void generateOutput(SourcefileContext tree, 
			CSPMParser parser) {
		tState.setInputName(processor.sourceFilename());
		tState.output.registerNames(tState.inputName());
		// gather definitions
		Gen00 gen00 = new Gen00(tState, walker, parser);
		gen00.process(tree);
		// set up constants on process network
		Gen01 gen01 = new Gen01(tState, walker, parser);
		gen01.process(tree);
		//// build functions 
		//Gen02 gen02 = new Gen02(tState, walker, parser);
		//gen02.process(tree);
		// build process objects	
		Gen02 gen02 = new Gen02(tState, walker, parser);
		gen02.process(tree);

		// render output
		GenRender genR = new GenRender(tState, walker, parser, processor);
		genR.process(tree);
//		logger.trace(tState.output.procNet);
	//	logger.trace(tState.output.procNet.render());
	}
}
