/**
 * GenRender
 */
package io.github.jamesdibley.cspider.translator.gen;

import java.io.File;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.stringtemplate.v4.*;
import io.github.jamesdibley.cspider.FileProcessor;
import io.github.jamesdibley.cspider.parser.gen.*;
import io.github.jamesdibley.cspider.parser.gen.CSPMParser.*;
import io.github.jamesdibley.cspider.translator.BaseGen;
import io.github.jamesdibley.cspider.translator.TranState;
import io.github.jamesdibley.cspider.translator.symbol.*;
import io.github.jamesdibley.cspider.translator.go.*;

public class GenRender extends BaseGen {
	private static final Logger logger = LogManager.getLogger(GenRender.class);
	private OutputModel out;
	private FileProcessor processor;
	private SymbolTable symTable;
	private String transDirPath;
	private int wrapCol = 78;

	public GenRender(TranState tState, ParseTreeWalker walker,
			CSPMParser parser, FileProcessor processor) {
		super(tState, walker, parser);
		logger.trace("Render process network and objects to Go source files.");
		out = tState.output;
		symTable = tState.symTable;
		this.processor = processor;
		transDirPath = processor.targetDirectory();
	}

	public void process(SourcefileContext tree) {
		renderProcessNetwork();
		for (String key : out.procObjects.keySet()) {
			renderProcessObject(key);
		}
	}

	private void renderProcessNetwork() {
		String inputFilename = 
			processor.sourceFilename().toLowerCase();
		int bIdx = inputFilename.lastIndexOf("/");
		int eIdx = inputFilename.lastIndexOf("impl.");

		String filePath =
			transDirPath
			+ inputFilename.substring(bIdx+1, eIdx)
			+ out.getSrcFileExt();

		logger.trace("rendering process network to: \n" + filePath);
		File procNetworkFile = new File(filePath);
		processor.storeData(procNetworkFile,
				out.procNet.pn.render(wrapCol));
	}

	private void renderProcessObject(String objKey) {
		ProcessObject pObj = 
			out.procObjects.get(objKey);
		String filePath = 
			transDirPath 
			+ pObj.name.toLowerCase()
			+ out.getSrcFileExt();
		File procObjFile = new File(filePath);

		logger.trace("rendering process object "
				+ pObj.name 
				+ " to: \n" + filePath);
		processor.storeData(procObjFile, pObj.st.render(wrapCol));
	}
}
