/**
 * ProcessObject
 */
package io.github.jamesdibley.cspider.translator.go;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.stringtemplate.v4.*;

import io.github.jamesdibley.cspider.parser.gen.*;
import io.github.jamesdibley.cspider.parser.gen.CSPMParser.*;
import io.github.jamesdibley.cspider.translator.def.*;

public class ProcessObject {
	private static final Logger logger =
		LogManager.getLogger(ProcessObject.class);
	public String name; 
	public OutputModel o;
	public ProcessNetwork procNet;
	public ProcessDef pd;
	public ProcInvocationDef pid;
	public ST literal;
	public ST st;

	public ProcessObject(ProcessDef pd, ProcessNetwork pn, 
			ProcInvocationDef pid) {
		name = pd.name().toLowerCase();
		this.pd = pd;
		this.pid = pid;
		procNet = pn;
		o = pn.om;
		literal = o.templates.getInstanceOf("procObjectLiteral");
		literal.add("n", name);
		st = o.templates.getInstanceOf("processObject");
		st.add("pkn", pn.pkn);
		st.add("procPfx", name.toUpperCase());
		st.add("n", name);
		st.add("nI", getPOInitial());
		//setupParams(pd, pid);
		setupChannels(pd, pid);
	}

	public String getPOInitial() {
		return name.substring(0,1);
	}

	public void addLiteralInit(String name, String value) {
		literal.add("varName", name);
		literal.add("varInit", value);
	}

	public void addParam(String name, ST type) {
		st.add("stateVarName", name);
		st.add("stateVarType", type);
	}

	private void setupChannels(ProcessDef pd, ProcInvocationDef pid) {
		for (String s : pd.chanRefKeys()) {
			ChannelDef cd = (ChannelDef) pd.chanRef(s);
			st.add("chanName", s);
			genChannelType(cd);
			literal.add("poChanName", s);
			genChannelAssignment(s, pd, pid);
		}
		for (String s : pd.indexedChanRefKeys()) {
			ChannelDef cd = (ChannelDef) pd.indexedChanRef(s);
			st.add("chanArrayName", s);
			genChannelType(cd);
			literal.add("poChanName", s);
			genChannelAssignment(s, pd, pid);
		}
		for (String s : pd.LDEchanRefKeys()) {
			ChannelDef cd = (ChannelDef) pd.LDEchanRef(s);
			st.add("chanName", s);
			genChannelType(cd);
			literal.add("poChanName", s);
			genChannelAssignment(s, pd, pid);
		}
		for (String s : pd.LDEindexedChanRefKeys()) {
			ChannelDef cd = (ChannelDef) pd.LDEindexedChanRef(s);
			st.add("chanArrayName", s);
			genChannelType(cd);
			literal.add("poChanName", s);
			genChannelAssignment(s, pd, pid);
		}
	}

	private void genChannelAssignment(String str,
			ProcessDef pd, 
			ProcInvocationDef pid) {
		String assChName = null;
		Set<String> chanRenameTargets = 
			pd.chanRenameKeys();
		Map<String, String> renamings = 
			pid.renamings();
		//logger.trace("genChannelAssignment for: " 
		//		+ pd.name() + "\nchanRenameTargets: "
		//		+ chanRenameTargets 
		//		+ "\nrenamings: " + renamings;
		if (chanRenameTargets.contains(str)) {
			ChannelDef assCh = o.chanDefMap.get(str);
			String targetChanName = 
				pd.chanRenaming(str);
			List<String> targetChanExprs =
				new ArrayList<String>
				(Arrays.asList(targetChanName.split("\\.")));
			assChName = targetChanExprs.remove(0);
			if (procNet.clientChanIDs.get(assChName) != null) {
				assChName = 
					procNet.clientChanIDs.get(assChName);
			}
			for (String s : targetChanExprs) {
				assChName = assChName + "[" + s + "]";
			}
			literal.add("pnChanName", assChName);
		} else if (renamings.containsKey(str)) {
			ChannelDef assCh = o.chanDefMap.get(str);
			switch (assCh.visibility()) {
			case CLIENT: 
				assChName = 
					pd.chanRenaming(str).substring(0,1).toUpperCase()
					+ pd.chanRenaming(str).substring(1);
				literal.add("pnChanName", assChName);
				break;
			default: 
				literal.add("pnChanName", 
						pd.chanRenaming(str));
				break;
			}
			literal.add("pnChanName",
					renamings.get(str));
		} else {
			ChannelDef assCh = o.chanDefMap.get(str);
			switch (assCh.visibility()) {
			case CLIENT:
				assChName = str.substring(0,1).toUpperCase()
					+ str.substring(1);
				literal.add("pnChanName", assChName);
				break;
			default:
				literal.add("pnChanName", str);
			}
		}
	}

	private void genChannelType(ChannelDef cd) {
		SetDef sd = null;
		switch (cd.chanType()) {
		case EVENT:
			st.add("chanType", 
				o.getTemplate("chan").add("t",
					o.getTemplate("emptyStruct")));
			break;
		case DATA:
			sd = cd.getSpecField(0);
			st.add("chanType", 
				o.getTemplate("chan").add("t",
					o.getGoType(sd.memberType())));
			break;
		case MULTI_DATA:
			st.add("chanType", 
				o.getTemplate("chan").add("t",
					cd.msgStructName()));
			break;
		case INDEXED_EVENT:
			st.add("chanArrayType", 
				o.getTemplate("slice").add("t",
					o.getTemplate("chan").add("t", 
						o.getTemplate("emptyStruct"))));
			break;
		case INDEXED_DATA:
			sd = cd.getSpecField(0);
			st.add("chanArrayType", 
				o.getTemplate("slice").add("t",
					o.getTemplate("chan").add("t",
						o.getGoType(sd.memberType()))));
			break;
		case INDEXED_MULTI_DATA:
			st.add("chanArrayType", 
				o.getTemplate("slice").add("t",
					o.getTemplate("chan").add("t",
						cd.msgStructName())));
			break;
		}
	}

	public ST addSubstateMethod(String substateName) {
		ST substateST = o.getTemplate("func");
		substateST.add("rxAbbr", getPOInitial());
		substateST.add("rxName", name);
		substateST.add("n", substateName.toLowerCase());
		substateST.add("t", o.getTemplate("int"));
		st.add("procLabel", substateName);
		st.add("methodName", substateName.toLowerCase());
		st.add("procStMethods", substateST);
		return substateST;
	}

	public void addProxyChan() {
		st.add("proxyChan", "");
	}
}
