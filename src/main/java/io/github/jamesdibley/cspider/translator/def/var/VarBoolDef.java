/**
 * VarBoolDef
 */
package io.github.jamesdibley.cspider.translator.def.var;

import io.github.jamesdibley.cspider.translator.ValueType;
import io.github.jamesdibley.cspider.translator.def.VarBaseDef;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;

public class VarBoolDef extends VarBaseDef {

	public VarBoolDef(ParserRuleContext ctx, String name) {
		super(ctx, ValueType.BOOL, name);
		this.markComplete();
	}
}
