/**
 * ExtBoolDef
 */
package io.github.jamesdibley.cspider.translator.def.ext;

import io.github.jamesdibley.cspider.translator.ValueType;
import io.github.jamesdibley.cspider.translator.def.ExtBaseDef;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;

public class ExtBoolDef extends ExtBaseDef {
	public ExtBoolDef(ParserRuleContext ctx, String name) {
		super(ctx, ValueType.BOOL, name);
		this.markComplete();
	}
}
