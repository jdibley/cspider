/**
 * ChanInputDef
 */
package io.github.jamesdibley.cspider.translator.def;

import java.util.ArrayList;
import java.util.List;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
import io.github.jamesdibley.cspider.parser.gen.*;
import io.github.jamesdibley.cspider.parser.gen.CSPMParser.*;
import io.github.jamesdibley.cspider.translator.BaseDef;

public class ChanInputDef extends BaseDef {
	private ChannelDef src;
	private boolean requiresMsgStruct;
	private String valueType;
	private List<String> indexes;
	private List<String> identifiers;
	private List<ExpressionContext> indexExprs;
	private List<ExpressionContext> identifierExprs;
	private SetDef acceptanceSet;

	public ChanInputDef(ParserRuleContext ctx, String name, 
			ChannelDef src) {
		super(ctx, name); 
		this.src = src;
		indexes = new ArrayList<String>();
		identifiers = new ArrayList<String>();
		indexExprs = new ArrayList<ExpressionContext>();
		identifierExprs = new ArrayList<ExpressionContext>();

		switch(src.chanType()) {
		case EVENT:
		case INDEXED_EVENT:
		case DATA:
		case INDEXED_DATA:
			requiresMsgStruct = false;
			break;
		case MULTI_DATA:
		case INDEXED_MULTI_DATA:
			requiresMsgStruct = true;
			break;
		}

		switch(src.valueType()) {
		case INT: 
			valueType = "Int";
			break;
		case BOOL:
			valueType = "Bool";
			break;
		case CHAR: 
			valueType = "Char";
			break;
		case EVENT:
			valueType = "Event";
			break;
		case UNKNOWN:
			valueType = "Unknown"; 
			break;
		}

	}

	public ChannelDef src() {
		return src;
	}

	public String valueType() {
		return valueType;
	}

	public void addIndex(String idx) {
		indexes.add(idx);
	}

	public void addIdentifier(String id) {
		identifiers.add(id);
	}

	public void addIndexExpr(ExpressionContext ctx) {
		indexExprs.add(ctx);
	}

	public void addIdentifierExpr(ExpressionContext ctx) {
		identifierExprs.add(ctx);
	}

	public boolean requiresMsgStruct() {
		return requiresMsgStruct;
	}

	public void setAcceptanceSet(SetDef accept) {
		acceptanceSet = accept;
	}


	public List<String> indexes() {
		return indexes;
	}

	public String index(int i) {
		if (i < indexes.size()) {
			return indexes.get(i);
		}
		return null;
	}

	public List<String> identifiers() {
		return identifiers;
	}

	public String identifier(int i) {
		if (i < identifiers.size()) {
			return identifiers.get(i);
		}
		return null;
	}

	public List<ExpressionContext> indexExprs() {
		return indexExprs;
	}

	public List<ExpressionContext> identifierExprs() {
		return identifierExprs;
	}

	public String toString() {
		return name() + defStateToString() + "->"
			+ requiresMsgStruct 
			+ valueType  
			+ indexes 
			+ identifiers;
	}
}
