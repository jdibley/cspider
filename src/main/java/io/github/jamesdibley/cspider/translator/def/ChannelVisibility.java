/**
 * ChannelVisibility
 */
package io.github.jamesdibley.cspider.translator.def;

public enum ChannelVisibility {
	CLIENT,	
	NETWORK,
	OBJECT	
}
