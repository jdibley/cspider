/**
 * TranState
 */

package io.github.jamesdibley.cspider.translator;

import java.util.Stack; 

import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeProperty;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.stringtemplate.v4.*;

import io.github.jamesdibley.cspider.FileProcessor;
import io.github.jamesdibley.cspider.translator.go.OutputModel;
import io.github.jamesdibley.cspider.translator.symbol.*;


public class TranState {
	private static final Logger logger = LogManager.getLogger(TranState.class);
	
	public CommonTokenStream tokens;
	public SymbolTable symTable;
	public ParseTreeProperty<BaseDef> nodeContextMap;
	public OutputModel output;
	public String inputName;
	public boolean abort = false;


	public TranState() {
		symTable = new SymbolTable();
		output = new OutputModel();
	}

	public TranState(TranState state) {
		symTable = state.symTable.clone();
		output = state.output;
	}

	public String debugSymTable() {
		return symTable.toString();
	}

	public boolean isInMap(ParseTree t) {
		return (nodeContextMap.get(t) != null);
	}

	public void abortTranslation() {
		abort = true;
	}

	public boolean aborted() {
		return (this.abort == true);
	}

	public String inputName() {
		return inputName;
	}

	public void setInputName(String src) {
		inputName = stripSuffix(src);
	}

	private String stripSuffix(String str) {
		int b = str.lastIndexOf("/");
		int e = str.lastIndexOf("Impl.csp");
		return str.substring(b+1,e);
	}
}
