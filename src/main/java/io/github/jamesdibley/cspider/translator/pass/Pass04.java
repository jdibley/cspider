/**
 * Pass04
 * Gathering set declarations 
 */
package io.github.jamesdibley.cspider.translator.pass;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
import org.antlr.v4.runtime.tree.xpath.XPath;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import io.github.jamesdibley.cspider.parser.gen.*;
import io.github.jamesdibley.cspider.parser.gen.CSPMParser.*;
import io.github.jamesdibley.cspider.translator.BasePass;
import io.github.jamesdibley.cspider.translator.TranState;
import io.github.jamesdibley.cspider.translator.ValueType;
import io.github.jamesdibley.cspider.translator.def.*;
import io.github.jamesdibley.cspider.translator.symbol.*;

public class Pass04 extends BasePass {
	private static final Logger logger = LogManager.getLogger(Pass04.class);
	private String rootXPath = "/sourcefile/declaration";
	private String letXPath = "//letClause";
	private String setExprXPath = 
		"/patternDecl/expression/primaryExpr/setExpr";
	private String chanSpecXPath = 
		"/sourcefile/declaration/chanDeclWithSpec//setExpr";
	private ValueType tmpVT;
	private boolean excludable;
	private int tmpRangeVal;
	// for labeling and defining anon sets in chan specs
	private int anonSetCtr; 
	// collect references to external definitions 
	private Map<String, ExtBaseDef> tmpExtRefs;

	public Pass04(TranState tState, ParseTreeWalker walker, CSPMParser parser) {
		super(tState, walker, parser);
		tmpVT = ValueType.UNKNOWN;
		excludable = false;
		tmpExtRefs = new LinkedHashMap<String, ExtBaseDef>();
		anonSetCtr = 0;
		logger.trace("Gather all set expressions, including anons.");
	}

	public void process(SourcefileContext tree) {
		processTopLevelSetDecls(tree, rootXPath+setExprXPath);
		processLetClauseSetDecls(tree, letXPath+setExprXPath);
		processChanSpecSetDecls(tree);
	}

	private int nextAnonSetCtr() {
		return anonSetCtr++;
	}

	private void recordExtRefs(SetDef sd) {
		for (ExtBaseDef ebd : tmpExtRefs.values()) {
			ebd.addReferencingSet(sd);
			sd.addExtRef(ebd);
		}
		tmpExtRefs.clear();
	}

	private void processTopLevelSetDecls(ParseTree tree, String xpath) {	
		for (ParseTree t : XPath.findAll((ParseTree) tree, 
					xpath, parser)) {
			// get name
			PatternDeclContext pdc = 
				(PatternDeclContext) 
				t.getParent().getParent().getParent();
			NamePatternContext np = pdc.patternLHS().namePattern();
			ExpressionContext ec = pdc.expression();
			if (np == null || !(np instanceof NpNameContext)) {
				logger.trace("Not a simple decl, skipping: "
						+ np.getText());
				continue;
			}
			// Set literals commonly appear in parallel composition
			// We need to avoid processing these by accident
			if (ec instanceof ExprAlphaParallelContext
				|| ec instanceof ExprInterfaceParallelContext
				|| ec instanceof ExprReplAlphaParallelContext
				|| ec instanceof ExprReplInterfaceParallelContext
				|| ec instanceof ExprInterleaveContext
				|| ec instanceof ExprReplInterleaveContext
				|| ec instanceof ExprRenameContext 
				|| ec instanceof ExprHideContext ) {
				continue;
			}
			String name = ((NpNameContext)np).Name().getText();
			if (t instanceof SetContext) {
				registerSet(pdc, name, (SetContext)t, true);
			} else if (t instanceof SetRangedIntContext) {
				registerSetRangedInt(pdc, name, 
						(SetRangedIntContext)t, true);
			} else if (t instanceof SetEnumeratedContext) {
				registerSetEnumerated(pdc, name,
						(SetEnumeratedContext)t, true);
			} else if (t instanceof SetEnumeratedCompContext) {
				registerSetEnumeratedComp(pdc, name,
						(SetEnumeratedCompContext)t, true);
			} else if (t instanceof SetUnionContext) {
				registerSetUnion(pdc, name, (SetUnionContext)t, true);
			}
		}
	}

	private void processLetClauseSetDecls(ParseTree tree, String xpath) {	
		for (ParseTree t : XPath.findAll((ParseTree) tree, 
					xpath, parser)) {
			// We check the name format BUT we don't want to 
			// write these declarations to the symtab - we do that
			// in Pass07 when we're in the right scope.
			PatternDeclContext pdc = 
				(PatternDeclContext) 
				t.getParent().getParent().getParent();
			NamePatternContext np = pdc.patternLHS().namePattern();
			ExpressionContext ec = pdc.expression();
		
			if (np == null || !(np instanceof NpNameContext)) {
				logger.trace("Not a simple decl, skipping: "
						+ np.getText());
				continue;
			}

			if (ec instanceof ExprAlphaParallelContext
				|| ec instanceof ExprInterfaceParallelContext
				|| ec instanceof ExprReplAlphaParallelContext
				|| ec instanceof ExprReplInterfaceParallelContext
				|| ec instanceof ExprInterleaveContext
				|| ec instanceof ExprReplInterleaveContext) {
				continue;
			}

			String name = ((NpNameContext)np).Name().getText();
			if (t instanceof SetContext) {
				registerSet(pdc, name, (SetContext)t, false);
			} else if (t instanceof SetRangedIntContext) {
				registerSetRangedInt(pdc, name, 
						(SetRangedIntContext)t, false);
			} 
		}
	}

	private void processChanSpecSetDecls(ParseTree tree) {
		for (ParseTree t : XPath.findAll((ParseTree) tree,
					chanSpecXPath, parser)) {
			//logger.trace("CSSD processing: " + t.getText());
			String anonName = 
				"anonSet" + Integer.toString(nextAnonSetCtr());
			if (t instanceof SetContext) {
				SetContext sc = (SetContext) t;
				// get members
				List<String> members = new ArrayList<String>();
				ValueType memberVT = ValueType.UNKNOWN;
				ExpressionListContext elc = sc.expressionList();
				for (ExpressionContext e : elc.expression()) {
					String member = e.getText();
					members.add(member);
				}
	
				detectType(elc.expression(0));

				SetDef sDef = 
					new SetDef(sc, members, tmpVT, anonName,
							tState.symTable.currentScope());
				sDef.setTranState(tState);
				sDef.markComplete();
				setDef(sc, sDef);
				recordExtRefs(sDef);
				tState.symTable.currentScope().define(sDef, anonName);
				
			} else if (t instanceof SetRangedIntContext) {
				SetRangedIntContext sric = 
					(SetRangedIntContext) t;
				String begin = sric.expression(0).getText();
				String end = sric.expression(1).getText();
				
				SetDef sDef = 
					new SetDef(sric, begin, end, anonName,
							tState.symTable.currentScope());
				sDef.setTranState(tState);
				sDef.markComplete();
				setDef(sric, sDef);
				recordExtRefs(sDef);
				tState.symTable.currentScope().define(sDef, anonName);
			}
		}
	}


	private boolean belongsToSyncSet(String member) {
		Symbol tmp = tState.symTable.currentScope().resolve(member);
		if ((tmp != null) && (tmp.def() instanceof ChannelDef)) {
			logger.trace("Channel name found in set," +
					" marking set as excludable.");
			return true;
		}
		return false;
	}

	private void detectType(ExpressionContext ctx) {
		// tmpVT gets set during the walk 
		// we use its value on returning
		walker.walk(this, (ParseTree) ctx);
	}

	public void registerSet(PatternDeclContext pdc, String name, 
			SetContext ctx, boolean topLevelDecl) {
		List<String> members = new ArrayList<String>();
		ExpressionListContext elc = ctx.expressionList();
		if (name.matches("a[A-Z](.*)")) {
			
		}
		for (ExpressionContext e : elc.expression()) {
			String member = e.getText();
			excludable = belongsToSyncSet(member);
			members.add(member);
		}
	
		detectType(elc.expression(0));

		if (name.matches("a(.*)")) {
			tmpVT = ValueType.EVENT;
		}

		SetDef sDef = new SetDef(pdc, members, tmpVT, name,
				tState.symTable.currentScope());
		sDef.setTranState(tState);
		setDef(pdc, sDef);
		recordExtRefs(sDef);
		sDef.markComplete();
		if (excludable) {
			sDef.markExcluded();
		}
		//logger.trace("defined set named: " + name);
		if (topLevelDecl) {
			tState.symTable.currentScope().define(sDef, name);
		}
		// reset per-set flags 
		this.tmpVT = ValueType.UNKNOWN;
		this.excludable = false;
	}

	public void registerSetRangedInt(PatternDeclContext pdc, String name,
			SetRangedIntContext ctx, boolean topLevelDecl) {
		
		String begin = ctx.expression(0).getText();
		String end = ctx.expression(1).getText();
		
		// sanity check: exprs must be made up of numbers or symbols
		walker.walk(this, (ParseTree) ctx.expression(0));
		walker.walk(this, (ParseTree) ctx.expression(1));

		SetDef sDef = 
			new SetDef(pdc, begin, end, name, 
					tState.symTable.currentScope());
		sDef.setTranState(tState);
		setDef(pdc, sDef);
		recordExtRefs(sDef);
		sDef.markComplete();
		//logger.trace("defined ranged set named: " + name);	
		if (topLevelDecl) {
			tState.symTable.currentScope().define(sDef, name);
		}
	}

	public void registerSetEnumerated(PatternDeclContext pdc, String name,
			SetEnumeratedContext ctx, boolean topLevelDecl) {
		logger.trace(">>> " + name);
		List<String> members = new ArrayList<String>();
		ExpressionListContext elc = ctx.expressionList();
		for (ExpressionContext e : elc.expression()) {
			String member = e.getText();
			belongsToSyncSet(member);
			members.add(member);
		}

		detectType(elc.expression(0));

		if (name.matches("a(.*)")) {
			tmpVT = ValueType.EVENT;
		}

		SetDef sDef = 
			new SetDef(pdc, members, tmpVT, name, tState.symTable.currentScope());
		sDef.setTranState(tState);
		setDef(pdc, sDef);
		recordExtRefs(sDef);
		sDef.markComplete();
		if (excludable) {
			sDef.markExcluded();
		}

		if (topLevelDecl) {
			tState.symTable.currentScope().define(sDef, name);
		}
		this.tmpVT = ValueType.UNKNOWN;
		this.excludable = false;
	}

	public void registerSetEnumeratedComp(PatternDeclContext pdc, String name,
			SetEnumeratedCompContext ctx, boolean topLevelDecl) {
		logger.error("Enumerated set comprehensions not implemented yet!");
	}

	public void registerSetUnion(PatternDeclContext pdc, String name,
			SetUnionContext ctx, boolean topLevelDecl) {
		if (name.matches("a(.*)")) {

			SetDef sDef = new SetDef(pdc, new ArrayList<String>(), ValueType.EVENT, 
					name, tState.symTable.currentScope());
			sDef.setTranState(tState);
			setDef(pdc, sDef);
			sDef.markComplete();
		} else {
			logger.error("Set definitions over union not implemented yet.");
		}
	}

	@Override
	public void exitLitInt(LitIntContext ctx) {
		tmpVT = ValueType.INT;
	}

	@Override
	public void exitLitChar(LitCharContext ctx) {
		tmpVT = ValueType.CHAR;
	}

	@Override
	public void exitLitString(LitStringContext ctx) {
		tmpVT = ValueType.STRING;
	}

	// 2018-01-06
	// The purpose of this override is to figure out whether a set 
	// declaration contains a sync set or not in order to exclude the 
	// set declaration from the final translation. If an identifier 
	// (not a literal) appears in the set decl, we try to look it up in 
	// the symbol table to find out whether it's an -event- identifier.
	// 	Before we supported LET...WITHIN ('local definition 
	// environments'), an unresolvable identifier in a set decl was a 
	// showstopper, indicating that the translator had somehow missed an
	// identifier.
	// 	As a result of supporting LET...WITHIN, we might occasionally
	// encounter a set declaration (inside an LDE) that references an 
	// identifier that isn't in our symbol table (because identifiers in 
	// LDEs don't get written to the symtab until the owner process/func
	// is itself in the symtab and has its own scope). 
	// 	Overlooking this situation is -safe- for the purposes of -this 
	// override- because an LDE set can -never- be used to define a 
	// synchronisation set.
	@Override
	public void exitPExprName(PExprNameContext ctx) {
		String name = ctx.Name().getText();
		Symbol tmp = tState.symTable.currentScope().resolve(name);
		if (tmp != null) {
			if (tmp.def() instanceof ChannelDef) {
				this.excludable = true;
				tmpVT = ValueType.EVENT;
			}
			if (tmp.def() instanceof ExtBaseDef) {
				ExtBaseDef ebd = (ExtBaseDef) tmp.def();
				tmpExtRefs.put(name, ebd);	
			}
		} else {
			logger.trace("Undefined id found in set: " + name);
		}
	}

}
