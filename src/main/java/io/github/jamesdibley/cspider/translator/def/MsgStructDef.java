/**
 * MsgStructDef
 */
package io.github.jamesdibley.cspider.translator.def;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.stringtemplate.v4.*;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
import io.github.jamesdibley.cspider.parser.gen.CSPMParser.*;
import io.github.jamesdibley.cspider.translator.BaseDef;
import io.github.jamesdibley.cspider.translator.ValueType;

public class MsgStructDef extends BaseDef {
	private List<VarBaseDef> fields;
	private ST structDecl;

	public MsgStructDef(ParserRuleContext ctx, String name) {
		super(ctx, name);
		structDecl = null;
		fields = new ArrayList<VarBaseDef>();
	}

	public int numFields() {
		return fields.size();
	}

	public List<VarBaseDef> fields() {
		return fields;
	}

	public void addField(VarBaseDef vbd) {
		fields.add(vbd);
	}

	public VarBaseDef field(int i) {
		return fields.get(i);
	}

	public ST structDecl() {
		return structDecl;
	}

	public void setStructDecl(ST st) {
		structDecl = st;
	}

	public String toString() {
		return fields.toString(); 
		//return name();
	}
}
