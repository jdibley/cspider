/**
 * VarListDef
 */
package io.github.jamesdibley.cspider.translator.def.var;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
import io.github.jamesdibley.cspider.translator.ValueType;
import io.github.jamesdibley.cspider.translator.def.VarBaseDef;

public class VarListDef extends VarBaseDef {
	public ValueType elemType;

	public VarListDef(ParserRuleContext ctx, String name) {
		super(ctx, ValueType.LIST, name);
	}

	public void setElemType(ValueType elemType) {
		this.elemType = elemType;
		this.markComplete();
	}

	public String toString() {
		String elem = "";
		switch(elemType) {
		case INT:
			elem = "[]int";
		case BOOL:
			elem = "[]bool";
		case CHAR:
			elem = "string";
		}
		return elem;
	}
}
