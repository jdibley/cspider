/**
 * Pass06
 */
package io.github.jamesdibley.cspider.translator.pass;

import java.util.List;
import java.util.ArrayList;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
import org.antlr.v4.runtime.tree.xpath.XPath;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import io.github.jamesdibley.cspider.parser.gen.*;
import io.github.jamesdibley.cspider.parser.gen.CSPMParser.*;
import io.github.jamesdibley.cspider.translator.BasePass;
import io.github.jamesdibley.cspider.translator.TranState;
import io.github.jamesdibley.cspider.translator.ValueType;
import io.github.jamesdibley.cspider.translator.def.*;
import io.github.jamesdibley.cspider.translator.symbol.*;

public class Pass06 extends BasePass {
	private static final Logger logger = LogManager.getLogger(Pass06.class);
	private String rootXPath = "/sourcefile/declaration";
	private String letXPath = "//letClause";
	private String seqExprXPath = 
		"/patternDecl/expression/primaryExpr/seqExpr";
	private ValueType tmpVT;
	private SymbolTable symTable;

	public Pass06(TranState tState, ParseTreeWalker walker, CSPMParser parser) {
		super(tState, walker, parser);
		tmpVT = ValueType.UNKNOWN;
		symTable = tState.symTable;
		logger.trace("Gather all seq decls.");
	}

	public void process(SourcefileContext tree) {
		processTopLevelSeqDecls(tree);
		processLetClauseSeqDecls(tree);
	}

	private void processTopLevelSeqDecls(ParseTree tree) {
		for (ParseTree t : XPath.findAll((ParseTree) tree, 
					rootXPath+seqExprXPath, parser)) {
			PatternDeclContext pdc = 
				(PatternDeclContext) t.getParent().getParent().getParent();
			NamePatternContext np = pdc.patternLHS().namePattern();
			if (np == null || !(np instanceof NpNameContext)) {
				logger.trace("Not a simple declaration, skipping.");
				continue;
			}
			String name = ((NpNameContext)np).Name().getText();

			if (t instanceof ListContext) {
				//logger.trace("Found list.");
				registerList(pdc, name, (ListContext)t, true);
			} else if (t instanceof ListRangedIntContext) {
				//logger.trace("Found ranged int list.");
				registerRangedIntList(pdc, name, 
						(ListRangedIntContext)t, true);
			}
			// TODO: other types of seq declaration!
		}
	}
	
	private void processLetClauseSeqDecls(ParseTree tree) {
		for (ParseTree t : XPath.findAll((ParseTree) tree, 
					letXPath+seqExprXPath, parser)) {
			PatternDeclContext pdc = 
				(PatternDeclContext) t.getParent().getParent().getParent();
			NamePatternContext np = pdc.patternLHS().namePattern();
			if (np == null || !(np instanceof NpNameContext)) {
				logger.trace("Not a simple declaration, skipping.");
				continue;
			}
			String name = ((NpNameContext)np).Name().getText();
			if (t instanceof ListContext) {
				//logger.trace("Found list.");
				registerList(pdc, name, (ListContext)t, false);
			} else if (t instanceof ListRangedIntContext) {
				//logger.trace("Found ranged int list.");
				registerRangedIntList(pdc, name, 
						(ListRangedIntContext)t, false);
			}
		}
	}

	public void registerList(PatternDeclContext pdc, String name,
			ListContext ctx, boolean globalDecl) {
		List<String> elements = new ArrayList<String>();
		ExpressionListContext elc = ctx.expressionList();
		for (ExpressionContext e : elc.expression()) {
			String element = e.getText();
			elements.add(element);
		}

		detectType(elc.expression(0));

		ListDef ld = new ListDef(pdc, elements, tmpVT, name,
				symTable.currentScope());
		logger.trace("List registered: " + ld);
		ld.setTranState(tState);
		setDef(pdc, ld);
		ld.markComplete();
		if (globalDecl) {
			symTable.currentScope().define(ld, name);
		}
		this.tmpVT = ValueType.UNKNOWN;
	}
	
	public void registerRangedIntList(PatternDeclContext pdc, String name,
			ListRangedIntContext ctx, boolean globalDecl) {
		String begin = ctx.expression(0).getText();
		String end = ctx.expression(1).getText();

		ListDef ld = new ListDef(pdc, begin, end, name,
				symTable.currentScope());
		logger.trace("List registered: " + ld);
		ld.setTranState(tState);
		setDef(pdc, ld);
		ld.markComplete();
		if (globalDecl) {
			symTable.currentScope().define(ld, name);
		}
	}

	private void detectType(ExpressionContext ctx) {
		walker.walk(this, (ParseTree) ctx);
	}

	// TESTING: This should never be triggered by this.process() --
	// the walker should -only- be walking the subtrees found by XPath!
	@Override
	public void exitSourcefile(SourcefileContext ctx) {
		logger.trace("Pass06 over.");
	}

	// Overridden rules for Listener on this pass:
	@Override
	public void exitLitInt(LitIntContext ctx) {
		this.tmpVT = ValueType.INT;
	}

	@Override
	public void exitLitChar(LitCharContext ctx) {
		this.tmpVT = ValueType.CHAR;
	}

	@Override
	public void exitLitString(LitStringContext ctx) {
		this.tmpVT = ValueType.STRING;
	}

	@Override
	public void exitPExprName(PExprNameContext ctx) {
		String name = ctx.Name().getText();
		Symbol tmp = symTable.currentScope().resolve(name);
		if (tmp != null) {
			tmpVT = tmp.def().type();
		} else {
			logger.error("Undefined identifier found in list.");
			tState.abortTranslation();
		}
	}
}
