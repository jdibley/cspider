/**
 * ExtBaseDef
 */
package io.github.jamesdibley.cspider.translator.def;

import java.util.LinkedHashMap;
import java.util.Map;
import io.github.jamesdibley.cspider.translator.BaseDef;
import io.github.jamesdibley.cspider.translator.ValueType;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;

public abstract class ExtBaseDef extends BaseDef {
	private Map<String, ProcessDef> referencingProcs;
	private Map<String, FunctionDef> referencingFuncs;
	private Map<String, SetDef> referencingSets;

	public ExtBaseDef(ParserRuleContext ctx, ValueType type, String name) {
		super(ctx, name);
		setType(type);
		this.referencingProcs = new LinkedHashMap<String, ProcessDef>();
		this.referencingFuncs = new LinkedHashMap<String, FunctionDef>();
		this.referencingSets = new LinkedHashMap<String, SetDef>();
	}

	public Map<String, ProcessDef> getReferencingProcs() {
		return referencingProcs;
	}

	public Map<String, FunctionDef> getReferencingFuncs() {
		return referencingFuncs;
	}

	public Map<String, SetDef> getReferencingSets() {
		return referencingSets;	
	}

	public void addReferencingProc(ProcessDef pd) {
		referencingProcs.put(pd.name(), pd);
	}

	public void addReferencingFunc(FunctionDef fd) {
		referencingFuncs.put(fd.name(), fd);
	}

	public void addReferencingSet(SetDef sd) {
		referencingSets.put(sd.name(), sd);
	}

	public ProcessDef referencingProc(String name) {
		return referencingProcs.get(name);
	}
	
	public FunctionDef referencingFunc(String name) {
		return referencingFuncs.get(name);
	}
	
	public SetDef referencingSet(String name) {
		return referencingSets.get(name);
	}

	public String toString() {
		return type().toString() 
			+ referencingProcs.keySet().toString()
			+ referencingFuncs.keySet().toString()
			+ referencingSets.keySet().toString();
	}
}
