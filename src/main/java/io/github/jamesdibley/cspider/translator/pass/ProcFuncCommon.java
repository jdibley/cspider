/**
 * ProcFuncCommon
 */ 
package io.github.jamesdibley.cspider.translator.pass;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
import org.antlr.v4.runtime.tree.xpath.XPath;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import io.github.jamesdibley.cspider.parser.gen.*;
import io.github.jamesdibley.cspider.parser.gen.CSPMParser.*;
import io.github.jamesdibley.cspider.translator.BaseDef;
import io.github.jamesdibley.cspider.translator.BasePass;
import io.github.jamesdibley.cspider.translator.TranState;
import io.github.jamesdibley.cspider.translator.ValueType;
import io.github.jamesdibley.cspider.translator.symbol.*;
import io.github.jamesdibley.cspider.translator.def.*;
import io.github.jamesdibley.cspider.translator.def.var.*;

public class ProcFuncCommon extends BasePass {
	private static final Logger logger = LogManager.getLogger(ProcFuncCommon.class);
	
	// These attributes determine how to process names found in the parse 
	// tree.
	protected boolean classifying = false;
	protected boolean tmpInput = false; 
	protected boolean tmpParallel = false; 
	protected boolean tmpSyncFunction = false;
	private int tmpChanOpCtr = 0;
	// External Choice
	private ExternalChoiceDef tmpExtCh = null;
	private int extChCtr = 0;
	private int altCtr = 0;
	// These attributes collect whether a subtree walk identified a process 
	// definition 
	protected boolean walkedProcessDecl = false;
	protected CompType walkedCompType = CompType.NOT_COMP;
	
	protected List<String> tmpFields = null; 
	protected List<PrimaryExprContext> tmpChanExprs = null; 
	protected Map<String, ExtBaseDef> tmpExtRefs = null;
	

	protected Map<String, SetDef> walkedSetRefs = null;
	protected Map<String, ListDef> walkedListRefs = null;
	protected Map<String, BaseDef> walkedConstRefs = null;

	public ProcFuncCommon(TranState tState, ParseTreeWalker walker, 
			CSPMParser parser) {
		super(tState, walker, parser);
		walkedSetRefs = new LinkedHashMap<String, SetDef>();
		walkedListRefs = new LinkedHashMap<String, ListDef>();
		walkedConstRefs = new LinkedHashMap<String, BaseDef>();
		tmpExtRefs = new LinkedHashMap<String, ExtBaseDef>();
	}

	protected boolean isRegularPattern(NamePatternContext npc) {
		if (npc==null || !(npc instanceof NpNameContext)) {
			return false;
		}
		return true;
	}

	protected boolean isParametricPattern(NamePatternContext npc) {
		if (npc == null || !(npc instanceof NpArgsContext)) {
			return false;
		}
		return true;
	}

	protected String getTmpName(NamePatternContext npc) {
		if (npc instanceof NpNameContext) {
			NpNameContext nnc = (NpNameContext) npc;
			return nnc.Name().getText();
		} else if (npc instanceof NpArgsContext) {
			NpArgsContext nac = (NpArgsContext) npc;
			return nac.namePattern().getText();
		}
		return "";
	}

	protected Map<String, String> processRenamingBlock(PatternDeclContext pdc) {
		if (!(pdc.expression() instanceof ExprRenameContext)) {
			return new LinkedHashMap<String, String>();
		} 
		ExprRenameContext erc = (ExprRenameContext) pdc.expression();
		RenamingClauseContext rcc = erc.renamingClause();
		return processRenamingClause(rcc);
	}

	protected Map<String, String> processRenamingBlock(
			ParametricPatternDeclContext ppdc) {
		if (!(ppdc.expression(1) instanceof ExprRenameContext)) {
			return new LinkedHashMap<String, String>();
		}
		ExprRenameContext erc = (ExprRenameContext) ppdc.expression(1);
		RenamingClauseContext rcc = erc.renamingClause();
		return processRenamingClause(rcc);
	}

	// This function processes a renaming expression of the form: 
	//
	//	bar(.x)* <- foo(.x)*
	//
	// where bar is the identifier of a channel within a process 
	// expression, and where foo(.x)* is the corresponding channel's 
	// global identifier. 
	// 	In a CSPm file both bar and foo will be declared as 
	// global channels. In a verified CSPm file, the types will have
	// been checked, meaning that if foo.x is mapped to bar, then bar's 
	// channel spec will correspond to foo's channel spec without the 
	// initial indexing field.
	//	Because channel specs are -assumed- to describe data fields
	// in the first instance, encountering an expression such as 
	// bar <- foo.i triggers an update on foo's channel spec, marking its
	// first field as an indexing field.
	// 	The same is -not- true of expressions such as bar.i <- foo,
	// which may appear in the terms of a process composition. These are 
	// typically in/out mappings to indexes of a process network and have
	// no implications for the classification of the channels involved.
	// 	For the corresponding updates triggered on non-renamed channels
	// that appear within process expressions, see scanIOOperations().
	protected Map<String, String> processRenamingClause(RenamingClauseContext rcc) {
		Map<String, String> renamingMap = 
			new LinkedHashMap<String, String>();
		//logger.trace(rcc.getText());
		for (RenamingContext rc : rcc.renaming()) {
		// The external event will be either: 
		// 'foo'
		// 'foo.x'
		// 'foo.x.y...'
			String intChanExpr = rc.expression(0).getText();
			List<String> intChanExprs = 
				new ArrayList<String>(Arrays.asList(intChanExpr.split("\\.")));
			String extChanExpr = rc.expression(1).getText();
			List<String> extChanExprs = 
				new ArrayList<String>(Arrays.asList(extChanExpr.split("\\.")));

			for (String s : intChanExprs) {
			//	logger.trace(s);
				Symbol sym = tState.symTable.currentScope().resolve(s);
				if (sym != null && sym.def() != null 
						&& sym.def() instanceof ExtBaseDef) {
					ExtBaseDef ebd = (ExtBaseDef) sym.def();
					tmpExtRefs.put(s, ebd);	
					//logger.trace("added ext ref: " + s);
				}
			}
			for (String s : extChanExprs) {
			//	logger.trace(s);
				Symbol sym = tState.symTable.currentScope().resolve(s);
				if (sym != null && sym.def() != null 
						&& sym.def() instanceof ExtBaseDef) {
					ExtBaseDef ebd = (ExtBaseDef) sym.def();
					tmpExtRefs.put(s, ebd);	
					//logger.trace("added ext ref: " + s);
				}
			}

			ChannelDef icd = getChannel(intChanExprs.remove(0));
			ChannelDef ecd = getChannel(extChanExprs.remove(0));
			//logger.trace("ecd before marking: " + ecd);
			for (int i = 0; i < extChanExprs.size(); i++) {
				ecd.markAsIndex(i);
			}
			//logger.trace("ecd after marking:  " + ecd);
	
			// Visibility control:
			// (a) is the target channel of the renaming obviously 
			// an object channel?
			// (b) does the target channel of the renaming  
			// clearly represent an index of the source channel? 
			// 
			if (icd.name().matches("(.*)In")
					|| icd.name().matches("(.*)Out")
					|| intChanExprs.size() < extChanExprs.size()) {
				// Mark internal chan as internal 
				icd.setVisibilityObject();
			}


			//renamingMap.put(icd.name(), ecd.name());
			renamingMap.put(intChanExpr, extChanExpr);
			//logger.trace("added renaming: " + intChanExpr + " << " + extChanExpr);
		}
		return renamingMap;
	}


	protected void registerParametricProcess(ParametricPatternDeclContext ppdc,
			String tmpName, List<ParamDef> tmpParams, 
			Map<String, String> tmpRenameMap) {
		Scope parentScope = tState.symTable.currentScope().parentScope();
		ProcessDef pd = new ProcessDef(ppdc, null, parentScope, tmpName);
		// We also stow the scope for the -contents- of this process and
		// its expression:
		pd.setContainedScope(tState.symTable.currentScope());
		//logger.trace("parametric process registration");
		scanIOOperations(ppdc.expression(1), pd);

		walker.walk(this, ppdc.expression(1));
		
		if (walkedCompType != CompType.UNKNOWN) {
			pd.setCompType(walkedCompType);
			resetWalkedCompType();
		}
	
		for (ParamDef prd : tmpParams) {
			pd.addParam(prd);
		}

		for (SetDef sd : walkedSetRefs.values()) {
			sd.addReferencingProc(pd);
			pd.addSetRef(sd);
		}
		walkedSetRefs.clear();

		for (ListDef ld : walkedListRefs.values()) {
			ld.addReferencingProc(pd);
			pd.addListRef(ld);
		}
		walkedListRefs.clear();

		for (BaseDef bd : walkedConstRefs.values()) {
			pd.addConstRef(bd);
			if (bd instanceof ConstIntDef) {
				ConstIntDef cid = (ConstIntDef) bd;
				for (ExtBaseDef ebd : cid.extRefs()) {
					boolean matchesParam = false;
					for (ParamDef prd : tmpParams) {
						if (prd.name().matches(ebd.name())) {
							matchesParam = true;
						}
					}
					if (matchesParam) {
						continue;
					} else {
						ebd.addReferencingProc(pd);
						pd.addExtRef(ebd);
					}
				}
			}
		//	logger.trace("added constref " + bd.name() + 
		//		" + to processDef" + pd.name());
		}
		walkedConstRefs.clear();
		//logger.trace(pd.name() + " constref keys: " + pd.constRefKeys());

		for (ExtBaseDef ebd : tmpExtRefs.values()) {
		//	logger.trace("ebd name: " + ebd.name());
			boolean matchesParam = false;
			for (ParamDef prd : tmpParams) {
		//		logger.trace("param name: " + prd.name());
				if (prd.name().matches(ebd.name())) {
					matchesParam = true;
				}
			}
			if (matchesParam) {
				continue;
			} else {
				ebd.addReferencingProc(pd);
				pd.addExtRef(ebd);
			}
		}
		tmpExtRefs.clear();

		//logger.trace(pd.name() + " tmp rename map: " + tmpRenameMap);
		for (String s : tmpRenameMap.keySet()) {
			pd.addChanRenaming(s, tmpRenameMap.get(s));
		}

		if (ppdc.letClause() != null) {
			LetClauseContext lcc = ppdc.letClause();
			LetClauseDef lcd = new LetClauseDef(lcc, 
					tState.symTable.currentScope(), pd);
			lcd.setTranState(tState);
			setDef(lcc, lcd);
		}

		//logger.trace(pd.name());
		//logger.trace("params: " );
		//for (ParamDef prd : pd.params()) {
		//	logger.trace(prd.name());
		//}
		//logger.trace("extrefs: " + pd.extRefKeys());

		pd.setTranState(tState);
		setDef(ppdc, pd);
		tState.symTable.currentScope().parentScope().define(pd, tmpName);
	}

	protected void registerFunction(ParametricPatternDeclContext ppdc,
			String tmpName, List<ParamDef> tmpParams) {
		Scope parentScope = tState.symTable.currentScope().parentScope();
		FunctionDef fd = new FunctionDef(ppdc, parentScope, tmpName); 
		
		fd.setContainedScope(tState.symTable.currentScope());
		
		for (ParamDef prd : tmpParams) {
			fd.addParam(prd);
		}

		walker.walk(this, ppdc.expression(1));
	
		for (SetDef sd : walkedSetRefs.values()) {
			sd.addReferencingFunc(fd);
			fd.addSetRef(sd);
		}

		for (ListDef ld : walkedListRefs.values()) {
			ld.addReferencingFunc(fd);
			fd.addListRef(ld);
		}

		for (BaseDef bd : walkedConstRefs.values()) {
			fd.addConstRef(bd);
			if (bd instanceof ConstIntDef) {
				ConstIntDef cid = (ConstIntDef) bd;
				for (ExtBaseDef ebd : cid.extRefs()) {
					boolean matchesParam = false;
					for (ParamDef prd : tmpParams) {
						if (prd.name().matches(ebd.name())) {
							matchesParam = true;
						}
					}
					if (matchesParam) {
						continue;
					} else {
						ebd.addReferencingFunc(fd);
						fd.addExtRef(ebd);
					}
				}
			}
		//	logger.trace("added constref " + bd.name() + 
		//		" + to processDef" + pd.name());
		}
		walkedConstRefs.clear();
		//logger.trace(pd.name() + " constref keys: " + pd.constRefKeys());

		for (ExtBaseDef ebd : tmpExtRefs.values()) {
		//	logger.trace("ebd name: " + ebd.name());
			boolean matchesParam = false;
			for (ParamDef prd : tmpParams) {
		//		logger.trace("param name: " + prd.name());
				if (prd.name().matches(ebd.name())) {
					matchesParam = true;
				}
			}
			if (matchesParam) {
				continue;
			} else {
				ebd.addReferencingFunc(fd);
				fd.addExtRef(ebd);
			}
		}
		tmpExtRefs.clear();
		/*
		for (ExtBaseDef ebd : tmpExtRefs.values()) {
			ebd.addReferencingFunc(fd);
			fd.addExtRef(ebd);
		}
		tmpExtRefs.clear();
		*/

		if (ppdc.letClause() != null) {
			LetClauseContext lcc = ppdc.letClause();
			LetClauseDef lcd = new LetClauseDef(lcc, 
					tState.symTable.currentScope(), fd);
			lcd.setTranState(tState);
			setDef(lcc, lcd);
		}
		
		registerFunctionType(fd, ppdc, tmpName);
		
		fd.setTranState(tState);
		setDef(ppdc, fd);
		tState.symTable.currentScope().parentScope().define(fd, tmpName);
	}
	
	protected void registerTmpParams(ParametricPatternDeclContext ppdc,
			NpArgsContext nac, List<ParamDef> tmpParams) {
		for (ExpressionContext e : ppdc.expressionList().expression()) {
			int idx = ppdc.expressionList().expression().indexOf(e);
			ExpressionContext paramID = nac.expression(idx);
			String paramName = paramID.getText();
			ParamDef prd = new ParamDef(paramID, paramName);
			if (!(e instanceof ExprPrimaryContext)) {
				logger.error("Wrong ExprCtx found in registerTmpParams");
				continue;
			}

			Symbol sym = 
				tState.symTable.currentScope().resolve(paramName);
			if (sym != null && 
				( sym.def() instanceof ConstIntDef
				  || sym.def() instanceof ConstStringDef
				  || sym.def() instanceof ConstCharDef)) {
				//logger.trace("found a const ref: " + paramName);
				walkedConstRefs.put(paramName, sym.def());
			}

			PrimaryExprContext pec = ((ExprPrimaryContext)e).primaryExpr();
			if (pec instanceof PExprSetExprContext) {
				if (!(registerParamTypeSet(pec, prd, paramID, ppdc, e))) {
					logger.error("Couldn't register set param: "
							+ pec.getText());
					continue;
				}
			} else if (pec instanceof PExprSeqExprContext) {
				logger.trace("Registering sequence param. " + pec.getText() );
				if (!(registerParamTypeSeq(pec, prd, paramID, ppdc, e))) {
					logger.error("Couldn't register seq param: "
							+ pec.getText());
					continue;
				}
			} else if (pec instanceof PExprBuiltInIdentifierContext) {
				if (!(registerParamTypeSimple(pec, prd, paramID))) {
					logger.error("Couldn't register simple param: "
							+ pec.getText());
				}
			}
			prd.setTranState(tState);
			setDef((ParseTree) paramID, prd);
			tState.symTable.currentScope().define(prd, paramName);
			tmpParams.add(prd);
		}
	}

	protected boolean registerParamTypeSet(PrimaryExprContext pec, ParamDef prd,
			ExpressionContext paramID,
			ParametricPatternDeclContext ppdc,
			ExpressionContext e) {
		VarSetDef vsd = new VarSetDef(e, paramID.getText());
		SetExprContext sec = 
			((PExprSetExprContext)pec).setExpr();
		ExpressionContext ec = 
			((SetContext)sec).expressionList().expression(0);
		PrimaryExprContext setPec = 
			((ExprPrimaryContext)ec).primaryExpr();
		if (setPec instanceof PExprBuiltInIdentifierContext) {
			String id = setPec.getText();
			if (id.matches("Int")) {
				vsd.setMemberType(ValueType.INT);
			} else if (id.matches("Bool")) {
				vsd.setMemberType(ValueType.BOOL);
			} else if (id.matches("Char")) {
				vsd.setMemberType(ValueType.CHAR);
			}
		} else {
			logger.error("Function set param has no clear"
					+ " member type." 
					+ ppdc.getText());
			tState.abortTranslation();
			return false;
		}
		prd.setVar(vsd);
		return true;
	}

	protected boolean registerParamTypeSeq(PrimaryExprContext pec, ParamDef prd, 
			ExpressionContext paramID,
			ParametricPatternDeclContext ppdc,
			ExpressionContext e) {
		VarListDef vld = new VarListDef(e, paramID.getText());
		SeqExprContext sec = ((PExprSeqExprContext)pec).seqExpr();
		ExpressionContext ec = 
			((ListContext)sec).expressionList().expression(0);
		PrimaryExprContext seqPec = 
			((ExprPrimaryContext)ec).primaryExpr();
		if (seqPec instanceof PExprBuiltInIdentifierContext) {
			String id = seqPec.getText();
			if (id.matches("Int")) {
				vld.setElemType(ValueType.INT);
			} else if (id.matches("Bool")) {
				vld.setElemType(ValueType.BOOL);
			} else if (id.matches("Char")) {
				vld.setElemType(ValueType.CHAR);
			}
		} else {
			logger.error("Function list param has no clear"
					+ " element type."
					+ ppdc.getText());
			tState.abortTranslation();
			return false;
		}
		prd.setVar(vld);
		return true;
	}

	protected boolean registerParamTypeSimple(PrimaryExprContext pec, ParamDef prd, 
			ExpressionContext paramID) {
		String id = pec.getText();
		if (id.matches("Int")) {
			VarIntDef vid = new VarIntDef(paramID, paramID.getText());
			prd.setVar(vid);
		} else if (id.matches("Bool")) {
			VarBoolDef vbd = new VarBoolDef(paramID, paramID.getText());
			prd.setVar(vbd);
		} else if (id.matches("Char")) {
			VarCharDef vcd = new VarCharDef(paramID, paramID.getText());
			prd.setVar(vcd);
		}
		return true;
	}

	protected void registerFunctionType(FunctionDef fd,
			ParametricPatternDeclContext ppdc, String name) {
		ExpressionContext e = ppdc.expression(0);
//		logger.trace(e.getText());
		if (!(e instanceof ExprPrimaryContext)) {
			logger.error("Wrong ExprCtx in registerFunctionType");
			tState.abortTranslation();
			return;
		}
		PrimaryExprContext pec = ((ExprPrimaryContext)e).primaryExpr();
		if (pec instanceof PExprSetExprContext) {
//			logger.trace("Function returns a set type.");
			VarSetDef rv = new VarSetDef(e, name);
			SetExprContext sec = ((PExprSetExprContext)pec).setExpr();
			ExpressionContext ec = 
				((SetContext)sec).expressionList().expression(0);
			// Text will be: {Int} | {Bool} | {Char} | {a} | {b}...
			PrimaryExprContext setPec = 
				((ExprPrimaryContext)ec).primaryExpr();
			if (setPec instanceof PExprBuiltInIdentifierContext) {
				String id = setPec.getText();
				if (id.matches("Int")) {
					rv.setMemberType(ValueType.INT);
				} else if (id.matches("Bool")) {
					rv.setMemberType(ValueType.BOOL);
				} else if (id.matches("Char")) {
					rv.setMemberType(ValueType.CHAR);
				} else if (id.matches("Event")) {
					// Function defines sync set
					rv.setMemberType(ValueType.EVENT);
					fd.markExcluded();
				}
			} else { 
				logger.error("Function returns no clear type." +
						ppdc.getText());
				tState.abortTranslation();
				return;
			}
			fd.setReturnType(rv);
		} else if (pec instanceof PExprSeqExprContext) {
//			logger.trace("Function returns a list type." );
			VarListDef rv = new VarListDef(e, name);
			SeqExprContext sec = ((PExprSeqExprContext)pec).seqExpr();
			ExpressionContext ec = 
				((ListContext)sec).expressionList().expression(0);
			PrimaryExprContext seqPec = 
				((ExprPrimaryContext)ec).primaryExpr();
			if (seqPec instanceof PExprBuiltInIdentifierContext) {
				String id = seqPec.getText();
//				logger.trace(id);
				if (id.matches("Int")) {
					rv.setElemType(ValueType.INT);
				} else if (id.matches("Bool")) {
					rv.setElemType(ValueType.BOOL);
				} else if (id.matches("Char")) {
					rv.setElemType(ValueType.CHAR);
				}
			} else { 
				logger.error("Function returns no clear seq type."
						+ ppdc.getText());
				tState.abortTranslation();
				return;
			}
			fd.setReturnType(rv);
		} else if (pec instanceof PExprBuiltInIdentifierContext) {
			String id = pec.getText();
			if (id.matches("Int")) {
//				logger.trace("Function returns an int.");
				VarIntDef rv = new VarIntDef(e, name);
				fd.setReturnType(rv);
			} else if (id.matches("Bool")) {
//				logger.trace("Function returns a bool.");
				VarBoolDef rv = new VarBoolDef(e, name);
				fd.setReturnType(rv);
			} else if (id.matches("Char")) {
//				logger.trace("Function returns a char.");
				VarCharDef rv = new VarCharDef(e, name);
				fd.setReturnType(rv);
			}
		}	
	}

	protected void scanIOOperations(ExpressionContext subtree, ProcessDef pd) {
		classifying = false;
		String path = "//expression";
		tmpFields = new ArrayList<String>();
		tmpChanExprs = new ArrayList<PrimaryExprContext>();
		for (ParseTree t : XPath.findAll(subtree, path, parser)) {
			if (t.getParent() instanceof RenamingContext) {
				// skip entries in renaming clauses
				// (handle these in later pass)
				continue;
			}
			if (t instanceof ExprInputContext) {
				processChanInput(t, pd);
			} else if (t instanceof ExprOutputContext) {
				processChanOutput(t, pd);
			} else if (t instanceof ExprDottedContext) {
				// Ensure this isn't nested inside an expression
				// we've already examined
				if (t.getParent() instanceof ExprDottedContext 
					|| t.getParent() instanceof ExprInputContext
					|| t.getParent() instanceof ExprOutputContext
					|| t.getParent().getParent() instanceof SetExprContext 
					|| t.getParent().getParent() instanceof SeqExprContext) {
					continue;
				}
				processDottedChan(t, pd);
				//logger.trace("scanIOOperations: " + pd);	
			} else if (t instanceof ExprPrimaryContext) {
				if (t.getParent() instanceof ExprDottedContext
					|| t.getParent() instanceof ExprInputContext
					|| t.getParent() instanceof ExprOutputContext
					|| t.getParent().getParent() instanceof SetExprContext
					|| t.getParent().getParent() instanceof SeqExprContext) {
					continue;
				}
				processSimpleChan(t, pd);
			}
		}
	}

	protected void processChanInput(ParseTree t, ProcessDef pd) {
		ExprInputContext eic = (ExprInputContext) t;
		boolean msgStructRequired = false;
	
		// Grab the LHS as a block of text to process conveniently for 
		// metadata before we recursively gather the expression 
		// contexts for codegen.
		String lhs = eic.expression(0).getText();
		List<String> lhsStrings = 
			new ArrayList<String>(Arrays.asList(lhs.split("\\.")));

		ChannelDef cd = getChannel(lhsStrings.remove(0));

		for (int i = 0; i < lhsStrings.size(); i++) {
			cd.markAsIndex(i);
		}

		String opName = pd.name().toLowerCase()
				+ cd.name().substring(0,1).toUpperCase()
				+ cd.name().substring(1);

		ChanInputDef cid = new ChanInputDef((ParserRuleContext) t, 
				opName + "In" + tmpChanOpCtr++, cd);

		for (int i = 0; i < lhsStrings.size(); i++) {
			cid.addIndex(lhsStrings.get(i));
		}
		setDef(t, cid);

		switch (cd.chanType()) {
		case EVENT:
		case DATA:
		case MULTI_DATA:
			pd.addChanRef(cd);
			break;
		case INDEXED_EVENT:
		case INDEXED_DATA:
		case INDEXED_MULTI_DATA:
			pd.addIndexedChanRef(cd);
			break;
		}
	
		// Recursively gather the expression contexts on the LHS for 
		// later codegen. We're only interested in indexing (not the 
		// chan name), so we don't bother looking at anything besides a 
		// dotted expr, and having done so, we discard the first term 
		// (which is the chan name)
		if (eic.expression(0) instanceof ExprDottedContext) {
			ExprDottedContext edc = (ExprDottedContext) eic.expression(0);
			//cid.addIndexExpr(edc.expression(0));
			while (edc.expression(1) instanceof ExprDottedContext) {
				cid.addIndexExpr(edc.expression(0));
				edc = (ExprDottedContext) edc.expression(1);
			}
			cid.addIndexExpr(edc.expression(0));
			cid.addIndexExpr(edc.expression(1));
		}
		if (cid.indexExprs().size() > 0) {
			cid.indexExprs().remove(0);	
		//	logger.trace("cid.indexExprs() size is " 
		//			+ cid.indexExprs().size() 
		//			+ " ... " 
		//			+ eic.getText());
		}
		// We also need a list of contexts to assign variables with
		tmpInput = true;
		walker.walk(this, eic.expression(1));
		//logger.trace("inputExprs after 2nd walk: ");

		for (PrimaryExprContext pec : tmpChanExprs) {
			String name = pec.getText();
			int specIdx = tmpChanExprs.indexOf(pec);
			registerInputFieldVar(pec, name, cd, specIdx, pd);
			cid.addIdentifier(name);
			//logger.trace("tmpChanExpr: " + name);
		}
		tmpChanExprs.clear();
		tmpInput = false;

	}

	protected void processChanOutput(ParseTree t, ProcessDef pd) {
		ExprOutputContext eoc = (ExprOutputContext) t;
		// We want to capture the exact expressions on both
		// sides of an output expression.
		String lhs = eoc.expression(0).getText();
		List<String> lhsStrings = 
			new ArrayList<String>(Arrays.asList(lhs.split("\\.")));

		ChannelDef cd = getChannel(lhsStrings.remove(0));
		
		for (int i = 0; i < lhsStrings.size(); i++) {
			cd.markAsIndex(i);
		}

		ChanOutputDef cod = new ChanOutputDef((ParserRuleContext) t,
						pd.name().toLowerCase()
						+ cd.name().substring(0,1).toUpperCase()
						+ cd.name().substring(1)
						+ "Out" + tmpChanOpCtr++, cd);
		
		for (int i = 0; i < lhsStrings.size(); i++) {
			cod.addIndex(lhsStrings.get(i));
		}
		setDef(t, cod);

		switch (cd.chanType()) {
		case EVENT:
		case DATA:
		case MULTI_DATA:
			pd.addChanRef(cd);
			break;
		case INDEXED_EVENT:
		case INDEXED_DATA:
		case INDEXED_MULTI_DATA:
			pd.addIndexedChanRef(cd);
			break;
		}
	
		// Recursively gather the expression contexts on the LHS for 
		// later codegen.
		if (eoc.expression(0) instanceof ExprDottedContext) {
			ExprDottedContext edc = (ExprDottedContext) eoc.expression(0);
			//cid.addIndexExpr(edc.expression(0));
			while (edc.expression(1) instanceof ExprDottedContext) {
				cod.addIndexExpr(edc.expression(0));
				edc = (ExprDottedContext) edc.expression(1);
			}
			cod.addIndexExpr(edc.expression(0));
			cod.addIndexExpr(edc.expression(1));
		}
		if (cod.indexExprs().size() > 0) {
			cod.indexExprs().remove(0);	
		//	logger.trace("cod.indexExprs() size is " 
		//			+ cod.indexExprs().size() 
		//			+ " ... " 
		//			+ eoc.getText());
		}

		String rhs = eoc.expression(1).getText();
		List<String> rhsStrings =
			new ArrayList<String>(Arrays.asList(rhs.split("\\.")));

		for (String s : rhsStrings) {
			cod.addExpression(s);
		}
		
		// Recursively gather the expression contexts on the RHS for 
		// later codegen.
		if (eoc.expression(1) instanceof ExprDottedContext) {
			ExprDottedContext edc = (ExprDottedContext) eoc.expression(1);
			//cid.addIndexExpr(edc.expression(0));
			while (edc.expression(1) instanceof ExprDottedContext) {
				cod.addExpressionExpr(edc.expression(0));
				edc = (ExprDottedContext) edc.expression(1);
			}
			cod.addExpressionExpr(edc.expression(0));
			cod.addExpressionExpr(edc.expression(1));
		} else {
			cod.addExpressionExpr(eoc.expression(1));
		}
		//logger.trace("cod.expressionExprs() size is " 
		//		+ cod.expressionExprs().size() 
		//		+ " ... " 
		//		+ eoc.getText());
	}

	protected void processDottedChan(ParseTree t, ProcessDef pd) {
		ExprDottedContext edc = (ExprDottedContext) t;
		// It's a lot easier to treat this accurately by 
		// flattening it into a string than it is to walk the subtree.
		String dotString = edc.getText();
		//logger.trace("processDottedChan: " + edc.getText());
		List<String> dotExprs = 
			new ArrayList<String>(Arrays.asList(dotString.split("\\.")));

		String chanOp = dotExprs.remove(0);
		ChannelDef cd = getChannel(chanOp);

		// This should always be an Event or IdxEvent channel,
		// entailing that its name will end either "In" or "Out".
		if (chanOp.matches("(.*)In")) {
			for (int i = 0; i < dotExprs.size(); i++) {
				cd.markAsIndex(i);
			}			
			ChanInputDef cid = 
				new ChanInputDef((ParserRuleContext) t,
						pd.name().toLowerCase()
						+ cd.name().substring(0,1).toUpperCase()
						+ cd.name().substring(1)
						+ "In" + tmpChanOpCtr++, cd);
			for (int i = 0; i < dotExprs.size(); i++) {
				cid.addIndex(dotExprs.get(i));
			}
			setDef(t, cid);
		} else if (chanOp.matches("(.*)Out")) {
			for (int i = 0; i < dotExprs.size(); i++) {
				cd.markAsIndex(i);
			}
			ChanOutputDef cod = 
				new ChanOutputDef((ParserRuleContext) t,
						pd.name().toLowerCase()
						+ cd.name().substring(0,1).toUpperCase()
						+ cd.name().substring(1)
						+ "Out" + tmpChanOpCtr++, cd);
			for (int i = 0; i < dotExprs.size(); i++) {
				cod.addIndex(dotExprs.get(i));
			}
			setDef(t, cod);
		} else {
			logger.error("Dotted chanop found in process expression: " 
					+ edc.getText());
		}
		// no identifiers to worry about with this type of operation 
		switch (cd.chanType()) {
		case EVENT:
		case DATA:
		case MULTI_DATA:
			pd.addChanRef(cd);
			//logger.trace("Added channel ref for " + cd.name() 
			//		+ " to " + pd.name());
			break;
		case INDEXED_EVENT:
		case INDEXED_DATA:
		case INDEXED_MULTI_DATA:
			pd.addIndexedChanRef(cd);
			//logger.trace("Added indexed channel ref for " + cd.name() 
			//		+ " to " + pd.name());
			break;
		}
		tmpChanExprs.clear();
		//logger.trace("processDottedChan: " + pd);
	}

	protected void processSimpleChan(ParseTree t, ProcessDef pd) { 
		ExprPrimaryContext epc = (ExprPrimaryContext) t;
		if (!(epc.primaryExpr() instanceof PExprNameContext)) {
			return;
		}
		String chanOp = epc.getText();
		ChannelDef cd = getChannel(chanOp);
		// Many things with names aren't channels...
		if (cd == null) { return; } 

		// ...but this one -is-
		if (chanOp.matches("(.*)In")) {
			ChanInputDef cid = 
				new ChanInputDef((ParserRuleContext) t,
						pd.name() + "-" + cd.name() + "-In" + 
						tmpChanOpCtr++, cd);
			setDef(t, cid);
		} else if (chanOp.matches("(.*)Out")) {
			ChanOutputDef cod = 
				new ChanOutputDef((ParserRuleContext) t,
						pd.name() + "-" + cd.name() + "-Out" + 
						tmpChanOpCtr++, cd);
			setDef(t, cod);
		} else {
			logger.error("Channel with weird name? " + epc.getText());
		}
		
		if (pd.chanRenaming(cd.name()) != null) {
			String expr = pd.chanRenaming(cd.name());
			List<String> exprList = 
				new ArrayList<String>(Arrays.asList(expr.split("\\.")));
			String otherChan = exprList.remove(0);
			Symbol ocSym = 
				tState.symTable.currentScope().resolve(otherChan);
			ChannelDef ocDef = (ChannelDef) ocSym.def();
			switch (ocDef.chanType()) {
			case EVENT:
				pd.addChanRef(ocDef);
			case INDEXED_EVENT:
				pd.addIndexedChanRef(ocDef);
			default:
				logger.error("Unexpected chan type in renamed event chanop!");
			}
		} else {
			pd.addChanRef(cd);
		}
	}

	protected ChannelDef getChannel(String name) {
		Symbol chanSym = 
			tState.symTable.currentScope().resolve(name);
		if (chanSym == null) {
			return null;
		}
		BaseDef bd = chanSym.def();
		if (!(bd instanceof ChannelDef)) {
			return null;
		}
		return (ChannelDef) bd;
	}

	protected void registerInputFieldVar(PrimaryExprContext ctx, 
			String name, ChannelDef cd, int fieldIdx, 
			ProcessDef pd) {
		SetDef fieldSpec = cd.getSpecField(fieldIdx);
		if (fieldSpec == null) {
			logger.error("got a null fieldspec on " +
					cd.name() + " for idx: " + 
					fieldIdx + "(channel spec size: " +
					cd.spec().size() + ")");
		
			
			logger.trace("ChannelDef debug in registerInputFieldVar: " +
					cd);
			return;
		}
		if (fieldSpec.memberType() == ValueType.INT) {
			VarIntDef vid = new VarIntDef(ctx, name);
			registerVarDef(ctx, vid, name, pd);
			//logger.trace("registered int: " + name);
		} else if (fieldSpec.memberType() == ValueType.BOOL) {
			VarBoolDef vbd = new VarBoolDef(ctx, name);
			registerVarDef(ctx, vbd, name, pd);
			//logger.trace("registered bool: " + name);
		} else if (fieldSpec.memberType() == ValueType.CHAR) {
			VarCharDef vcd = new VarCharDef(ctx, name);	
			registerVarDef(ctx, vcd, name, pd);
			//logger.trace("registered char: " + name);
		} 
	}

	protected void registerVarDef(PrimaryExprContext ctx, 
			BaseDef bd, String name, ProcessDef pd) {
		Scope processScope = pd.containedScope();
		bd.setTranState(tState);
		setDef(ctx, bd);
		Symbol sym = new Symbol(bd, name);
		processScope.define(sym);
	}

	protected void resetWalkedCompType() {
		walkedCompType = CompType.NOT_COMP;
	}

	// Listener overrides

	protected void setWPD() {
		walkedProcessDecl = true;
	}

	protected void resetWP() {
		walkedProcessDecl = false;
	}

	@Override
	public void enterExprPrefix(ExprPrefixContext ctx) {
		setWPD();
	}

	@Override
	public void enterExprGuarded(ExprGuardedContext ctx) {
		setWPD();		
	}

	@Override
	public void enterExprTimeout(ExprTimeoutContext ctx) {
		setWPD();
	}

	@Override
	public void enterExprInterrupt(ExprInterruptContext ctx) {
		setWPD();
	}

	@Override
	public void enterExprIntCh(ExprIntChContext ctx) {
		setWPD();	
	}	

	@Override
	public void enterExprExtCh(ExprExtChContext ctx) {
		if (classifying) { 
			setWPD();
			return;
		}
		if (!(ctx.getParent() instanceof ExprExtChContext)) {
			extChCtr += 1;
			tmpExtCh = new ExternalChoiceDef(ctx, "ExtCh" + extChCtr);
			if (ctx.expression(0) instanceof ExprExtChContext) {
				ExprExtChContext eecc = (ExprExtChContext) ctx.expression(0);
				while (eecc.expression(0) instanceof ExprExtChContext) {
					eecc = (ExprExtChContext) eecc.expression(0);
				}
				tmpExtCh.alternatives.add(new AlternativeDef(eecc.expression(0),
								"ExtCh-" + extChCtr + "-Alt-" + altCtr++));
				tmpExtCh.alternatives.add(new AlternativeDef(eecc.expression(1),
								"ExtCh-" + extChCtr + "-Alt-" + altCtr++));
				eecc = (ExprExtChContext) eecc.getParent();
				while (eecc.getParent() instanceof ExprExtChContext) {
					tmpExtCh.alternatives.add(new AlternativeDef(eecc.expression(1),
								"ExtCh-" + extChCtr + "-Alt-" + altCtr++));
					eecc = (ExprExtChContext) eecc.getParent();
				}
				tmpExtCh.alternatives.add(new AlternativeDef(eecc.expression(1),
								"ExtCh-" + extChCtr + "-Alt-" + altCtr++));
			} else {
				tmpExtCh.alternatives.add(new AlternativeDef(ctx.expression(0), 
						"ExtCh-" + extChCtr + "-Alt-" + altCtr++));
				tmpExtCh.alternatives.add(new AlternativeDef(ctx.expression(1), 
						"ExtCh-" + extChCtr + "-Alt-" + altCtr));
			}
		}
	}

	@Override
	public void exitExprExtCh(ExprExtChContext ctx) {
		if (!(classifying)) {
			if (!(ctx.getParent() instanceof ExprExtChContext)) {
				tmpExtCh.setTranState(tState);
				setDef(ctx, tmpExtCh);
				//logger.trace("Embedded an external choice definition with " 
				//		+ tmpExtCh.alternatives.size() + " alternatives.");
				//for (AlternativeDef ad : tmpExtCh.alternatives) {
				//	logger.trace("alt: " + ad.ctx().getText() 
				//			+ " requires proxy chan? " + ad.requiresProxyChannel
				//			+ " with rest action: " + ad.rest.getText());
				//}
			}
		}
	}

	@Override
	public void enterExprException(ExprExceptionContext ctx) {
		setWPD();	
	}

	@Override
	public void enterExprAlphaParallel(ExprAlphaParallelContext ctx) {
		walkedCompType = CompType.ALPHABETISED;	
		tmpParallel = true;
		setWPD();
	}

	@Override
	public void exitExprAlphaParallel(ExprAlphaParallelContext ctx) {
		tmpParallel = false;
	}

	@Override
	public void enterExprInterfaceParallel(ExprInterfaceParallelContext ctx) {
		walkedCompType = CompType.INTERFACE;
		tmpParallel = true;
		setWPD();
	}

	@Override 
	public void exitExprInterfaceParallel(ExprInterfaceParallelContext ctx) {
		tmpParallel = false;
	}

	@Override
	public void enterExprLinkedParallel(ExprLinkedParallelContext ctx) {
		walkedCompType = CompType.LINKED;
		tmpParallel = true;
		setWPD();
	}

	@Override
	public void exitExprLinkedParallel(ExprLinkedParallelContext ctx) {
		tmpParallel = false;
	}

	@Override
	public void enterExprInterleave(ExprInterleaveContext ctx) {
		walkedCompType = CompType.INTERLEAVED;
		tmpParallel = true;
		setWPD();
	}

	@Override
	public void exitExprInterleave(ExprInterleaveContext ctx) {
		tmpParallel = false;
	}

	@Override
	public void enterExprReplAlphaParallel(ExprReplAlphaParallelContext ctx) {
		walkedCompType = CompType.REPL_ALPHABETISED;
		tmpParallel = false;
		setWPD();
	}

	@Override
	public void exitExprReplAlphaParallel(ExprReplAlphaParallelContext ctx) {
		tmpParallel = false;
	}

	@Override
	public void enterExprReplInterfaceParallel(ExprReplInterfaceParallelContext ctx) {
		walkedCompType = CompType.REPL_INTERFACE;
		tmpParallel = true;
		setWPD();
	}

	@Override
	public void exitExprReplInterfaceParallel(ExprReplInterfaceParallelContext ctx) {
		tmpParallel = false;
	}
	
	@Override
	public void enterExprReplInterleave(ExprReplInterleaveContext ctx) {
		walkedCompType = CompType.REPL_INTERLEAVED;
		tmpParallel = true;
		setWPD();
	}

	@Override
	public void exitExprReplInterleave(ExprReplInterleaveContext ctx) {
		tmpParallel = false;
	}
		
	@Override
	public void enterExprReplExtCh(ExprReplExtChContext ctx) {
		setWPD();
	}

	@Override
	public void enterExprReplIntCh(ExprReplIntChContext ctx) {
		setWPD();	
	}

	@Override
	public void enterExprHide(ExprHideContext ctx) {
		setWPD();	
	}


	@Override
	public void enterBuiltInProcess(BuiltInProcessContext ctx) {
		setWPD();	
	}

	@Override
	public void exitPExprUserFuncCall(PExprUserFuncCallContext ctx) {
		ParseTree t = (ParseTree) ctx.getParent().getParent();
		if (t instanceof PatternDeclContext) {
			setWPD();
//			logger.trace("Found solitary name with param on RHS of patternDecl.");
		}
	}

	@Override
	public void exitSetEnumerated(SetEnumeratedContext ctx) {
		ParseTree t = (ParseTree) ctx.getParent().getParent();
		if (!(t instanceof ExprInterfaceParallelContext)) { return; }
// 		logger.trace("Found enumerated set in the middle of a gen parallel expr.");
	}


	@Override
	public void enterExprInput(ExprInputContext ctx) {
		if (classifying) { setWPD(); } 
	}

	@Override 
	public void exitExprInput(ExprInputContext ctx) {
		if (classifying) { return; }
	}

	@Override
	public void enterExprOutput(ExprOutputContext ctx) {
		if (classifying) { setWPD(); }
	}

	@Override
	public void exitExprOutput(ExprOutputContext ctx) {
		if (classifying) { return; }
	}

	@Override
	public void enterExprDotted(ExprDottedContext ctx) {
		if (classifying) {
			setWPD();
			return;
		}
	}

	@Override 
	public void exitExprDotted(ExprDottedContext ctx) {
		if (classifying) { return; }
	}

	@Override
	public void exitPExprName(PExprNameContext ctx) {
		if (classifying) {
			ParseTree gpt = (ParseTree) ctx.getParent().getParent();
			if (gpt instanceof PatternDeclContext) {
				logger.trace("Found solitary name on RHS of patternDecl.");
				setWPD();
			}
			return;
		} 
		if (tmpInput) { // We need the contexts to bind new variable definitions to
			tmpChanExprs.add(ctx);
			return;
		}
		if (tmpParallel) {
			return;
		}

		// 2. The name of a set or a sequence, which we'll need to
		// 	store a reference to (because the referred-to item
		// 	probably needs to be declared locally).
		String name = ctx.Name().getText();
		//logger.trace(name);	
		Symbol sym = 
			tState.symTable.currentScope().resolve(name);
		if (sym != null && sym.def() != null) {
			if (sym.def() instanceof SetDef) {
				SetDef sd = (SetDef) sym.def();
				walkedSetRefs.put(name, sd);	
			} else if (sym.def() instanceof ListDef) {
				ListDef ld = (ListDef) sym.def();
				walkedListRefs.put(name, ld);
			} else if (sym.def() instanceof ConstIntDef
					|| sym.def() instanceof ConstStringDef
					|| sym.def() instanceof ConstCharDef) {
				//logger.trace("found a const ref: " + name);
				walkedConstRefs.put(name, sym.def());
			} else if (sym.def() instanceof ParamDef) {
				// do nothing, but prevent a param
				// from being recognised as a basedef
				// (which will result in a duplication)
			} else if (sym.def() instanceof ExtBaseDef) {
				ExtBaseDef ebd = (ExtBaseDef) sym.def();
				tmpExtRefs.put(name, ebd);
			}
		}
	}
}
