/**
 * FunctionDef
 */
package io.github.jamesdibley.cspider.translator.def;

import java.util.ArrayList;
import java.util.List;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import io.github.jamesdibley.cspider.parser.gen.CSPMParser.ParametricPatternDeclContext;
import io.github.jamesdibley.cspider.translator.BaseDef;
import io.github.jamesdibley.cspider.translator.def.VarBaseDef;
import io.github.jamesdibley.cspider.translator.symbol.Scope;

public class FunctionDef extends BaseDef {
	private List<ParamDef> params; 
	public Scope scope;
	private Scope containedScope;
	private VarBaseDef returnType;
	private Map<String, BaseDef> constRefs;
	private Map<String, SetDef> setRefs;
	private Map<String, ListDef> listRefs;
	private Map<String, ExtBaseDef> extRefs;
	private boolean simpleExpr = false;

	public FunctionDef(ParametricPatternDeclContext ctx, Scope scope, String name) {
		super(ctx, name);
		this.scope = scope;
		params = new ArrayList<ParamDef>();
		constRefs = new LinkedHashMap<String, BaseDef>();
		setRefs = new LinkedHashMap<String, SetDef>();
		listRefs = new LinkedHashMap<String, ListDef>();
		extRefs = new LinkedHashMap<String, ExtBaseDef>();
	}

	public void setContainedScope(Scope scope) {
		containedScope = scope;
	}

	public Scope containedScope() {
		return containedScope;
	}

	public List<ParamDef> params() {
		return params;
	}

	public void addParam(ParamDef pd) {
		params.add(pd);
	}

	public boolean isSimpleExpr() {
		return simpleExpr;
	}

	public void setSimpleExpr() {
		simpleExpr = true;
	}

	public void setReturnType(VarBaseDef vbd) {
		returnType = vbd;
	}

	public VarBaseDef returnType() {
		return returnType;
	}

	public void addSetRef(SetDef sd) {
		setRefs.put(sd.name(), sd);
		for (ExtBaseDef ebd : sd.extRefs()) {
			extRefs.put(ebd.name(), ebd);
		}
	}

	public Set<String> setRefKeys() {
		return setRefs.keySet();
	}

	public SetDef setRef(String s) {
		return setRefs.get(s);
	}

	public void addListRef(ListDef ld) {
		listRefs.put(ld.name(), ld);
	}

	public Set<String> listRefKeys() {
		return listRefs.keySet();
	}

	public ListDef listRef(String s) {
		return listRefs.get(s);
	}

	public void addExtRef(ExtBaseDef ebd) {
		extRefs.put(ebd.name(), ebd);
	}

	public Set<String> extRefKeys() {
		return extRefs.keySet();
	}

	public ExtBaseDef extRef(String s) {
		return extRefs.get(s);
	}

	public Set<String> constRefKeys() {
		return constRefs.keySet();
	}

	public BaseDef constRef(String refKey) {
		return constRefs.get(refKey);
	}

	public void addConstRef(BaseDef bd) {
		constRefs.put(bd.name(), bd);
	}

	public String toString() {
		return defStateToString() 
			+ "[" + returnType + "]" 
				+ params
				+ setRefs
				+ listRefs
				+ extRefs;
	}

}
