/**
 * ChannelDirection
 */
package io.github.jamesdibley.cspider.translator.def;

public enum ChannelDirection {
	UNKNOWN,
	INPUT,
	OUTPUT
}
