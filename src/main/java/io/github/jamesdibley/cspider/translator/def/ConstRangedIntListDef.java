/**
 * ConstRangedIntListDef
 */
package io.github.jamesdibley.cspider.translator.def;

import io.github.jamesdibley.cspider.parser.gen.CSPMParser.PatternDeclContext;
import io.github.jamesdibley.cspider.translator.BaseDef;
import io.github.jamesdibley.cspider.translator.ValueType;

public class ConstRangedIntListDef extends BaseDef {
	public String beginExpr;
	public String endExpr;
	public ValueType memberType;

	public ConstRangedIntListDef(PatternDeclContext ctx, String begin, String end, String name) {
		super(ctx, name);
		this.beginExpr = begin;
		this.endExpr = end;
		setType(ValueType.SET);
		this.memberType = ValueType.INT;
		this.markComplete();
	}

	public String toString() {
		return defStateToString() + "-> (" +
			beginExpr + ":" + endExpr + ")[" + memberType + "]";
	}

	public ValueType getMemberType() {
		return ValueType.INT;
	}
}
