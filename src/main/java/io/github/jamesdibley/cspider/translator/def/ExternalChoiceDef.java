/**
 * ExternalChoiceDef
 */
package io.github.jamesdibley.cspider.translator.def;

import java.util.ArrayList;
import java.util.List;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
import io.github.jamesdibley.cspider.parser.gen.*;
import io.github.jamesdibley.cspider.parser.gen.CSPMParser.*;
import io.github.jamesdibley.cspider.translator.BaseDef;

public class ExternalChoiceDef extends BaseDef {
	public List<AlternativeDef> alternatives;

	public ExternalChoiceDef(ParserRuleContext ctx, String name) {
		super(ctx, name);
		alternatives = new ArrayList<AlternativeDef>();
	}

	public String toString() {
		return name() + "alternatives: " + alternatives;
	}
}
