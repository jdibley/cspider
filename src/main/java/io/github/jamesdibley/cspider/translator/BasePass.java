/**
 * BasePass
 */

package io.github.jamesdibley.cspider.translator;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
import io.github.jamesdibley.cspider.parser.gen.*;
import io.github.jamesdibley.cspider.parser.gen.CSPMBaseListener;
import io.github.jamesdibley.cspider.parser.gen.CSPMParser.*;


public class BasePass extends CSPMBaseListener {
	protected TranState tState;
	protected ParseTreeWalker walker;
	protected CSPMParser parser;

	public BasePass(TranState tState, ParseTreeWalker walker, CSPMParser parser) {
		super();
		this.tState = tState;
		this.walker = walker;
		this.parser = parser;
	}

	public TranState state() {
		return tState;
	}

	public BaseDef def(ParseTree ctx) {
		return this.tState.nodeContextMap.get(ctx);
	}

	public void setDef(ParseTree ctx, BaseDef newDef) {
		if (this.tState.nodeContextMap != null) {
			this.tState.nodeContextMap.put(ctx, newDef);
		}
	}

}
