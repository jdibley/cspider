/**
 * Gen01
 */
package io.github.jamesdibley.cspider.translator.gen;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
import org.antlr.v4.runtime.tree.xpath.XPath.*;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.stringtemplate.v4.*;
import io.github.jamesdibley.cspider.parser.gen.*;
import io.github.jamesdibley.cspider.parser.gen.CSPMParser.*;
import io.github.jamesdibley.cspider.translator.BaseDef;
import io.github.jamesdibley.cspider.translator.BaseGen;
import io.github.jamesdibley.cspider.translator.TranState;
import io.github.jamesdibley.cspider.translator.ValueType;
import io.github.jamesdibley.cspider.translator.def.*;
import io.github.jamesdibley.cspider.translator.go.OutputModel;
import io.github.jamesdibley.cspider.translator.go.ProcessNetwork;

public class Gen01 extends ProcFuncCommon {
	private static final Logger logger = LogManager.getLogger(Gen01.class);
	private String pRoot = "/sourcefile/declaration";
	private OutputModel o = null;

	public Gen01(TranState tState, ParseTreeWalker walker,
			CSPMParser parser) {
		super(tState, walker, parser);
		o = tState.output;
		logger.trace("Set up constants, channels and parameters on process network.");
	}

	public void process(SourcefileContext tree) {
		for (ExtBaseDef ebd : o.extBaseDefMap.values()) {
			addExtParam(ebd);
		}
		for (ParamDef prd : o.exportedParamMap.values()) {
			addExportedParam(prd);
		}
		for (ConstCharDef ccd : o.constCharDefMap.values()) {
			addConstChar(ccd);
		}
		for (ConstIntDef cid : o.constIntDefMap.values()) {
			//logger.trace("addConstInt: " + cid.name());
			addConstInt(cid);
		}
		for (ConstStringDef csd : o.constStringDefMap.values()) {
			addConstString(csd);
		}
		for (SetDef sd : o.setDefMap.values()) {
			addSet(sd);
		}
		for (ChannelDef cd : o.chanDefMap.values()) {
			addChannel(cd);
		}
	}

	private void addExtParam(ExtBaseDef ebd) {
		ProcessNetwork n = o.procNet;
		n.addParam(ebd.name(), o.getGoType(ebd.type()));
	}

	private void addExportedParam(ParamDef prd) {
		ProcessNetwork n = o.procNet;
		n.addParam(prd.name(), o.getGoType(prd.type()));
	}

	private void addConstChar(ConstCharDef c) {
		ProcessNetwork n = o.procNet;
		n.addStVar(c.name(), o.getGoType(c.type()), c.value);
	}
	
	private void addConstInt(ConstIntDef c) {
		ProcessNetwork n = o.procNet;
		n.addStVar(c.name(), o.getGoType(c.type()), c.value);
		scanningFunction();
		for (FunctionDef fd : c.funcRefs()) {
			logger.trace("addConstInt: " + c.name() + " is setting up function: " + fd.name());
			setupFunction(fd, null);
		}
		resetScanningFunction();
	}
	
	private void addConstString(ConstStringDef c) {
		ProcessNetwork n = o.procNet;
		n.addStVar(c.name(), o.getGoType(c.type()), c.value);
	}

	private void addSet(SetDef sd) {
		ProcessNetwork n = o.procNet;
		if (sd.memberType() == ValueType.EVENT) { 
			return; 
		}
		//logger.trace("So what is the memberType?" + sd.name() + sd.memberType()); 
		ST stSetInitExpr = o.getTemplate("cspNewIST");
		switch (sd.type()) {
		case RANGED_SET:
			ST stInitBlock = o.getTemplate("block");
			ST stAddRange = o.getTemplate("cspISTAddRange");
			stAddRange.add("o", sd.name());
			stAddRange.add("e", sd.member(0));
			stAddRange.add("e", sd.member(1));
			stInitBlock.add("line", stSetInitExpr);
			stInitBlock.add("line", stAddRange);
			n.addStVar(sd.name(), o.getTemplate("cspISTType"), 
					stInitBlock.render());
			break;
		case SET:
			//stSetInit.add("e", stSetInitExpr); 
			for (String s : sd.members()) {
				stSetInitExpr.add("e", s);
			}
			n.addStVar(sd.name(), o.getTemplate("cspISTType"), 
					stSetInitExpr.render());
			break;
		}
	}

	private void addList(ListDef ld) {
		ProcessNetwork n = o.procNet;
		ST stListInitExpr = o.getTemplate("cspNewISQ");
		switch(ld.type()) {
		case RANGED_LIST:
			ST stInitBlock = o.getTemplate("block");
			ST stAddRange = o.getTemplate("cspISQAddRange");
			stAddRange.add("o", ld.name());
			stAddRange.add("e", ld.element(0));
			stAddRange.add("e", ld.element(1));
			stInitBlock.add("line", stListInitExpr);
			stInitBlock.add("line", stAddRange);
			n.addStVar(ld.name(), 
					o.getTemplate("cspISQType"), 
					stListInitExpr.render());
			break;
		case LIST:
			for (String s : ld.elements()) {
				stListInitExpr.add("e", s);
			}
			n.addStVar(ld.name(), 
					o.getTemplate("cspISQType"), 
					stListInitExpr.render());
			break;
		}

	}

	private void addChannel(ChannelDef cd) {
		//logger.trace(cd.name() + " has visibility: " + cd.visibility());
		switch (cd.visibility()) {
			case CLIENT:
				addClientChannel(cd);
				break;
			case NETWORK:
				addNetworkChannel(cd);
				break;
			case OBJECT:
				// Those only appear in process objects and
				// the corresponding process literals
				break;
		}
	}

	private void addClientChannel(ChannelDef cd) {
		String exportName = 
			cd.name().substring(0,1).toUpperCase()
			+ cd.name().substring(1);
		String renamedChanInit = null;
		// We have to do an odd little check to see if our client channel 
		// represents a renaming to a -network- channel. We do this over the
		// set of process invocations. 
		boolean addRenamedClientChannel = false;
		for (ProcInvocationDef pid : o.procInvocationDefMap.values()) {
			for (String s : pid.renamings().keySet()) {
				String str = pid.renaming(s);
				//logger.trace(str + " " + cd.name() );
				if (str.contains(cd.name())) {
					addRenamedClientChannel = true;
					renamedChanInit = s;
				}

			}
		}
		if (addRenamedClientChannel) {
			addRenamedClientChannel(cd, exportName, renamedChanInit);
		} else {
			addRegularClientChannel(cd, exportName);
		}
	}

	private void addRegularClientChannel(ChannelDef cd, String exportName) {
		ProcessNetwork n = o.procNet;
		ST type = null;
		SetDef sd = null; 
		String size = null;
		switch (cd.chanType()) {
		case EVENT:
			type = o.getTemplate("emptyStruct");
			n.addClientChannel(exportName, type.render());
			break;
		case DATA:
			sd = cd.getSpecField(0);
			type = o.getGoType(sd.memberType());
			n.addClientChannel(exportName, type.render());
			break;
		case MULTI_DATA:
			n.addClientChannel(exportName,
					cd.msgStructName());
			genCommStruct(cd);
			break;
		case INDEXED_EVENT:
			// TODO: support multidim chan idxes someday
			sd = cd.getSpecField(0);
			size = sd.member(1);
			type = o.getTemplate("emptyStruct");
			n.addClientChannelArray(exportName, type.render());
			break;
		case INDEXED_DATA:
			// TODO: support multidim chan idxes someday
			sd = cd.getSpecField(0);
			size = sd.member(1);
			type = o.getGoType(sd.memberType());
			n.addClientChannelArray(exportName, type.render());
			break;
		case INDEXED_MULTI_DATA:
			// TODO: support multidim chan idxes someday
			sd = cd.getSpecField(0);
			size = sd.member(1);
			n.addClientChannelArray(exportName, cd.msgStructName());
			genCommStruct(cd);
			break;
		}
	}

	private void addRenamedClientChannel(ChannelDef cd, String exportName,
			String renamedChan) {
		ProcessNetwork n = o.procNet;
		ST type = null;
		SetDef sd = null; 
		String size = null;
		List<String> renamedChanExprs = 
			new ArrayList<String>(Arrays.asList(renamedChan.split("\\.")));
		renamedChan = renamedChanExprs.remove(0);
		for (String s : renamedChanExprs) {
			renamedChan += "[" + s + "]";
		}

		// For now we only support this on -single- channels, not arrays
		switch (cd.chanType()) {
		case EVENT:
			type = o.getTemplate("emptyStruct");
			n.addRenamedClientChannel(exportName, type.render(), renamedChan);
			break;
		case DATA:
			sd = cd.getSpecField(0);
			type = o.getGoType(sd.memberType());
			n.addRenamedClientChannel(exportName, type.render(), renamedChan);
			break;
		case MULTI_DATA:
			n.addRenamedClientChannel(exportName,
					cd.msgStructName(), renamedChan);
			genCommStruct(cd);
			break;

		}
	}

	private void addNetworkChannel(ChannelDef cd) {
		ProcessNetwork n = o.procNet;
		ST type = null;
		SetDef sd = null;
		String size = null;
		switch (cd.chanType()) {
			case EVENT:
				type = o.getTemplate("emptyStruct");
				n.addNetworkChannel(cd.name(), type.render());
				break;
			case DATA:
				sd = cd.getSpecField(0);
				type = o.getGoType(sd.memberType());
				n.addNetworkChannel(cd.name(), type.render());
				break;
			case MULTI_DATA:
				n.addNetworkChannel(cd.name(),
						cd.msgStructName());
				genCommStruct(cd);
				break;
			case INDEXED_EVENT:
				// TODO: support multidim chan idxes someday
				sd = cd.getSpecField(0);
				//logger.trace("idxSig: " + cd.name() + " ... " + sd);
				size = sd.member(1);
				type = o.getTemplate("emptyStruct");
				n.addNetworkChannelArray(cd.name(), type.render(),
						size);
				break;
			case INDEXED_DATA:
				// TODO: support multidim chan idxes someday
				sd = cd.getSpecField(0);
				//logger.trace("idxData: " + cd.name() + " ... " + sd);
				size = sd.member(1);
				type = o.getGoType(sd.memberType());
				n.addNetworkChannelArray(cd.name(), type.render(),
						size);
				break;
			case INDEXED_MULTI_DATA:
				// TODO: support multidim chan idxes someday
				sd = cd.getSpecField(0);
				//logger.trace("idxMsg: " + cd.name() + " ... " + sd);
				size = sd.member(1);
				n.addNetworkChannelArray(cd.name(), cd.msgStructName(),
						size);
				genCommStruct(cd);
				break;
		}
	}

	// Generate the MsgStructs used for complex channels
	private void genCommStruct(ChannelDef c) {
		ProcessNetwork n = o.procNet;
		ST struct = o.getTemplate("strDecl");
		struct.add("n", c.msgStructName());
		MsgStructDef msd = c.msgStruct();
		//logger.trace(msd);
		for (VarBaseDef vbd : msd.fields()) {
			struct.add("f", vbd.name());
			struct.add("t", o.getGoType(vbd.type()));
		}
		n.addCommStruct(struct);
	}


}
