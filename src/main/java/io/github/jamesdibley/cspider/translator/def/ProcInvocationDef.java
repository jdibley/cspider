/**
 * ProcInvocationDef
 */
package io.github.jamesdibley.cspider.translator.def;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import io.github.jamesdibley.cspider.parser.gen.*;
import io.github.jamesdibley.cspider.parser.gen.CSPMParser.*;
import io.github.jamesdibley.cspider.translator.BaseDef;


public class ProcInvocationDef extends BaseDef {
	private static final Logger logger =
		LogManager.getLogger(ProcInvocationDef.class);
	private ProcessDef proc;
	private List<String> arguments;
	private List<ExpressionContext> argExprs;	
	private Map<String, String> renamings;
	private Map<ExpressionContext, ExpressionContext> renamingExprs;
	private boolean substate = false;

	public ProcInvocationDef(ParserRuleContext ctx,
			ProcessDef proc,// ProcessDef parent, 
			List<String> arguments,
			List<ExpressionContext> argExprs,
			Map<String, String> renamings,
			Map<ExpressionContext, ExpressionContext> renamingExprs,
			boolean substate) {
		super(ctx, proc.name());
		this.proc = proc;
		this.arguments = new ArrayList<String>(arguments);
		this.argExprs = argExprs;
		this.substate = substate;
		this.renamingExprs = renamingExprs;
		if (renamings != null) {
			this.renamings = new LinkedHashMap<String, String>(renamings);
		} else {
			this.renamings = new LinkedHashMap<String, String>();
		}
		if (substate) {
			logger.trace("registered piDef for substate: " 
					+ proc.name() + " with args: " + arguments
					+ " and renamings: " + renamings);
		} else {
			logger.trace("registered piDef for global proc: " 
					+ proc.name() + " with args: " + arguments
					+ " and renamings: " + renamings);
		}
	}

	public ProcessDef proc() {
		return proc;
	}

	public List<String> arguments() {
		return arguments;
	}

	public String argument(int i) {
		return arguments.get(i);
	}

	public List<ExpressionContext> argExprs() {
		return argExprs;
	}

	public ExpressionContext argExprs(int i) {
		return argExprs.get(i);
	}

	public boolean substate() {
		return substate;
	}

	public void addRenaming(String str0, String str1) {
		renamings.put(str0, str1);
	}

	public String renaming(String str) {
		return renamings.get(str);
	}

	public Map<String, String> renamings() {
		return renamings;
	}

	public ExpressionContext renamingExpr(ExpressionContext ec) {
		return renamingExprs.get(ec);
	}

	public Map<ExpressionContext, ExpressionContext> renamingExprs() {
		return renamingExprs;
	}

	public String toString() {
		switch (proc.compType()) {
		case UNKNOWN:
		case NOT_COMP:
			return "invocation of: " 
			+ "proc: " + proc.name() 
			+ " args " + arguments 
			+ " renamings " + renamings 
			+ " extRefs " + proc.extRefKeys() 
			+ " chanRefs " + proc.chanRefKeys() 
			+ " indexedChanRefs " + proc.indexedChanRefKeys() 
			+ " LDEchanRefs " + proc.LDEchanRefKeys() 
			+ " indexedLDEchanRefs " + proc.LDEindexedChanRefKeys() 
			+ " constRefs " + proc.constRefKeys() 
			+ " setRefs " + proc.setRefKeys() 
			+ " listRefs " + proc.listRefKeys() 
			+ " called functions " + proc.funcRefKeys() 
			;
		default:
			return "invocation of: " 
			+ "proc: " + proc.name()  
			+ " args " + arguments 
			+ " renamings " + renamings 
			+ " extRefs " + proc.extRefKeys() 
			+ " chanRefs " + proc.chanRefKeys() 
			+ " indexedChanRefs " + proc.indexedChanRefKeys()
			+ " constRefs " + proc.constRefKeys() 
			+ " setRefs " + proc.setRefKeys() 
			+ " listRefs " + proc.listRefKeys() 
			+ " called functions " + proc.funcRefKeys()
			; 
		}
	}
}
