/**
 * Pass05
 * Gather data channel declarations
 */
package io.github.jamesdibley.cspider.translator.pass;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
import org.antlr.v4.runtime.tree.xpath.XPath;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import io.github.jamesdibley.cspider.parser.gen.*;
import io.github.jamesdibley.cspider.parser.gen.CSPMParser.*;
import io.github.jamesdibley.cspider.translator.BasePass;
import io.github.jamesdibley.cspider.translator.TranState;
import io.github.jamesdibley.cspider.translator.def.ChannelDef;
import io.github.jamesdibley.cspider.translator.def.ChannelType;
import io.github.jamesdibley.cspider.translator.def.ExtBaseDef;
import io.github.jamesdibley.cspider.translator.def.SetDef;
import io.github.jamesdibley.cspider.translator.def.ext.*;
import io.github.jamesdibley.cspider.translator.symbol.*;

public class Pass05 extends BasePass {
	private static final Logger logger = 
		LogManager.getLogger(Pass05.class);
	private String xpath = "/sourcefile/declaration/chanDeclWithSpec";
	private List<SetDef> walkedChanSpecSets;

	public Pass05(TranState tState, ParseTreeWalker walker, 
			CSPMParser parser) {
		super(tState, walker, parser);
		walkedChanSpecSets = new ArrayList<SetDef>();
		logger.trace("Gather chan decls with specifications");
	}

	public void process(SourcefileContext tree) {
		for (ParseTree t : 
				XPath.findAll((ParseTree) tree, xpath, parser)) {
			if (t instanceof ChanDeclWithSpecContext) { 
				ChanDeclWithSpecContext cd = 
					(ChanDeclWithSpecContext) t;
				ParseTree e = (ParseTree) cd.expression();
				walker.walk(this, e);
				registerChanDecl((ChanDeclWithSpecContext) t);
				walkedChanSpecSets.clear();
			}
		}
	}

	public void registerChanDecl(ChanDeclWithSpecContext ctx) {
		// A declaration can set up more than one channel 
		for (TerminalNode n : ctx.Name()) {
			// We -assume- 'DATA' although later it may be revised 
			// to 'INDEXED_EVENT', 'INDEXED_DATA' ...
			ChannelDef cDef = 
				new ChannelDef(ctx, n.getText(), ChannelType.DATA);
			cDef.setTranState(tState);
			setDef(n, cDef);
			tState.symTable.currentScope().define(cDef, cDef.name());
			for (SetDef s : walkedChanSpecSets) {
				cDef.addSpecField(s);
				s.addReferencingChan(cDef);
			}

			// If we walked more than one type field, we -assume- 
			// all type fields represent data (this may be revised 
			// later)
			if (walkedChanSpecSets.size() > 1) {
				cDef.setChannelType(ChannelType.MULTI_DATA);
			}
		}
	}

	// Overridden rules for Listener on this pass:
	@Override
	public void exitSet(SetContext ctx) {
		SetDef sDef = (SetDef) def(ctx);
		walkedChanSpecSets.add(sDef);
	}

	@Override
	public void exitSetRangedInt(SetRangedIntContext ctx) {
		SetDef sDef = (SetDef) def(ctx);
		walkedChanSpecSets.add(sDef);
	}

	@Override
	public void exitPExprBuiltInIdentifier(PExprBuiltInIdentifierContext ctx) {
		String name = ctx.getText();
		Symbol sym = tState.symTable.currentScope().resolve(name);
		if (sym == null) {
			logger.trace("Found unknown built-in identifier "
					+ "in chan spec: " + name);
			return;
		}
		if (!(sym.def() instanceof SetDef)) {
			logger.error("Found an identifier that doesn't "
					+ "resolve to a set def in chan spec.");
			return;
		}

		SetDef sDef = (SetDef) sym.def();
		walkedChanSpecSets.add(sDef);
	}

	@Override
	public void exitPExprName(PExprNameContext ctx) {
		String name = ctx.Name().getText();
		Symbol sym = tState.symTable.currentScope().resolve(name);
		if (sym == null) {
			logger.error("Found unknown identifier in chan spec: "
					+ name);
			return;
		}
		if (!(sym.def() instanceof SetDef)) {
			logger.error("Found an identifer that doesn't resolve "
					+ "to a set def in chan spec: "
					+ name);
			return;
		}
		SetDef sDef = (SetDef) sym.def();
		walkedChanSpecSets.add(sDef);
	}
}

