/**
 * BaseGen
 */

package io.github.jamesdibley.cspider.translator;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
import org.stringtemplate.v4.*;
import io.github.jamesdibley.cspider.parser.gen.*;
import io.github.jamesdibley.cspider.parser.gen.CSPMBaseListener;
import io.github.jamesdibley.cspider.parser.gen.CSPMParser.*;

public class BaseGen extends CSPMBaseListener {
	protected TranState tState;
	protected ParseTreeWalker walker;
	protected CSPMParser parser;
	protected STGroup templates;

	public BaseGen(TranState tState, ParseTreeWalker walker, CSPMParser parser) {
		super();
		this.tState = tState;
		this.walker = walker;
		this.parser = parser;
		templates = tState.output.templates;
		ST funcST = templates.getInstanceOf("func");
	}

	public TranState state() {
		return tState;
	}

	public BaseDef def(ParseTree ctx) {
		if (this.tState.nodeContextMap == null) {
			return null;
		}
		return this.tState.nodeContextMap.get(ctx);
	}	

	public void setST(ParseTree ctx, ST newST) {
		this.tState.output.STMap.put(ctx, newST);
	}

	public ST getST(ParseTree ctx) {
		return tState.output.STMap.get(ctx);
	}

	public ST getTemplate(String name) { 
		return templates.getInstanceOf(name);
	}
}
