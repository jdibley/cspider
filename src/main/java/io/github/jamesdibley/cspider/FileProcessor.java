package io.github.jamesdibley.cspider;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;


public class FileProcessor {
	private String sourceFilename;
	private String targetDirectory;
	static final Logger logger = LogManager.getLogger(FileProcessor.class.getName());

	public FileProcessor(String[] args) {
//		logger.trace("FileProcessor starting...");
		if (args.length == 0) {
			// TODO: print help
		}

		for (int idx = 0; idx < args.length; idx++) {
			switch (args[idx].trim().toLowerCase()) {
			case "-i":
				idx++;
				sourceFilename = args[idx];
				break;
			case "-o":
				idx++;
				targetDirectory = args[idx];
				break;
			case "-h":
			default:
				// TODO: print help
				break;
			}
		}
	}

	public boolean init() {
//		logger.traceEntry();
		if (sourceFilename == null) {
			return false;
		}

		if (targetDirectory == null) {
			return false;
		}

		File root = new File(sourceFilename);
		if (!root.exists()) {
			return false;
		}
		
//	a	logger.traceExit();
		return true;
	}

	public String sourceFilename() {
		return sourceFilename;
	}

	public String targetDirectory() {
		return targetDirectory;
	}

	public String loadData(File source) {
		String sourceData = "";
		try {
			sourceData = FileUtils.readFileToString(source, "utf-8"); 
		} catch (IOException e) {
			System.out.println("Reading data failed from '" + source + "' " + e);
		} 
		return sourceData;
	}

	public void storeData(File target, String data) {
		try {
			FileUtils.writeStringToFile(target, data, "utf-8");
		} catch (IOException e) {
			System.out.println("Error writing result data to '" + target + "' " + e);
		}
	}

}
