/**
 * CSPIDER.java
 */

package io.github.jamesdibley.cspider;

import java.io.File;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import io.github.jamesdibley.cspider.translator.Translator;

public class Cspider { 
	private static final Logger logger = LogManager.getLogger(Cspider.class);
	
	public static void main(String[] args) {
		new Cspider(args);
	}

	public Cspider(String[] args) {
		logger.trace("Cspider starting...");

		if (args == null || args.length == 0) {
			logger.fatal("No args supplied, aborting.");
			return;
		}

		FileProcessor processor = new FileProcessor(args);

		if (processor.init()) {
			File source = new File(processor.sourceFilename());
		
			String inputData = processor.loadData(source);
			Translator translator = new Translator(processor);
		
			translator.translate(inputData);
		//	String result = translator.translate(inputData);
		//	logger.trace(result);	
		//
		//processor.storeData(target, inputData+result);
		}

	}
}
