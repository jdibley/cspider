grammar CSPM ;

@lexer::header {
	package io.github.jamesdibley.cspider.parser.gen;
}

@parser::header {
	package io.github.jamesdibley.cspider.parser.gen;
}

sourcefile
	: declaration* EOF;

declaration
	: assertionDecl 
	| chanDecl 
	| chanDeclWithSpec
	| datatypeDecl
	| extPatternDecl
	| nametypeDecl
	| patternDecl	
	| parametricPatternDecl
	| subtypeDecl
	| transparentDecl
	;

// Assertions -shouldn't- be present in the implementation file, but we 
// don't assume their absence.
assertionDecl
	: Assert (Not)? // assertions can be negated
	 	assertion
	;

assertion
	: proc RefTr proc
		# assRefTrace
	| proc RefFa proc 
		# assRefFailures
	| proc RefFd proc
		# assRefFailDivs
	| proc ':[' Deadlock '-'? Free FModel ']' 
		# assDLFFailures
	| proc ':[' Deadlock '-'? Free (FDModel)? ']' 
		# assDLFFailDivs
	| proc ':[' Determ FModel ']' 
		# assDetermFailures
	| proc ':[' Determ (FDModel)? ']' 
		# assDetermFailDivs
	| proc ':[' Divergence '-'? Free (FDModel)? ']' 
		# assDivFree
	| proc ':[' Has Trace FModel ']:' '<' expressionList '>' 
		# assHTrFailures
	| proc ':[' Has Trace (FDModel)? ']:' '<' expressionList '>' 
		# assHTrFailDivs
	| proc ':[' Has Trace TModel ']:' '<' expressionList '>' 
		# assHTrTraces
	| expression
		# assExpr
	;

proc	
	: builtInProcess 
	| expression 
	;

chanDecl
	: Channel Name (',' Name)* ;

chanDeclWithSpec
	: Channel Name (',' Name)* ':' expression ;

extPatternDecl
	: ExtPatternAnnotation Name '::' expression ;

patternDecl
	: patternLHS '=' letClause? expression ;

parametricPatternDecl
	: Name '::' ('(' .*? ')' '=>')? '(' expressionList ')' '->' expression 
		patternLHS '=' letClause? expression
	; 

letClause
	: Let 
	(patternDecl | parametricPatternDecl)+ 
	  Within 
	;

patternLHS
	: dottedPattern
	| namePattern
	| setPattern
	| seqPattern
	| tuplePattern
	;

dottedPattern 
	: Name ('.' Name)+ ;

namePattern
	: namePattern '(' ')'
		# npEmpty
	| namePattern '(' expression (',' expression)* ')'
		# npArgs
	| namePattern '(' expression '@@' expression ')' 
		# npDouble
	| Name
		# npName
	;

seqPattern
	: '<' Name (',' Name)* '>' ;

setPattern
	: '{' Name '}' ;

tuplePattern	
	: '(' Name (',' Name)* ')' ;

datatypeDecl
	: 'datatype' Name '=' datatypeClause ('|' datatypeClause)* ;
	
datatypeClause
	: Name ('.' expression)? ;

subtypeDecl // define subsets of a datatype 
	: 'subtype' Name '=' datatypeClause ('|' datatypeClause)* 
	;

nametypeDecl // associate name with set of values 
	: 'nametype' Name '=' expression 
	;

expression
	: '(' expression ')'		
		# exprParens
	| expression renamingClause
		# exprRename
	| expression '^' expression	
		# exprConcat
	| '#' expression		
		# exprLength
	| expression '*' expression	
		# exprMul
 	| expression '/' expression	
		# exprDiv
	| expression '%' expression	
		# exprMod
	| '-' expression		
		# exprNeg
	| expression '+' expression	
		# exprAdd
	| expression '-' expression	
		# exprSub
	| expression '==' expression 	
		# exprEq
	| expression '!=' expression	
		# exprNEq
	| expression '<' expression  	
		# exprLT
	| expression '<=' expression	
		# exprLTE
	| expression '>' expression	
		# exprGT
	| expression '>=' expression	
		# exprGTE
	| 'not' expression		
		# exprNot
	| expression 'and' expression	
		# exprAnd
	| expression 'or' expression	
		# exprOr
	| expression ':' primaryExpr 
		# exprType
	|<assoc=right>
	  expression '.' expression
	  	# exprDotted
	| expression '?' expression 
		# exprInput
	| expression '!' expression	
		# exprOutput
	|<assoc=right> 
	  expression '->' expression
	  	# exprPrefix
	|<assoc=right>
	  expression '&' expression	
	  	# exprGuarded
	| expression ';' expression 	
		# exprSeqComp
	| expression '[>' expression	
		# exprTimeout
	| expression '/\\' expression	
		# exprInterrupt
	| expression '[]' expression 
	  	# exprExtCh
	| expression '|~|' expression
		# exprIntCh
	| expression '[|' primaryExpr '|>' expression
		# exprException
	| expression 
		'[' primaryExpr '||' primaryExpr ']' 
		expression 
		# exprAlphaParallel
	| expression 
		'[|' primaryExpr '|]' 
		expression 
		# exprInterfaceParallel
	| expression 
		'[' primaryExpr '<->' primaryExpr ']' 
		expression 
		# exprLinkedParallel
	| expression 
		'|||' 
		expression 
		# exprInterleave
	| expression '\\' primaryExpr 	
		# exprHide
	| '||' expressionList '@' '[' primaryExpr ']' expression 
		# exprReplAlphaParallel
	| '[]' expressionList '@' expression 
		# exprReplExtCh
	| '[|' primaryExpr '|]' expressionList '@' expression 
		# exprReplInterfaceParallel
	| '|||' expressionList '@' expression 
		# exprReplInterleave
	| '|~|' expressionList '@' expression
		# exprReplIntCh
	| If expression Then expression (Else expression)?
		# exprIf
	| primaryExpr			
		# exprPrimary
	;

renamingClause 
	: '[[' renaming (',' renaming)* ']]'
	| '[[' renaming (',' renaming)* '|' setGenerator (',' setGenerator)* ']]'
	;

renaming
	: expression '<-' expression ;

primaryExpr
	: goReservedFunc
		# pExprGoReservedFunc
	| goReservedKeyword
		# pExprGoReservedKeyword
	| builtInFunc 		
		# pExprBuiltInCall
	| builtInProcess
		# pExprBuiltInProcess
	| builtInIdentifier	
		# pExprBuiltInIdentifier
	| seqExpr 		
		# pExprSeqExpr
	| setExpr		
		# pExprSetExpr
	| Name '(' expressionList ')'
		# pExprUserFuncCall
	| Name '::' Name	
		# pExprModuleAccess
	| Name 			
		# pExprName
	| mapLit		
 		# pExprMapLit
	| tupleExpr		
		# pExprTupExpr
	| literal 		
		# pExprLiteral 
	;

goReservedFunc 
	: 'close' '(' expression ')'
	| 'len' '(' expression ')'
	| 'cap' '(' expression ')'
	| 'new' '(' expression ')'
	| 'make' '(' expression ')' 
	| 'append' '(' expression ')'
	| 'copy' '(' expression ')'
	| 'delete' '(' expression ')'
	| 'complex' '(' expression ')'
	| 'real' '(' expression ')'
	| 'imag' '(' expression ')'
	| 'panic' '(' expression ')'
	| 'recover' '(' expression ')'
	| 'protect' '(' expression ')'
	| 'println' '(' expression ')'
	;

goReservedKeyword
	: 'break' | 'case' | 'chan' | 'const' | 'continue'
	| 'default' | 'defer' | 'else' | 'fallthrough' | 'for'
	| 'func' | 'go' | 'goto' | 'if' | 'import'
	| 'interface' | 'map' | 'package' | 'range' | 'return'
	| 'select' | 'struct' | 'switch' | 'type' | 'var'
	;

expressionList
	: expression (',' expression)* ;

literal	
	: Int	
		# litInt
	| Char	
		# litChar
	| String
		# litString
	;

seqExpr 
	: '<' '>'
		# listEmpty 
	| '<' expression '..' expression '>'
		# listRangedInt 
	| '<' expression '..' '>'
		# listRangedIntInfinite // TODO: non-implementable! 
	| '<' expressionList '>'
		# list 
	| '<' expression '..' expression '|' seqStatements '>'
		# listRangedComp
	| '<' expression '..' '|' seqStatements '>'
		# listRangedCompInfinite
	| '<' expressionList '|' seqStatements '>'
		# listComp
	| Concat '(' primaryExpr ')' 
		# listConcat
	| Tail '(' primaryExpr ')' 
		# listTail
	| Seq '(' primaryExpr ')' 
		# listFromSet
	;

seqStatements 
	: seqStatement (',' seqStatement)* ;

seqStatement 
	: seqGenerator 
	| seqPredicate
	;

seqGenerator
	: Name '<-' primaryExpr
		# seqGenSingle
	| tupleExpr '<-' primaryExpr 
		# seqGenTuple
	;

seqPredicate
	: expression ;

setExpr 
	: '{' '}'
		# setEmpty
	| '{' expression '..' expression '}' 
		# setRangedInt
	| '{' expression '..' '}'
		# setRangedIntInfinite
	| '{' expressionList '}'
		# set
	| '{' expression '..' expression '|' setStatements '}'
		# setRangedComp
	| '{' expressionList '|' setStatements '}'
		# setComp
	| '{|' expressionList '|}'
		# setEnumerated
	| '{|' expressionList '|' setStatements '|}'
		# setEnumeratedComp
	| Diff '(' primaryExpr ',' primaryExpr ')'
		# setDiff
	| DistInter '(' primaryExpr ')' 
		# setDistInter
	| DistUnion '(' primaryExpr ')'
		 # setDistUnion
	| Inter '(' primaryExpr ',' primaryExpr ')'
		# setInter
	| Union '(' primaryExpr ',' primaryExpr ')'
		# setUnion
	| Powerset '(' primaryExpr ')'
		# setPowerset
	| Set '(' primaryExpr ')' 
		# setFromList
	;

setStatements
	: setStatement (',' setStatement)* ;

setStatement 
	: setGenerator
	| setPredicate
	;

setGenerator 	
	: Name '<-' primaryExpr
	| tupleExpr '<-' primaryExpr 
	;

setPredicate
	: expression;

mapLit 	
	: '(|' 
	(expression '=>' expression
	(',' expression '=>' expression)*)?
	'|)'
	;

tupleExpr
	: '(' expressionList? ')'
	;

builtInFunc // All these functions take compulsory arguments 
	: Card '(' primaryExpr ')'  
		# setCard
	| Chaos '(' primaryExpr ')' 
		# cspChaosProcess
	| Elem '(' primaryExpr ',' primaryExpr ')'
		# seqElemTest
	| Empty '(' primaryExpr ')'
		# setEmptyTest
	| Error '(' String ')'
		# logError
	| Head '(' primaryExpr ')'
		# seqHead
	| Length '(' primaryExpr ')'
		# seqLen
	| Member '(' primaryExpr ',' primaryExpr ')'
		# setMemberTest
	| Null '(' primaryExpr ')'
		# seqNullTest 
	| Run '(' primaryExpr ')'
		# cspRunProcess
	| SeqSet '(' primaryExpr ')'
		# setSeqOverSetInf
	;

builtInIdentifier
	: BoolSet
	| CharSet
	| EventSet
	| Events
	| False
	| IntSet
	| Proc
	| True
	| Wildcard
	;

builtInProcess
	: Stop
	| Skip
	| Diverge
	;

transparentDecl
	: Transparent expressionList ;

//
// Lexer rules 
// 
ExtPatternAnnotation	
		: '--#' ;
Comment1	: '--' ~'#' .*? '\r'? '\n' -> skip ;
Comment2	: '{-' .*? '-}' -> skip ;

Stop		: 'STOP' ;
Skip		: 'SKIP' ;
Diverge		: 'DIV' ;
Run		: 'RUN' ; 	// nb. 'RUN' is a function
Chaos		: 'CHAOS' ; 	// nb. CHAOS is a function

RefTr		: '[T=' ;
RefFa		: '[F=' ;
RefFd		: '[FD=' ;

// TODO: Test application patterns for these options
PartOrdRedOpt	: ':[' Partial .*? ']' ;
TauPriorityOpt	: ':[' Tau .*? ']' '{' .*? '}' ;

Deadlock	: 'deadlock' ;
Determ		: 'deterministic' ;
Divergence	: 'divergence' ;
Has		: 'has' ;
Trace		: 'trace' ;
Free		: 'free' ;
FModel 		: '[F]' ;
TModel 		: '[T]' ;
FDModel		: '[FD]' ;
Partial		: 'partial' ;
Tau		: 'tau' ;

And		: 'and' ;
Assert		: 'assert' ;
BoolSet		: 'Bool' ;
Card		: 'card' ;
Channel		: 'channel' ;
CharSet		: 'Char' ;
Concat		: 'concat' ;
Datatype	: 'datatype' ;
Diff		: 'diff' ; // diff(a1, a2): a1 - a2
DistInter	: 'Inter' ;
DistUnion	: 'Union' ;
Elem		: 'elem' ;
Else		: 'else' ;
Empty		: 'empty' ;
Endmodule	: 'endmodule' ;
Error		: 'error' ;
EventSet	: 'Event' ;
Events		: 'Events' ;
Exports		: 'exports' ;
External	: 'external' ;
False		: 'False' | 'false' ;
Head		: 'head' ;
If		: 'if' ;
Include		: 'include' ;
Instance	: 'instance' ;
IntSet		: 'Int' ;
Inter		: 'inter' ; // inter(a1, a2): a1 /\ a2
Length		: 'length' ;
Let		: 'let' ;
Member		: 'member' ;
Module		: 'module' ;
Nametype	: 'nametype' ;
Not		: 'not' ;
Null		: 'null' ;
Or		: 'or' ;
Powerset	: 'Set' ; // Set(a): all subsets of a
Print		: 'print' ;
Proc		: 'Proc' ;
SeqSet		: 'Seq' ; // Seq(a): set of sequences over a set a
			  // "(infinite if a is not empty)"
Seq		: 'seq' ; // seq(a) convert a set to a sequence 
			  // "(in an arbitrary order)"
Set		: 'set' ; // set(s) convert a sequence s into a set
Tail		: 'tail' ;
Then		: 'then' ;
Timed		: 'Timed' ;
Transparent	: 'transparent' ;
True		: 'True' | 'true' ;
Type		: 'type' ;
Union		: 'union' ; // union(a1, a2): a1 \/ a2
Within		: 'within' ;

String		: '"' ( '\\"' | . )*? '"' ;
Char		: '\'' (AlphaNum | '\\\'' ) '\'' ;
Wildcard	: '_' ;
// 'Identifiers with a trailing underscore (such as 'f_') are 
//  	reserved for machine generated-code.' 
// https://www.cs.ox.ac.uk/projects/fdr/manual/cspm/syntax.html#syntax_Variable
//
ReservedName	: Alpha (AlphaNum | UScore | Primes)* UScore ;

// 'These must begin with an alphabetic character and are followed by any 
// number of alphanumeric characters or underscores, optionally
// followed by any number of prime characters (').' 
Name		: Alpha (AlphaNum | UScore )* Primes? ;
Int		: [0-9]+ ;
fragment Alpha	: [a-zA-Z] ;
fragment AlphaNum
		: [0-9a-zA-Z] ;
fragment NonAlphaNum
		: ~[0-9a-zA-Z] ;
fragment Primes	: '\''+ ; 
fragment UScore	: '_' ;
WS		: (' '|'\t') -> skip ;
NL 		: '\r'? '\n' -> skip ;
