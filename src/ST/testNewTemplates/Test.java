import java.util.List;
import org.stringtemplate.v4.*;

public class Test {
	
	public static void main(String[] args) {

		STGroup st = new STGroupFile("../cspiderGo.stg");
		testCSPIST(st);
		//testCSPISQ(st);
		testExprs(st);
		testGoFundamentals(st);
//		testProcessObject(st);
//		testProcessNetwork(st);
		System.out.println("\n");
	}
	
	private static void out(String cap, String con) {
		System.out.println(cap + ":\t\t" + con);
	}

	private static void testExprs(STGroup st) {
		//STGroup st = new STGroupFile("../goTemplateGroups/expr.stg");
		System.out.println("\n>>>> testExprs <<<<");
		String rule = null;

		testBooleanExpr(st);
		testArithmeticExpr(st);
		testComparisonExpr(st);
	}

	private static void testGoFundamentals(STGroup st) {
		//STGroup st = new STGroupFile("../goTemplateGroups/goFundamentals.stg");
		System.out.println("\n>>>> testGoFundamentals <<<<");
		String rule = null;

		testOperations(st);
		testDeclarations(st);
		testConstants(st);
		testBuiltin(st);
		testMisc(st);
	}

	private static void testCSPIST(STGroup st) {
		//STGroup st0 = new STGroupFile("../goTemplateGroups/goFundamentals.stg");
		//STGroup st1 = new STGroupFile("../goTemplateGroups/cspiderIntSet.stg");
		ST st00 = null;
		ST st01 = null;
		ST st02 = null;
		System.out.println("\n>>>> testCSPIST <<<<");
		String rule = null; 

		rule = "rVarDecl";
		st00 = st.getInstanceOf(rule);
		st00.add("v", "testSet");

		rule = "cspISTType";
		st01 = st.getInstanceOf(rule);
		st00.add("t", st01);
		out(rule, st00.render());

		rule = "sVarDecl";
		st00 = st.getInstanceOf(rule);
		st00.add("v", "set");
		rule = "cspNewIST";
		st01 = st.getInstanceOf(rule);
		st00.add("e", st01);
		out(rule, st00.render());
		for (int i = 0; i < 5; i++) {
			st01.add("e", i);
		}
		out(rule, st00.render());

		rule = "pe";
		st00 = st.getInstanceOf(rule);
		st00.add("e", "testSet");

		rule = "cspISTAdd";
		st01 = st.getInstanceOf(rule);
		st01.add("o", st00);
		st01.add("e", "5");
		out(rule, st01.render());

		rule = "cspISTAddInt";
		st01 = st.getInstanceOf(rule);
		st01.add("o", st00);
		st01.add("e", "6");
		out(rule, st01.render());

		rule = "cspISTAddRange";
		st01 = st.getInstanceOf(rule);
		st01.add("o", st00);
		st01.add("e", "7");
		st01.add("e", "10");
		out(rule, st01.render());

		rule = "cspISTCard";
		st01 = st.getInstanceOf(rule);
		st01.add("o", st00);
		st01.add("c", "just checking that comments also work");
		out(rule, st01.render());

		rule = "cspISTDiff";
		st01 = st.getInstanceOf(rule);
		st01.add("o", st00);
		st02 = st.getInstanceOf("cspNewIST");
		for (int i = 0; i < 3; i++) {
			st02.add("e", i);
		}
		st01.add("e", st02);
		out(rule, st01.render());

		rule = "cspISTEmpty";
		st01 = st.getInstanceOf(rule);
		st01.add("o", st00);
		st01.add("c", "just checking");
		out(rule, st01.render());

		rule = "cspISTInter";
		st01 = st.getInstanceOf(rule);
		st01.add("o", st00);
		st02 = st.getInstanceOf("cspNewIST");
		for (int i = 4; i < 6; i++) {
			st02.add("e", i);
		}
		st01.add("e", st02);
		out(rule, st01.render());

		rule = "cspISTMember";
		st01 = st.getInstanceOf(rule);
		st01.add("o", st00);
		st01.add("e", "3");
		out(rule, st01.render());
		
		rule = "cspISTRemove";
		st01 = st.getInstanceOf(rule);
		st01.add("o", st00);
		st02 = st.getInstanceOf("cspNewIST");
		for (int i = 7; i < 9; i++) {
			st02.add("e", i);
		}
		st01.add("e", st02);
		out(rule, st01.render());

		rule = "cspISTSeq";
		st01 = st.getInstanceOf(rule);
		st01.add("o", st00);
		out(rule, st01.render());
	}

	private static void testCSPISQ(STGroup st) {
		//STGroup st0 = new STGroupFile("../goTemplateGroups/goFundamentals.stg");
		//STGroup st1 = new STGroupFile("../goTemplateGroups/cspiderIntSeq.stg");
		ST st00 = null;
		ST st01 = null;
		String rule = null; 
		System.out.println("\n>>>> testCSPISQ <<<<");

		rule = "rVarDecl";
		st00 = st.getInstanceOf(rule);
		st00.add("v", "testSeq");

		rule = "cspISQType";
		st01 = st.getInstanceOf(rule);
		st00.add("t", st01);
		out(rule, st00.render());

		rule = "sVarDecl";
		st00 = st.getInstanceOf(rule);
		st00.add("v", "seq");
		rule = "cspNewISQ";
		st01 = st.getInstanceOf(rule);
		st00.add("e", st01);
		out(rule, st00.render());
		for (int i = 0; i < 5; i++) {
			st01.add("e", i);
		}
		out(rule, st00.render());

		rule = "pe";
		st00 = st.getInstanceOf(rule);
		st00.add("e", "testSeq");

		rule = "cspISQAddBack";
		st01 = st.getInstanceOf(rule);
		st01.add("o", st00);
		ST st02 = st.getInstanceOf("cspNewISQ");
		st02.add("e", "1");
		st02.add("e", "2");
		st01.add("e", st02);
		out(rule, st01.render());

		rule = "cspISQAddFront";
		st01 = st.getInstanceOf(rule);
		st01.add("o", st00);
		st01.add("e", st02);
		st02.add("e", "3");
		st02.add("e", "4");
		out(rule, st01.render());
		
		rule = "cspISQAddIntBack";
		st01 = st.getInstanceOf(rule);
		st01.add("o", st00);
		st01.add("e", "5");
		out(rule, st01.render());

		rule = "cspISQAddIntFront";
		st01 = st.getInstanceOf(rule);
		st01.add("o", st00);
		st01.add("e", "6");
		out(rule, st01.render());

		rule = "cspISQAddIntBack";
		st01 = st.getInstanceOf(rule);
		st01.add("o", st00);
		st01.add("e", "7");
		out(rule, st01.render());

		rule = "cspISQAddIntFront";
		st01 = st.getInstanceOf(rule);
		st01.add("o", st00);
		st01.add("e", "8");
		out(rule, st01.render());
	
		rule = "cspISQAddRangeBack";
		st01 = st.getInstanceOf(rule);
		st01.add("o", st00);
		st01.add("e", "5");
		out(rule, st01.render());

		rule = "cspISQAddRangeFront";
		st01 = st.getInstanceOf(rule);
		st01.add("o", st00);
		st01.add("e", "5");
		out(rule, st01.render());

		rule = "cspISQElem";
		st01 = st.getInstanceOf(rule);
		st01.add("o", st00);
		st01.add("e", "5");
		st01.add("c", "how about a comment");
		out(rule, st01.render());

		rule = "cspISQElements";
		st01 = st.getInstanceOf(rule);
		st01.add("o", st00);
		out(rule, st01.render());

		rule = "cspISQHead";
		st01 = st.getInstanceOf(rule);
		st01.add("o", st00);
		st01.add("c", "'We were speaking of belief...'");
		out(rule, st01.render());

		rule = "cspISQLength";
		st01 = st.getInstanceOf(rule);
		st01.add("o", st00);
		out(rule, st01.render());

		rule = "cspISQNull";
		st01 = st.getInstanceOf(rule);
		st01.add("o", st00);
		out(rule, st01.render());

		rule = "cspISQRemove";
		st01 = st.getInstanceOf(rule);
		st01.add("o", st00);
		st01.add("e", st02);
		out(rule, st01.render());

		rule = "cspISQSet";
		st01 = st.getInstanceOf(rule);
		st01.add("o", st00);
		out(rule, st01.render());

		rule = "cspISQTail";
		st01 = st.getInstanceOf(rule);
		st01.add("o", st00);
		st02 = st.getInstanceOf(rule);
		st02.add("o", st01);
		out(rule, st01.render());
		out(rule, st02.render());
		st02 = st.getInstanceOf("cspISQHead");
		st02.add("o", st01);
		out(rule, st02.render());
}

	private static void testProcessObject(STGroup st) {
		//STGroup st = new STGroupFile("../goTemplateGroups/processObject.stg");
		System.out.println("\n>>>> testProcessObject <<<<");
		String rule = null;
		
		rule = "processObject";
		ST st00 = st.getInstanceOf(rule);
		String packName = "transRA";
		String procName = "rxreq";
		String procPfx = procName.toUpperCase();
		String procInitial = procName.substring(0,1);

		st00.add("pkn", packName);
		st00.add("n", procName);
		st00.add("procPfx", procPfx);
		st00.add("nI", procInitial);

		String procLabel01 = "RX";
		st00.add("procLabel", procLabel01);
		st00.add("methodName", procLabel01.toLowerCase());

		ST st01 = st.getInstanceOf("func");
		st01.add("rxAbbr", procName.toLowerCase().substring(0,1));
		st01.add("rxName", procName.toLowerCase());
		st01.add("n", procLabel01.toLowerCase());
		st01.add("t", st.getInstanceOf("int"));
		st00.add("procStMethods", st01);
		st01.add("line", st.getInstanceOf("comment").add("c","who know what goes in here?"));	

		st00.add("constantName", "allNodes");
		st00.add("constantType", st.getInstanceOf("cspISTType"));
		st00.add("stateVarName", "responsesAnticipated");
		st00.add("stateVarType", st.getInstanceOf("int"));

		out(rule, "\n" + st00.render());
	}

	private static void testProcessNetwork(STGroup st) {
		//STGroup st = new STGroupFile("../goTemplateGroups/cspiderGo.stg");
		System.out.println("\n>>>> testProcessNetwork <<<<");
		String rule = null;
		ST st00 = null;

		String procName = "Primesieve";

		rule = "procNet";
		ST stProcNet = st.getInstanceOf(rule);
		stProcNet.add("n", procName);
		stProcNet.add("pkn", "transl" + procName);

		rule = "field";
		ST st01 = st.getInstanceOf(rule);
	
		stProcNet.add("pnParamName", "limit");
		stProcNet.add("pnParamType", "int");

	//	stProcNet.add("pnParamName", "backoff");
	//	stProcNet.add("pnParamType", "int");

		stProcNet.add("pnStateVarName", "numFilters");
		stProcNet.add("pnStateVarType", st.getInstanceOf("int"));
		stProcNet.add("pnStateVarInit", "limit");

		stProcNet.add("procName", "generator");
		stProcNet.add("procLiteral", st.getInstanceOf("strLitPtr").add("n", "generator"));

		stProcNet.add("procArrayName", "filter");
		stProcNet.add("procArrayLiteral", st.getInstanceOf("strLitPtr").add("n", "filter"));
		
		stProcNet.add("pnClientChanName", "Out");
		stProcNet.add("pnClientChanType", st.getInstanceOf("int"));
		stProcNet.add("pnClientChanArrayName", "In");
		stProcNet.add("pnClientChanArrayType", st.getInstanceOf("int"));


		stProcNet.add("nwChanArrayName", "filterPipe");
		stProcNet.add("nwChanArrayType", st.getInstanceOf("int"));
		stProcNet.add("nwChanArraySize", "numFilters" + "+ 1");

		ST stGCF = st.getInstanceOf("gcf");
		stProcNet.add("pnGuardedChanFuncs", stGCF);

		stGCF = st.getInstanceOf("gcf");
		stGCF.add("n", "Int");
		stGCF.add("t", st.getInstanceOf("int"));
		stProcNet.add("pnGuardedChanFuncs", stGCF);
	
		ST stF = st.getInstanceOf("func");
		stF.add("n", "estSqRt");
		stF.add("t", st.getInstanceOf("int"));
		ST stFArgs = st.getInstanceOf("field");
		stFArgs.add("v", "x");
		stFArgs.add("v", "max");
		stFArgs.add("t", st.getInstanceOf("int"));
		stF.add("args", stFArgs);
		stProcNet.add("pnFuncs", stF);

		out(rule, "\n"+stProcNet.render());
	}

	private static void testOperations(STGroup st) {
		System.out.println("\n>> Operations <<");
		ST st00 = null;
		String rule = null;
	
		rule = "uOp";
		st00 = st.getInstanceOf(rule);
		st00.add("op", "-");
		st00.add("e", "(3+4)");
		out(rule, st00.render());	
		st00.add("c", "a comment");
		out(rule, st00.render());
		
		rule = "bOp";
		st00 = st.getInstanceOf(rule);
		st00.add("op", "-");
		st00.add("e0", "3");
		st00.add("e1", "2");
		out(rule, st00.render());

		rule = "fCall";
		st00 = st.getInstanceOf(rule);
		st00.add("n", "add");
		st00.add("args", "3");
		st00.add("args", "2");
		out(rule, st00.render());

		rule = "mCall";
		st00 = st.getInstanceOf(rule);
		st00.add("o", "thing");
		st00.add("m", "add");
		st00.add("args", "3");
		st00.add("args", "2");
		out(rule, st00.render());
		st00.add("args", "7");
		st00.add("c", "another argument and a comment");
		out(rule, st00.render());

		rule = "mAttrib";
		st00 = st.getInstanceOf(rule);
		st00.add("o", "thing");
		st00.add("a", "pointOfOrigin");
		st00.add("c", "\"another world\"");
		out(rule, st00.render());

		rule = "chIn";
		st00 = st.getInstanceOf(rule);
		st00.add("ch", "stuffChan");
		st00.add("v", "newStuff");
		out(rule, st00.render());

		st00 = st.getInstanceOf(rule);
		st00.add("ch", "signalChan");
		st00.add("c", "just a sync channel, no data");
		out(rule, st00.render());

		rule = "chOut";
		st00 = st.getInstanceOf(rule);
		st00.add("ch", "thingsChan");
		st00.add("e", "2 + 2");
		out(rule, st00.render());

		st00 = st.getInstanceOf(rule);
		st00.add("ch", "signalChan");
		st00.add("c", "just sending a sync signal");
		out(rule, st00.render());
	}

	private static void testDeclarations(STGroup st) {
		System.out.println("\n>> Declarations <<");
		ST st00 = null;
		String rule = null;
		
		rule = "rVarDecl";
		st00 = st.getInstanceOf(rule);
		st00.add("v", "wg");
		st00.add("t", "*sync.WaitGroup");
		out(rule, st00.render());

		st00 = st.getInstanceOf(rule);
		st00.add("v", "filterPipes");
		st00.add("t", 
				(st.getInstanceOf("slice")).add("t", (st.getInstanceOf("chan")).add("t",st.getInstanceOf("int"))));
		out(rule, st00.render());

		rule = "sVarDecl";
		st00 = st.getInstanceOf(rule);
		st00.add("v", "out");
		st00.add("e", (st.getInstanceOf("make")).add("t", (st.getInstanceOf("chan")).add("t", st.getInstanceOf("emptyStruct"))));
		out(rule, st00.render());

		rule = "assign";
		st00 = st.getInstanceOf(rule);
		st00.add("v", "isOver7000");
		st00.add("e", st.getInstanceOf("cFalse"));
		out(rule, st00.render());
		st00.add("c", "and comments still work too");
		out(rule, st00.render());

		rule = "bool";
		st00 = st.getInstanceOf(rule);
		out(rule, st00.render());

		rule = "int";
		st00 = st.getInstanceOf(rule);
		out(rule, st00.render());

		rule = "rune";
		st00 = st.getInstanceOf(rule);
		out(rule, st00.render());

		rule = "string";
		st00 = st.getInstanceOf(rule);
		out(rule, st00.render());

		rule = "emptyStruct";
		st00 = st.getInstanceOf(rule);
		out(rule, st00.render());

		rule = "chan";
		st00 = st.getInstanceOf(rule);
		st00.add("t", st.getInstanceOf("bool"));
		out(rule, st00.render());	

		rule = "chan";
		st00 = st.getInstanceOf(rule);
		out(rule, st00.render());	

		rule = "slice";
		st00 = st.getInstanceOf(rule);
		st00.add("t", st.getInstanceOf("int"));
		out(rule, st00.render());	

		rule = "slice";
		st00 = st.getInstanceOf(rule);
		out(rule, st00.render());

		rule = "ptr";
		st00 = st.getInstanceOf(rule);
		st00.add("t", st.getInstanceOf("int"));
		out(rule, st00.render());

		rule = "addr";
		st00 = st.getInstanceOf(rule);
		st00.add("v", "stuffThinger");
		out(rule, st00.render());

		rule = "struct";
		st00 = st.getInstanceOf(rule);
		st00.add("n", "Primesieve");
		ST st01 = st.getInstanceOf("field");
		st01.add("v", "numFilters");
		st01.add("t", "int");
		st00.add("field", st01);
		ST st02 = st.getInstanceOf("field");
		st02.add("v", "gen");
		st02.add("t", st.getInstanceOf("ptr").add("t", "generator"));
		st00.add("field", st02);
		out("\n"+rule, "\n"+st00.render());

		rule = "func";
		st00 = st.getInstanceOf(rule);
		String rxName = "generator";
		st00.add("rxName", rxName);
		st00.add("rxAbbr", rxName.substring(0,1));
		st00.add("n", "gen0");
		st00.add("t", st.getInstanceOf("int"));
		st02.add("c", "no, this doesn't make any sense in context.");
		st00.add("line", st02);
		out("\n"+rule, "\n"+st00.render());

		st00.add("c", "gen0 is derived from the CSP process named GEN0");
		out("\n"+rule, "\n"+st00.render());

		rule = "strLit";
		st00 = st.getInstanceOf(rule);
		st00.add("n", "filter");
		st00.add("k", "wg");
		st00.add("v", "&wg");
		st00.add("k", "id");
		st00.add("v", "i");
		out("\n"+rule, "\n"+st00.render());

		st02 = st.getInstanceOf("addr");
		st02.add("v", st00);
		out("pointer to rule", "\n"+st02.render());
	}

	private static void testConstants(STGroup st) {
		System.out.println("\n>> Constants <<");
		ST st00 = null;
		String rule = null;
		
		rule = "cFalse";
		st00 = st.getInstanceOf(rule);
		out(rule, st00.render());

		rule = "cTrue";
		st00 = st.getInstanceOf(rule);
		out(rule, st00.render());

		rule = "emptyStructSignal";
		st00 = st.getInstanceOf(rule);
		out(rule, st00.render());
	}

	private static void testBuiltin(STGroup st) {
		System.out.println("\n>> Built-in functions <<");
		ST st00 = null;
		ST st01 = null;
		String rule = null;
	
		rule = "make";
		st00 = st.getInstanceOf(rule);
		st00.add("t", (st.getInstanceOf("chan")).add("t", st.getInstanceOf("int")));
		out(rule, st00.render());

		rule = "make";
		st00 = st.getInstanceOf(rule);
		st00.add("t", (st.getInstanceOf("chan")).add("t", st.getInstanceOf("int")));
		st00.add("m", "5"); // shouldn't appear in final output because no 'n'
		out(rule, st00.render());

		rule = "make";
		st00 = st.getInstanceOf(rule);
		st00.add("t", (st.getInstanceOf("chan")).add("t", st.getInstanceOf("int")));
		st00.add("n", "1");
		out(rule, st00.render());		
	
		rule = "make";
		st00 = st.getInstanceOf(rule);
		st00.add("t", (st.getInstanceOf("slice")).add("t", st.getInstanceOf("int")));
		st00.add("n", "10");
		st00.add("m", "100");
		out(rule, st00.render());		
			
		rule = "append";
		st00 = st.getInstanceOf("make");
		st00.add("t", (st.getInstanceOf("chan")).add("t", st.getInstanceOf("int")));
		st01 = st.getInstanceOf(rule);
		st01.add("e0", st00);
		st01.add("e1", "intChans");
		out(rule, st01.render());
	}

	private static void testBooleanExpr(STGroup st) {
		System.out.println("\n>> Boolean <<");
		ST st00 = null;
		String rule = null;
		
		rule = "and";
		st00 = st.getInstanceOf(rule);
		st00.add("e0", "x");
		st00.add("e1", "y");
		out(rule, st00.render());

		rule = "or";
		st00 = st.getInstanceOf(rule);
		st00.add("e0", "x");
		st00.add("e1", "y");
		out(rule, st00.render());
		
		rule = "not";
		st00 = st.getInstanceOf(rule);
		st00.add("e", "x");
		out(rule, st00.render());
	}

	private static void testArithmeticExpr(STGroup st) {
		System.out.println("\n>> Arithmetic <<");
		ST st00 = null;
		String rule = null;

		rule = "add";
		st00 = st.getInstanceOf(rule);
		st00.add("e0", "x");
		st00.add("e1", "y");
		out(rule, st00.render());
		
		rule = "sub";
		st00 = st.getInstanceOf(rule);
		st00.add("e0", "x");
		st00.add("e1", "y");
		out(rule, st00.render());
		
		rule = "mul";
		st00 = st.getInstanceOf(rule);
		st00.add("e0", "x");
		st00.add("e1", "y");
		out(rule, st00.render());
		
		rule = "div";
		st00 = st.getInstanceOf(rule);
		st00.add("e0", "x");
		st00.add("e1", "y");
		out(rule, st00.render());
		
		rule = "mod";
		st00 = st.getInstanceOf(rule);
		st00.add("e0", "x");
		st00.add("e1", "y");
		out(rule, st00.render());
	}

	private static void testComparisonExpr(STGroup st) {
		System.out.println("\n>> Comparison <<");
		ST st00 = null;
		String rule = null;
		
		rule = "eq";
		st00 = st.getInstanceOf(rule);
		st00.add("e0", "x");
		st00.add("e1", "y");
		out(rule, st00.render());
		
		rule = "neq";
		st00 = st.getInstanceOf(rule);
		st00.add("e0", "x");
		st00.add("e1", "y");
		out(rule, st00.render());
		
		rule = "lt";
		st00 = st.getInstanceOf(rule);
		st00.add("e0", "x");
		st00.add("e1", "y");
		out(rule, st00.render());
		
		rule = "lte";
		st00 = st.getInstanceOf(rule);
		st00.add("e0", "x");
		st00.add("e1", "y");
		out(rule, st00.render());
		
		rule = "gt";
		st00 = st.getInstanceOf(rule);
		st00.add("e0", "x");
		st00.add("e1", "y");
		out(rule, st00.render());
		
		rule = "gte";
		st00 = st.getInstanceOf(rule);
		st00.add("e0", "x");
		st00.add("e1", "y");
		out(rule, st00.render());
	}

	private static void testMisc(STGroup st) {
		System.out.println("\n>> Misc <<");
		ST st00 = null;
		String rule = null;

		rule = "comment";
		st00 = st.getInstanceOf(rule);
		st00.add("c", "Listening to Kate Bush as I write these tests");
		out(rule, st00.render());

		rule = "return";
		st00 = st.getInstanceOf(rule);
		st00.add("e", "x");
		out(rule, st00.render());

		rule = "parens";
		st00 = st.getInstanceOf(rule);
		st00.add("e", "x + y + z");
		out(rule, st00.render());

		rule = "pe";
		st00 = st.getInstanceOf(rule);
		st00.add("e", "7001");
		st00.add("c", "it's over 7000!");
		out(rule, st00.render());
	}
}
