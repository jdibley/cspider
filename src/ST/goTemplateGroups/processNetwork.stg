import "goFundamentals.stg"
import "expr.stg"
import "misc.stg"

// Process network string template

procNet(n,			// Exportable type name
	pkn,			// package name
	pnParamName,		// process network argument attributes
	pnParamType,
	pnConstantName,		// process network global constants
	pnConstantType,
	pnConstantInit,
	pnStateVarName,		// process network state variable attributes
	pnStateVarType,
	pnStateVarInit,
	pnClientChanName,	// client channel attributes
	pnClientChanType,
	pnClientChanArrayName,	// client channel array attributes
	pnClientChanArrayType,
	pnRenamedChanClientName, // renamed channel exposed to client
	pnRenamedChanType,
	pnRenamedChanNetworkName,
	nwChanName,		// internal channel attributes
	nwChanType,
	nwChanArrayName,	// internal channel array attributes
	nwChanArraySize,
	nwChanArrayType,
	procName,		// internal process attributes
	procLiteral,
	procArrayName,		// internal replicated process attributes
	procArrayCtr,
	procArraySize,
	procArrayLiteral,
	pnFuncs,		// Non-process functions 
	pnCommStruct,		// multi-value channel structs	
	pnGuardedChanFuncs	// typed GCFs (for use by processes within package)
	) ::= <<
package <pkn>

import "sync"

type <n> struct {
	<field("*sync.WaitGroup", "wg")>
	<if(pnConstantName)>
	// process network state variables
	<pnConstantName, pnConstantType:{n,t|<field(t,n)>}; separator="\n">
	<endif>
	<if(pnParamName)>
	// process network parameters
	<if(pnParamName)><pnParamName, pnParamType:{n,t|<field(t,n)>}; separator="\n"><endif>
	<endif>
	<if(pnStateVarName)>
	// process network state variables
	<if(pnStateVarName)><pnStateVarName, pnStateVarType:{n,t|<field(t,n)>}; separator="\n"><endif>
	<endif>
	<if(pnClientChanName || pnClientChanArrayName || pnRenamedChanClientName)>
	// client channels
	<endif>
	<if(pnClientChanName)>
	<pnClientChanName, pnClientChanType:{n,t|<field(t,n)>}; separator="\n">
	<endif>
	<if(pnClientChanArrayName)>
	<pnClientChanArrayName, pnClientChanArrayType:{n,t|<field(t,n)>}; separator="\n">
	<endif>
	<if(pnRenamedChanClientName)>
	<pnRenamedChanClientName, pnRenamedChanType:{n,t|<field(t,n)>}; separator="\n">
	<endif>
	<if(procName||procArrayName)>
	// processes
	<endif>
	<if(procName)>
	<procName:{n|<procField(n)>}; separator="\n">
	<!<procField(procName); separator="\n">!>
	<endif>
	<if(procArrayName)>
	<procArrayName:{n|<procArrayField(n)>}; separator="\n">
	<!<procArrayField(procArrayName); separator="\n">!>
	<endif>
}

func New<n>(<newArgsList(pnParamName, pnParamType, pnClientChanName, pnClientChanType, pnClientChanArrayName, pnClientChanArrayType); wrap="\n">) <ptr(n)> {
	<rVarDecl("wg", "sync.WaitGroup")>
	<if(pnConstantName)>
	
	// init package constants 
	<pnConstantName, pnConstantInit:{v,e|<sVarDecl(v,e)>}; separator="\n">
	<endif>
	<if(pnStateVarName)>
	
	// init state variables
	<pnStateVarName, pnStateVarInit:{v,e|<sVarDecl(v,e)>}; separator="\n">
	<endif>
	<if(nwChanName||nwChanArrayName)>
	
	// allocate internal channels
	<nwChanName, nwChanType:{c,m| <makeChan(c,m)>}; separator="\n">
	<nwChanArrayName, nwChanArraySize, nwChanArrayType: {n,s,t | <makeChanArray(n,s,t)>}; separator="\n\n">
	<endif>
	<if(procName)>
	
	// allocate processes
	<procName, procLiteral:{n, l| <makeProc(n,l)>}; separator="\n">
	<endif>
	<if(procArrayName)>
	
	// allocate replicated processes
	<procArrayName, procArraySize, procArrayLiteral, procArrayCtr:{n,s,l,c|<makeProcArray(n,s,l,c)>}; separator="\n">
	<endif>
	
	pn := <addr(n)>{ wg: &wg,
		<initProcNetFields(pnParamName, pnConstantName, pnStateVarName, procName, procArrayName, pnClientChanName, pnClientChanArrayName, pnRenamedChanClientName, pnRenamedChanNetworkName)>
	}
	<return("pn")>
}

func (pn *<n>) <n>() {
	<procName:invokeProcName(); separator="\n">
	<procArrayName:{n|<invokeProcArray(n)>}; separator="\n\n">
}

<if(pnFuncs)>
// User-defined (non-process) functions 
<pnFuncs; separator="\n\n">

<endif>
<if(pnCommStruct)>
// Message structs for complex channels 
<pnCommStruct; separator="\n\n">

<endif>
<if(pnGuardedChanFuncs)>
// Functions for guarded channel operations
<pnGuardedChanFuncs; separator="\n\n">
<endif>
>>


// Utility macros

procField(name) ::= <<
<name> <name:ptr()>
>>

procArrayField(name) ::= <<
<pluralise(name)> <name:ptr():slice()>
>>

invokeProcName(name) ::= <<
pn.<name>.<name>()
>>

invokeProcArray(name) ::= <<
for _, p := range pn.<name:pluralise()> {
	p.<name>()
}
>>

initProcNetFields(pnP, pnC, pnSVN, pN, pAN, pCC, pCCA, pRCCN, pRCNN) ::= <<
<if(pnP)><pnP:{v|<fieldAssign(v,v)>,}; separator="\n"><endif>
<if(pnC)><pnC:{v|<fieldAssign(v,v)>,}; separator="\n"><endif>
<if(pnSVN)><pnSVN:{v|<fieldAssign(v,v)>,}; separator="\n"><endif>
<if(pN)><pN:{p|<fieldAssign(p,p)>,}; separator="\n"><endif>
<if(pAN)><pAN:{pa|<fieldAssignProcArray(pa)>,}; separator="\n"><endif>
<if(pCC)><pCC:{cc|<fieldAssign(cc,cc)>,}; separator="\n"><endif>
<if(pCCA)><pCCA:{cca|<fieldAssign(cca,cca)>,}; separator="\n"><endif>
<if(pRCCN)><pRCCN,pRCNN:{ccn,cnn|<fieldAssign(ccn,cnn)>,}; separator="\n"><endif>
>>

makeChan(name, type) ::= 
	"<name> := <chan(type):make()>"

makeChanArray(name, size, type) ::= 
<<
var <name> <chan(type):slice()>
for i := 0; i \<= <size>; i++ {
	<name> = <chan(type):make():appendChan(name)>
}
>>

fieldAssignProcArray(p) ::= 
	"<p:pluralise()>: <p:pluralise()>"

makeProc(name, literal) ::=
	"<name> := <literal>"

makeProcArray(name, size, literal, ctr) ::=
<<
var <name:pluralise()> <ptr(name):slice()>
for <ctr> := 0; <ctr> \<= <size>; <ctr>++ {
	<name:pluralise()> = <literal:append(name)>
}
>>

newArgsList(pnPN, pnPT, pnCCN, pnCCT, pnCCAN, pnCCAT) ::= <%
	<if(pnPN)><pnPN,pnPT:{n,t|<arg(n,t)>}; separator=", "><endif>
	<if(pnCCN&&pnPN)>, <endif>
	<if(pnCCN)><pnCCN,pnCCT:{n,t|<arg(n,t)>}; separator=", "><endif>
	<if((pnCCAN&&pnPN)||(pnCCAN&&pnCCN))>, <endif>
	<if(pnCCAN)><pnCCAN,pnCCAT:{n,t|<arg(n,t)>}; separator=", "><endif>
%>
