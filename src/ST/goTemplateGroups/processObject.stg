import "goFundamentals.stg"
import "expr.stg"
import "misc.stg"


// Process object string templates

procObjectLiteral(
	n,
	varName,
	varInit,
	poChanName,
	pnChanName
	) ::= <<
<addr(n)>{wg: &wg,
	<varName, varInit: {n,i|<fieldAssign(n,i)>,}; separator="\n">
	<poChanName, pnChanName:{o,n|<fieldAssign(o,n)>,}; separator="\n"> 
}
>>



processObject(
	pkn, 		// package name
	procPfx, 	// process label prefix 
	procLabel, 	// process labels
	n, 		// name of process object
	nI, 		// initial of process object 
	constantName,	// object constant declarations
	constantType,
	stateVarName, 	// state variable declarations
	stateVarType, 
	proxyChan,	// proxy chan
	chanName,	// channel declarations 
	chanType,
	chanArrayName,	// channel array declarations
	chanArrayType,	
	methodName, 	// method names
	init, 		// initialisation assignments
	initSt,	 	// initial process label
	procStMethods,	// methods corresponding to states of the process object
	userDefMethods	// methods corresponding to CSPM functions
	) ::= <<
package <pkn>

const (
	<pLabel("SKIP",procPfx)> = iota
	<procLabel:pLabel(procPfx);separator = "\n">
)

<! process object definition !>
type <n> struct {
	// admin
	wg 	  *sync.WaitGroup
	jumpTable map[int]func() int
	jump	  int
	<if(constantName)>
	// constants
	<constantName, constantType:{n,t|<field(t,n)>}; separator="\n">
	<endif>
	<if(stateVarName)>
	// state variables
	<stateVarName, stateVarType:{n,t|<field(t,n)>}; separator="\n">
	<endif>
	<if(proxyChan)>
	// proxy channel
	<proxy()> <emptyStruct():chan()>
	<endif>
	<if(chanName||chanArrayName)>
	// channels
	<endif>
	<if(chanName)>
	<chanName, chanType:{n,t|<field(t,n)>}; separator="\n">
	<endif>
	<if(chanArrayName)>
	<chanArrayName, chanArrayType:{n,t|<field(t,n)>}; separator="\n"> 
	<endif>
}

<! primary driver function - this is what we invoke from 'outside' !>
func (<nI> *<n>) <n>() {
	<nI>.jumpTable = map[int]func() int{
		<procLabel, methodName:{c,m|<pLabel(c,procPfx)>: <nI>.<m>,}; separator="\n">
	}
	<nI>.wg.Add(1)
	<nI>.jump = <if(initSt)><initSt><else><first(procLabel):pLabel(procPfx)><endif>
	<if(proxyChan)>
	<nI>.<proxy()> = <makeProxyChan()> 
	<nI>.<proxy():chOut()> 
	<endif>
	<if(init)>
	// initialisation
	<init; separator="\n">
	<endif>

	go func() {
		for {
			<nI>.jump = <nI>.jumpTable[<nI>.jump]()
			if <nI>.jump == <pLabel("SKIP", procPfx)> {
				break
			}
		}
		<nI>.wg.Done()
	}()
}

// Implemented process states
<procStMethods; separator="\n\n">

<if(userDefMethods)>
// User-defined (non-process) functions
<userDefMethods; separator="\n\n">
<endif>
>>


// Utility macros 

pLabel(n, pfx) ::= "<pfx>_<n>"

makeProxyChan() ::= "make(<chan()>, 1)"  
